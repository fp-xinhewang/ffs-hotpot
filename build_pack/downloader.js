cc.loader.downloader.loadSubpackage = function (name, completeCallback) {
    wx.loadSubpackage({
        name: name,
        success: function () {
            if (completeCallback) completeCallback();
        },
        fail: function () {
            if (completeCallback) completeCallback(new Error(`Failed to load subpackage ${name}`));
        }
    })
};

function downloadScript (item, callback, isAsync) {
    var url = '../../' + item.url;
    require(url);
    callback(null, item.url);
}

function loadFont (item) {
    var url = item.url;

    cc.log('url m =' + url);
    var fontFamily = wx.loadFont(url);
    return fontFamily || 'Arial';
}

function arrayBufferHandler(item, callback) 
{
    cc.log("-------arrayBufferHandler----------");
    var urlstr = item.url;

    cc.log('url n =' + urlstr);
    wx.request({
      data: {},
      url: urlstr,
      method: "GET",
      responseType: "arraybuffer",
      success: function success(_ref) 
      {        
        var data = _ref.data;
        var arrayBuffer = data;
        if (arrayBuffer) {
            var result = new Uint8Array(arrayBuffer);
            callback(null, result);
        }
        else {
            callback("empty pb"); 
        }
      },
      fail: function fail(_ref2) {
        callback("get pb error"); 
      }
    });

/*
    let xhr = cc.loader.getXMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "arraybuffer";
    xhr.onload = function (oEvent) {
        var arrayBuffer = xhr.response;
        if (arrayBuffer) {
            var result = new Uint8Array(arrayBuffer);
            callback(null, result);
        }
        else {
            callback("empty pb"); 
        }
    }
    xhr.onerror = function(){
        callback("get pb error"); 
    }
    xhr.ontimeout = function(){
        callback("get pb timeout");
    };
    xhr.send(null);
    */
};


cc.loader.downloader.addHandlers({
    js : downloadScript,

    pvr: cc.loader.downloader.extMap.png,
    etc: cc.loader.downloader.extMap.png,
});

cc.loader.loader.addHandlers({
    pvr: cc.loader.loader.extMap.png,
    etc: cc.loader.loader.extMap.png,

    // Font
    pb : arrayBufferHandler,
    font: loadFont,
    eot: loadFont,
    ttf: loadFont,
    woff: loadFont,
    svg: loadFont,
    ttc: loadFont,
});
