var utils = require('utils');
var game = require('game');
var data = require('datas');
var ui = require('ui');
var configLoader = require('configLoader');
var webRequestManager = require('webRequestManager');
var soundManager = require('soundManager');
var taskManager = require('taskManager');

var gameLevel = cc.Class({
    extends: cc.Component,

    properties: {

        soupBarNode : cc.Node,
        canvasNode : cc.Node,
        npcAreaNode : cc.Node,
        plateAreaNode : cc.Node,
        platePrefab : cc.Prefab,
        npcPrefab : cc.Prefab,
        potPrefab_1 : cc.Prefab,
        potPrefab_2 : cc.Prefab,
        potPrefab_3 : cc.Prefab,
        completePrefab : cc.Prefab,
        potContainer : cc.Node,
        potDebugNode : cc.Node,
        effectLayer : cc.Node,
        crashEffectLayer : cc.Node,
        bgmEnabledFlag : cc.Node,
        effectSoundEnabledFlag : cc.Node,
        menuButtons : cc.Node,
        bossComingFlag : cc.Node,
        bossComingLight : cc.Node,
        flyObjsNode : cc.Node,
        coinsBarNode : cc.Node,
        playComboNode : cc.Node,
        addStepLimitNode : cc.Node,
        guideLayerNode : cc.Node,
        bossCurtain : cc.Node,
        menuMaskLayer : cc.Node
    },

    onLoad : function () {

        this.npcNumber = 0;         //同屏人数
        this.stepNumberLeft = 0;   //剩余步数
        this.stepNumberLeftMax = 0;
        this.npcRefreshFromServer = false; //是否从后端刷新了
        this.needUpdated = true;
        this.getCoinsNumber = 0;
        this.stageId = 1;
        this.sharedPanelDisplay = false;

        this.stepZeroCheckTime = 0;
        this.shareTimes = 0;
        
        this.bossMode = false;
        this.bossNumber = 1;

        this.menuOpened = false;
        this.menuButtons.active = false;

        this.stageCompleteFlag = false;

        this.finishedNpcId = new utils.jsVector();
        this.npcQueue = new utils.jsQueue(256);

        this.hideBossComingFlag();
        //soundManager.instance.playBgm('');
        //soundManager.instance.playBgm('main_bg' + utils.randInt(3));
        soundManager.instance.playBgm('main_bg');
        soundManager.instance.playSound('enter_stage');

        this.bgmEnabledFlag.active = !soundManager.instance.isBgmEnabled();
        this.effectSoundEnabledFlag.active = !soundManager.instance.isSoundEnabled();
    },

    onDestroy()
    {
        game.instance.gameLevelInstance = null;
    }
    ,

    getBombObjects : function()
    {
        let foodNode = this.potContainer.getChildByName('pot');
        if (!!foodNode) {

            let foodNodeManager = foodNode.getComponent('FoodManager');
            if (!!foodNodeManager) {
                let objs = foodNodeManager.getBombListInGuide();
                return objs;
            }
        }

        return null;
    }
    ,

    get3LinkedObjects : function()
    {
        let foodNode = this.potContainer.getChildByName('pot');
        if (!!foodNode) {

            let foodNodeManager = foodNode.getComponent('FoodManager');
            if (!!foodNodeManager) {
                let objs = foodNodeManager.get3LinkkedInGuide();
                return objs;
            }
        }

        return null;
    }
    ,

    start : function()
    {
        game.instance.gameLevelInstance = this;

        let guideStatus = game.instance.getGuideStatus();
        if (!!guideStatus) {
            if (guideStatus.isInGuideStatus() == false) {
                this.guideLayerNode.active = false;
            }
        }

        ui.instance.clearNpcDisplayPool();
        this.stageId = game.instance.getGameLevelStage();
        this.startGameLevel(this.stageId);
    }
    ,
    
    startGameLevel : function (stage) {

        let stageConfig = configLoader.instance.getStageConfig(stage);
        this.npcNumber = stageConfig.GuestNum;
        this.stepNumberLeft = stageConfig.SoupNum;
        this.stepNumberLeftMax = stageConfig.SoupNum;

        let soupCompent = this.soupBarNode.getComponent('soupBar');
        if (!!soupCompent) {
            soupCompent.initialize(this.stepNumberLeft, this.stepNumberLeftMax);
        }

        let potType = stageConfig.SpecId;

        this.initPot(potType);

/*
        for (let i = 0; i < 200; i ++) {
            let npc = new data.npcOrder();
        
            npc.npcId = 10001 + utils.randInt(4);
            npc.orderId = 1;
            npc.wantEat = 20001 + utils.randInt(4);
            npc.wantNum = utils.randInt(20) + 1;
            npc.coins = utils.randInt(200) + 1;;

            this.npcQueue.enQueue(npc);
        }

        this.npcRefreshFromServer = true;
        this.initNpcOrders();

        */

        
        webRequestManager.instance.requestStartGameLevel(stage, 
            ( rsp ) => {
                cc.log('--------------------------------startGameLevel');
                cc.log(rsp);

                this.addNpcQueueFromServer(rsp.npcs);
                this.npcRefreshFromServer = true;
                this.initNpcOrders();

                //cc.log('--------------------------------30');
            }
        );
        
    }
    ,

    initPot : function(potType)
    {
        if (!!this.potContainer) {

            let potNode = cc.instantiate(this['potPrefab_'+potType]);
            let foodMgr = potNode.getComponent('FoodManager');

            if (!!foodMgr) {

                foodMgr.canvas = this.canvasNode;
                foodMgr.debug_node = this.potDebugNode;
                foodMgr.effectLayer = this.effectLayer;
                foodMgr.crashEffectLayer = this.crashEffectLayer;

                foodMgr.coin_icon = cc.find('statusLeft/stageCoinsBar/coins', this.canvasNode);
                foodMgr.soup_icon = cc.find('statusMiddle/soupBar/broth', this.canvasNode);

                foodMgr.onInitialize();
            }

            this.potContainer.addChild(potNode,0,'pot');
        }
    }
    ,

    initNpcOrders : function () {

        if (!this.npcRefreshFromServer) {
            return;
        }

        let npcNumber = this.getNpcPosCount();
        
        this.npcPool = new cc.NodePool();
        this.npcDatas = [];

        for (let i =0;i<npcNumber;i ++) {
            this.npcDatas[i] = new data.npcOrder();
            let npc = this.npcQueue.deQueue();
            this.npcDatas[i].copyFrom(npc);

            let npcNode = this.createNpcNode(npc.npcId);
            this.npcPool.put(npcNode);
        }

        let w = this.npcAreaNode.width;
        let sx = - w*0.375;
        let pw = w*0.25;

        if (npcNumber == 1) {
            sx = 0;
            pw = 0;
        }else if (npcNumber == 2) {
            sx = - w*0.25;
            pw = w*0.5;
        }else if (npcNumber == 3) {
            sx = - w*0.33;
            pw = w*0.33;
        }

        if (this.npcAreaNode != null)
        {
            for (let i = 0;i < npcNumber; i ++) {

                let node = this.getNpcNode(this.npcDatas[i].npcId);
                this.npcAddToNode(node,npcNumber,i);

                this.npcDatas[i].npcNode = node;
            }
        }

        if (this.plateAreaNode != null)
        {
            for (let i = 0;i <npcNumber; i ++) {

                let node = cc.instantiate(this.platePrefab);
                node.setAnchorPoint(0.5,0.0);
                node.setPosition(sx + i * pw,0);

                let leftDishNode = node.getChildByName('dishLeftNum');
                let dishNum = leftDishNode.getComponent('cc.Label');

                let allNum = this.npcDatas[i].wantNum;
                let finishNum = this.npcDatas[i].finishNum;
                let numLeft = allNum - finishNum;

                this.npcDatas[i].labDisplay = numLeft;
                dishNum.string = numLeft;
                
                let wantEat = this.npcDatas[i].wantEat;

                let dishKindPic = node.getChildByName('dishKind');
                
                let stageId = game.instance.getGameLevelStage();
                let plist_name = 'dish/' + stageId.toString() + '/dishKind';

                let sprKind = utils.createSpriteFromPlist(plist_name,wantEat);
                dishKindPic.addChild(sprKind);                

                node.setName("npc_" + i);
                this.plateAreaNode.addChild(node);

                let avatarComp = this.npcDatas[i].npcNode.getChildByName('npc_node').getComponent('avatar');
                if (!!avatarComp) {
                    avatarComp.plateNode = node;
                }
                this.npcDatas[i].plateNode = node;
            }
        }
    },

    getNpcNumberInScene : function()
    {
        let npcNumber = this.getNpcPosCount();

        let retValue = 0;
        for (let i = 0; i < npcNumber; i ++)
        {
            let allNum = this.npcDatas[i].wantNum;
            let finishNum = this.npcDatas[i].finishNum;
            
            if (finishNum < allNum) {
                retValue ++;
            }
        }

        return retValue;
    }
    ,

    getNpcPosCount : function()
    {
        let npcNumber = this.npcNumber;
        if (this.bossMode) {
            npcNumber = this.bossNumber;
        }

        return npcNumber;
    }
    ,

    showBossComingFlag()
    {
        if (!!this.bossComingFlag && this.bossComingFlag.active == false) {
            this.bossComingFlag.active = true;
            this.bossComingFlag.x = this.bossComingFlag.width;
            this.bossComingFlag.runAction(cc.moveTo(1.0,cc.v2(0,0)));

            let flashScript = this.bossComingLight.getComponent('lightFlash');
            if (!!flashScript) {
                flashScript.startNormalFlash();
            }
        }
    },

    hideBossComingFlag()
    {
        if (!!this.bossComingFlag) {
            this.bossComingFlag.active = false;
        }
    }
    ,

    testButtonRefreshPot()
    {
        let foodNode = this.potContainer.getChildByName('pot');
        if (!!foodNode) {

            let foodNodeManager = foodNode.getComponent('FoodManager');
            if (!!foodNodeManager) {
                foodNodeManager.refreshPot();
            }
        }
    }
    ,

    testButtonFly()
    {
        let limitMaxCompent = this.addStepLimitNode.getComponent('panelMaxStepAdd');
        if (!!limitMaxCompent) {
            limitMaxCompent.showStepLimitAddPanel();
        }
    }
    ,

    switchBossMode(val)
    {
        this.bossMode = val;
        this.hideBossComingFlag();

        let curtainScript = this.bossCurtain.getComponent('bossCurtain');
        if (!!curtainScript) {
            if (this.bossMode == true) {
                curtainScript.openCurtain();
            }else{
                curtainScript.closeCurtain();
            }
        }

        if (this.bossMode == true)
        {
            soundManager.instance.playBgm('boss');
        }
        else{
            soundManager.instance.playBgm('main_bg');
            //soundManager.instance.playBgm('main_bg' + utils.randInt(3));
        }

        let flashScript = this.bossComingLight.getComponent('lightFlash');
        if (!!flashScript) {
            if (this.bossMode) {
                flashScript.startQuickFlash();
            }else
            {
                flashScript.endFlash();
            }
        }
   
        let npcNumber = this.getNpcPosCount();
      
        for (let i =0; i < npcNumber; i ++)
        {
            let nextNpc = this.npcQueue.getTop();
            if (val == nextNpc.isBoss) {
                
                if (this.bossMode) {
                    cc.log('enter BOSSS mode... npcId = ' + nextNpc.npcId);
                }
                else {
                    cc.log('enter normal mode... npcId = ' + nextNpc.npcId);
                }

                this.npcQueue.deQueue();
                this.npcDatas[i].copyFrom(nextNpc);
            
                this.npcPool.put(this.npcDatas[i].npcNode);
                this.npcDatas[i].npcNode = this.getNpcNode(nextNpc.npcId);
                this.npcAddToNode(this.npcDatas[i].npcNode,npcNumber,i);
                
                //刷新订单数量和图标
                let plateNode = this.npcDatas[i].plateNode;
                let wantEat = this.npcDatas[i].wantEat;
                let dishKindPic = plateNode.getChildByName('dishKind');
                dishKindPic.removeAllChildren(true);

                let stageId = game.instance.getGameLevelStage();
                let plist_name = 'dish/' + stageId.toString() + '/dishKind';
                let sprKind = utils.createSpriteFromPlist(plist_name,wantEat);
                dishKindPic.addChild(sprKind);
    
                this.npcDatas[i].labDisplay = null;
                let avatarComp = this.npcDatas[i].npcNode.getChildByName('npc_node').getComponent('avatar');
                if (!!avatarComp) {
                    avatarComp.showPlate();
                }                
            }
        }
    }
    ,

    getNextNpc : function (finishedIdx)
    {
        let i = finishedIdx;
        let finishedId = this.npcDatas[i].npcId;
        let npcNumber = this.getNpcPosCount();

        let nextNpc = this.npcQueue.getTop();
        if (nextNpc.isBoss != this.bossMode) 
        {
            let willBeBoss = nextNpc.isBoss;

            let avatarComp = this.npcDatas[i].npcNode.getChildByName('npc_node').getComponent('avatar');
            if (!!avatarComp) {

                this.playAddHeartAnimation(finishedIdx);

                let num = this.getNpcNumberInScene();
                if (num == 0) {

                    avatarComp.playDisappearAndHide(()=>{
                        
                        this.switchBossMode(willBeBoss);
                    });
                }
                else {
                    avatarComp.playDisappearAndHide();
                }                
            }

            if (willBeBoss) {
                this.showBossComingFlag();
            }

            return;
        }

        nextNpc = this.npcQueue.deQueue();

        if (!!nextNpc) {
            
            this.playAddHeartAnimation(finishedIdx);

            this.npcDatas[i].copyFrom(nextNpc);
            if (!this.bossMode) {
                this.npcPool.put(this.npcDatas[i].npcNode);
                this.npcDatas[i].npcNode = this.getNpcNode(nextNpc.npcId, true);
                this.npcAddToNode(this.npcDatas[i].npcNode,npcNumber,i);
                let avatarComp = this.npcDatas[i].npcNode.getChildByName('npc_node').getComponent('avatar');
                if (!!avatarComp) {
                    avatarComp.playOlderDisappearAndNewerAppear(finishedId, nextNpc.npcId);
                }
            }
            else {
                let avatarComp = this.npcDatas[i].npcNode.getChildByName('npc_node').getComponent('avatar');
                if (!!avatarComp) {
                    avatarComp.playBossAnimation();
                }
            }

            //刷新订单数量和图标
            let plateNode = this.npcDatas[i].plateNode;
            let wantEat = this.npcDatas[i].wantEat;
            let dishKindPic = plateNode.getChildByName('dishKind');
            dishKindPic.removeAllChildren(true);

            let stageId = game.instance.getGameLevelStage();
            let plist_name = 'dish/' + stageId.toString() + '/dishKind';
            let sprKind = utils.createSpriteFromPlist(plist_name,wantEat);
            dishKindPic.addChild(sprKind);

            this.npcDatas[i].labDisplay = null;
        }
    }
    ,

    createNpcNode : function (npcId)
    {
        if (!!this.npcPrefab) {
            let node = cc.instantiate(this.npcPrefab);
            let npcNode = node.getChildByName('npc_node');
            let npcComp = npcNode.getComponent('avatar');

            if (!!npcComp) {
                npcComp.init(npcId);
            }

            return node;
        }

        return null;
    }
    ,

    addNpcQueueFromServer(npcs)
    {
        if (!!npcs) {
            let len = npcs.length;
            for(let i = 0; i < len; i ++) {

                let orderLen = npcs[i].orders.length;

                for (let j = 0; j < orderLen; j++) {

                    let npc = new data.npcOrder();
        
                    npc.npcId = npcs[i].npcId;
                    npc.npcUniqueId = npcs[i].npcUniqueId;

                    npc.isBoss = false;
                    
                    let npcConfig = configLoader.instance.getNpcConfig(npc.npcId);
                    if (!!npcConfig) {
                        npc.isBoss = npcConfig.BossIs || false;
                    }

                    npc.orderId = npcs[i].orders[j].orderId;
                    npc.wantEat = npcs[i].orders[j].pieceId;
                    npc.wantNum = npcs[i].orders[j].needPieceNum;
                    npc.coins = npcs[i].orders[j].coins;
            
                    this.npcQueue.enQueue(npc);
                }
            }
        }
    }
    ,

    addStepLeft : function(num)
    {
        this.stepNumberLeft += num;
        if (this.stepNumberLeft > this.stepNumberLeftMax) {
            this.stepNumberLeft = this.stepNumberLeftMax;
        }
    }
    ,

    playComboEffect : function(level)
    {
        let comboEffect = this.playComboNode.getComponent('comboEffect');
        if (!!comboEffect)
        {
            comboEffect.playCombo(level);
        }
    }
    ,

    playNpcDisappointedAnimation : function()
    {
        let npcNumber = this.getNpcPosCount();

        for (let i =0; i < npcNumber; i ++) {
            
            let avatarComp = this.npcDatas[i].npcNode.getChildByName('npc_node').getComponent('avatar');
            if (!!avatarComp) {
                avatarComp.playDisappointed();
            }
        }
    }
    ,

    addGuideStepReward : function(reward)
    {
        if (reward == null) {
            return;
        }

        for (let key in reward) {
            if (key == 19) {

                let coinsNum = reward[key];

                if (coinsNum > 0) {

                    this.getCoinsNumber += coinsNum;
                    game.instance.addCurrentStageCoins(coinsNum);

                    /*
                    this.flyObjsNode.active = true;
                    let objsFly = this.flyObjsNode.getComponent('flyObjects');
                    if (!!objsFly) {
                        objsFly.flyCoins(new cc.Vec2(0,0), coinsNum);
                    }
                    */           
                }
            }
        }
    }
    ,

    eattingFood : function(foodMap)
    {
        if (this.stageCompleteFlag == true) {
            return;
        }

        let use_step = foodMap.has(0) && foodMap.get(0) > 0;
        foodMap.delete(0);

        let flyCoinsPos = cc.v2(0,0);
        if (foodMap.has('pos')) {
            flyCoinsPos = foodMap.get('pos');
            foodMap.delete('pos');
        }

        let eventSource = '';
        if (foodMap.has('eventSource')) {
            eventSource = foodMap.get('eventSource');
            foodMap.delete('eventSource');
        }
        

        let msg0 =
        {
            details: {}, 
            coins : 0,
            isEnd : false
        };

        let curCoinsNum = 0;
        let needSendToServer = false;

        let npcNumber = this.getNpcPosCount();
        
        let eatNum = 0;
        foodMap.forEach((num, id) =>
            {
                eatNum += num;
            }
        );

        let objs = {};
        foodMap.forEach((num, id) =>
            {
                objs[id] = num;
            }
        );

        let guideLayerScript = this.guideLayerNode.getComponent('guideLayer');
        if (!!guideLayerScript) {
            guideLayerScript.updateStep(objs,false,false,eventSource);
        }

        taskManager.instance.upDateTask('eliminate_element', eatNum);

        let stepAddNumber = 0;

        for (let i =0; i < npcNumber; i ++)
        {
            foodMap.forEach((num, id) =>
            {
                if(id == this.npcDatas[i].wantEat)
                {
                    let allNum = this.npcDatas[i].wantNum;
                    let finishNum = this.npcDatas[i].finishNum;
                    let numLeft = allNum - finishNum;
        
                    if (numLeft > 0)
                    {
                        if(numLeft > num)
                        {
                            //此order没有结束
                            this.npcDatas[i].finishNum += num;
                        }
                        else
                        {
                            //此order结束了，需要加上order的金币数量
                            curCoinsNum += this.npcDatas[i].coins;
        
                            this.npcDatas[i].finishNum = allNum; 
        
                            //准备切换npc订单
                            this.pushToNpcFinished(i);
    
                            taskManager.instance.upDateTask('finish_order', 1);
                            
                            if (!this.bossMode) {
                                soundManager.instance.playSound('complete_order');

                                if (!!guideLayerScript) {
                                    guideLayerScript.updateStep(objs, true);
                                }

                            }else{
                                
                                soundManager.instance.playSound('complete_boss');

                                if (!!guideLayerScript) {
                                    guideLayerScript.updateStep(objs, false, true);
                                }
                                /*
                                let nextNpc = this.npcQueue.getTop();
                                if (!nextNpc.isBoss) {
                                    soundManager.instance.playSound('complete_boss');
                                }
                                */
                            }


                            if (this.bossMode) {
                                let nextNpc = this.npcQueue.getTop();
                                if (nextNpc.isBoss != this.bossMode) 
                                {
                                    //体力上限加1
                                    this.stepNumberLeftMax ++;

                                    let soupCompent = this.soupBarNode.getComponent('soupBar');
                                    if (!!soupCompent) {
                                        soupCompent.addStepLeftMax(1);
                                    }

                                    let limitMaxCompent = this.addStepLimitNode.getComponent('panelMaxStepAdd');
                                    if (!!limitMaxCompent) {
                                        limitMaxCompent.showStepLimitAddPanel();
                                    }
                                }
                            }

                            stepAddNumber ++;
                        }
    
                        let key0 = this.npcDatas[i].npcUniqueId.toString();

                        msg0.details[key0] = {};
                        msg0.details[key0].pieces = {};
                        msg0.details[key0].pieces[id.toString()] = num;

                        needSendToServer = true;
   
                        let avatarComp = this.npcDatas[i].npcNode.getChildByName('npc_node').getComponent('avatar');
                        if(!!avatarComp)
                        {
                            avatarComp.playEatting();
                        }
                    }
                }
            }
            );
        }

        if (stepAddNumber > 0) {
            this.addStepLeft(stepAddNumber);
        }else {
            if(use_step)
            {
                this.addStepLeft(-1);

                let soupCompent = this.soupBarNode.getComponent('soupBar');
                if (!!soupCompent) {
                    soupCompent.addStepLeft(-1);
                }

                this.playNpcDisappointedAnimation();
            }
        }

        foodMap.forEach((num, id) =>
        {
            let config = configLoader.instance.getPieceConfig(id);
            
            if(config.Coins > 0)
            {
                curCoinsNum += config.Coins * num;
            }
        }); 

        game.instance.addCurrentStageCoins(curCoinsNum);
        this.getCoinsNumber += curCoinsNum;

        msg0.coins = this.getCoinsNumber;
        
        if (needSendToServer) {
            webRequestManager.instance.requestClearPiecesNew(msg0,
                (rsp)=>{
                    this.addNpcQueueFromServer(rsp.npcs);
                }
            );
        }

        if (curCoinsNum > 0) {
            //game.instance.addCoins(msg0.coins);
            
            taskManager.instance.upDateTask('earn_coin',curCoinsNum);

            this.flyObjsNode.active = true;
            let objsFly = this.flyObjsNode.getComponent('flyObjects');
            if (!!objsFly) {
                objsFly.flyCoins(flyCoinsPos, curCoinsNum);
            }            
        }
    }
    ,

    addShareReward : function(times)
    {
        game.instance.addCurrentStageCoins(this.getCoinsNumber * (times - 1));
        this.getCoinsNumber *= times;
    }
    ,

    playAddHeartAnimation : function(idx)
    {
        this.flyObjsNode.active = true;
        let objsFly = this.flyObjsNode.getComponent('flyObjects');
        if (!!objsFly) {

            let w = this.npcDatas[idx].npcNode.width *0.5;
            let h = this.npcDatas[idx].npcNode.height *0.7;

            let pos = this.npcDatas[idx].npcNode.convertToWorldSpace(new cc.Vec2(w,h));
            objsFly.flyHeart(pos);
        }
    }
    ,

    stageComplete : function()
    {
        taskManager.instance.upDateTask('play_level', 1);
        taskManager.instance.flushTask();

        this.stageCompleteFlag = true;
        this.stepZeroCheckTime = 0;
        this.soupBarNode.active = false;
    }
    ,

    showSharePanel : function(times)
    {
        var self = this;
        this.sharedPanelDisplay = true;

        ui.instance.showSharePanel(times, (eventName)=>{
                
            if (eventName == 'close') {
                self.showCompletePanel();
                self.sharedPanelDisplay = false;
            }
            else if (eventName == 'sharedSuccess') {
                
                self.continueToPlay();
                self.shareTimes ++;
                self.sharedPanelDisplay = false;
            }
        });
    }
    ,

    onStageFinishCheck : function()
    {
        
        if (this.stepNumberLeft <= 0 
            && this.canvasNode.getChildByName('completePanel') == null
            && this.sharedPanelDisplay == false) {

            let config = configLoader.instance.getShareConfig(1);
            if (!!config) {
                if (config.ShareQueue.length == 0) {
                    this.showCompletePanel();
                }
                else {
                    if (this.shareTimes < config.ShareQueue.length) {
                        this.showSharePanel(this.shareTimes);
                    }
                    else {
                        this.showCompletePanel();
                    }
                }
            }
        }
    }
    ,

    sendFinish : function()
    {
        let event = new cc.Event.EventCustom('stage_finish', true);
        this.node.dispatchEvent(event);

        let msg0 =
        {
            details: {}, 
            coins : this.getCoinsNumber,
            isEnd : true
        };

        this.stageComplete();

        webRequestManager.instance.requestClearPiecesNew(msg0,
            (rsp)=>{
                this.addNpcQueueFromServer(rsp.npcs);
            }
        );
    }
    ,

    continueToPlay : function()
    {
        let config = configLoader.instance.getShareConfig(1);
        let stepVal = config.RewardValue;

        this.addStepLeft(stepVal);

        let soupCompent = this.soupBarNode.getComponent('soupBar');
        if (!!soupCompent) {
            soupCompent.addStepLeft(stepVal);
        }
    }
    ,

    showCompletePanel : function()
    {
        let num = this.getCoinsNumber;
        game.instance.updateStageHistory(this.stageId, num);
        
        let completePanel = cc.instantiate(this.completePrefab);
        let comp = completePanel.getComponent('completePanel');
        if (!!comp) {

            let params = {
                coins : num,
                stageId : this.stageId
            };

            comp.initUI(params);
        }

        this.canvasNode.addChild(completePanel,0,'completePanel');
    }
    ,

    onBgmButton : function()
    {
        let bgmEnabled = soundManager.instance.isBgmEnabled();
        bgmEnabled = !bgmEnabled;
        soundManager.instance.setBgmEnabled(bgmEnabled);

        this.bgmEnabledFlag.active = !bgmEnabled;

        soundManager.instance.playSound('button');
    }
    ,

    onEffectSoundButton : function()
    {
        let effectSoundEnabled = soundManager.instance.isSoundEnabled();
        effectSoundEnabled = !effectSoundEnabled;
        soundManager.instance.setSoundEnabled(effectSoundEnabled);

        this.effectSoundEnabledFlag.active = !effectSoundEnabled;

        soundManager.instance.playSound('button');
    }
    ,

    onStirButton : function()
    {
        cc.log('====大风车按钮 -- gameLevel.js onStirButton');

        soundManager.instance.playSound('button');
    }
    ,

    onCloseSettingButton : function()
    {
        if (this.menuOpened == true) {
            this.menuButtons.active = false;
            this.menuMaskLayer.active = false;
            this.menuOpened = false;
        }
    }
    ,

    onSettingButton : function()
    {
        if (this.menuOpened) {
            this.menuButtons.active = false;
            this.menuMaskLayer.active = false;
        }
        else {
            this.menuButtons.active = true;
            this.menuMaskLayer.active = true;
        }

        this.menuOpened = !this.menuOpened;

        soundManager.instance.playSound('button');
    }
    ,

    onOpenFinish : function()
    {
        this.showCompletePanel();
    }
    ,

    onQuitStage : function()
    {
        soundManager.instance.playSound('button');
        this.showCompletePanel();
    }
    ,

    reset : function()
    {
        
    }
    ,

    testNextNpcButtonClick : function()
    {
        this.getNextNpc(this.npcDatas[0].npcId);
    }
    ,

    testEatButtonClick : function()
    {
        let eatNum = 1;
        let eatKind = this.npcDatas[0].wantEat;

        this.eatting(eatKind, eatNum);
    },

    pushToNpcFinished : function(npcId)
    {
        this.finishedNpcId.push_back(npcId);
    }
    ,

    checkNpcStatus : function()
    {
        let si = this.finishedNpcId.size();
        for (let i = 0; i < si; i ++)
        {
            let finishedNpcId = this.finishedNpcId.get(i);
            this.getNextNpc(finishedNpcId);
        }

        this.finishedNpcId.clear();
    }
    ,

    getNpcNode : function (npcId, lazyInit = false)
    {
        let npc = null;
        if (this.npcPool.size() > 0) {
            npc = this.npcPool.get();

            if (!lazyInit) {
                let npcNode = npc.getChildByName('npc_node');
                let npcComp = npcNode.getComponent('avatar');

                if (!!npcComp) {
                    npcComp.init(npcId);
                }
            }

        } else {
            npc = this.createNpcNode(npcId);
        }

        return npc;
    }
    ,

    npcAddToNode : function (node,npcNumber,npcPos)
    {
        let w = this.npcAreaNode.width;
        let sx = - w*0.375;
        let pw = w*0.25;

        if (npcNumber == 1) {
            sx = 0;
            pw = 0;
        }else if (npcNumber == 2) {
            sx = - w*0.25;
            pw = w*0.5;
        }else if (npcNumber == 3) {
            sx = - w*0.33;
            pw = w*0.33;
        }

        if (!!node) {
            node.setAnchorPoint(0.5,0.0);
            let nodePos = cc.v2(sx + (npcPos) * pw, - 15);
            node.setPosition(nodePos);        
            this.npcAreaNode.addChild(node);

            if (!!this.npcDatas && !!this.npcDatas[npcPos].plateNode) {
                this.npcDatas[npcPos].plateNode.x = sx + (npcPos) * pw;
            }
        }

        return node;
    }
    ,

    update (dt) {

        var manager = cc.director.getPhysicsManager();
        manager.enabledAccumulator = true;
        manager.FIXED_TIME_STEP = dt;
        manager.VELOCITY_ITERATIONS = 3;
        manager.POSITION_ITERATIONS = 3;

        if (!this.npcRefreshFromServer) {
            return;
        }

        /*
        if (this.stageCompleteFlag == true) {
            this.stepZeroCheckTime += dt;

            if (this.stepZeroCheckTime >= 3 &&
                this.stepZeroCheckTime < 10) {
                this.onStageFinishCheck();
                
                this.stepZeroCheckTime = 10;
            }

            return;
        }
*/
        let npcNumber = this.getNpcPosCount();

        for (let i = 0;i <npcNumber; i ++) {
            let allNum = this.npcDatas[i].wantNum;
            let finishNum = this.npcDatas[i].finishNum;
            let numLeft = allNum - finishNum;

            if (this.npcDatas[i].labDisplay  != numLeft || this.needUpdated) {

                let node = this.plateAreaNode.getChildByName("npc_" + i);
                let leftDishNode = node.getChildByName('dishLeftNum');
                let dishNum = leftDishNode.getComponent('cc.Label');
                
                let dispNum = numLeft;
                if (!this.needUpdated && this.npcDatas[i].labDisplay > numLeft) {
                    dispNum = utils.numberStepToNumber(this.npcDatas[i].labDisplay, numLeft);
                }

                dishNum.string = Math.floor(dispNum);
                this.npcDatas[i].labDisplay  = dispNum;
            }
        }

        //let soupCompent = this.soupBarNode.getComponent('soupBar');
        //if (!!soupCompent) {
            //soupCompent.updateStepLeftNumber(this.stepNumberLeft);
        //}

        this.checkNpcStatus();

        let potMask = this.potContainer.getChildByName('maskLayer');
        if (!!potMask) {
            potMask.active = (this.stepNumberLeft <= 0);
        }

        this.needUpdated = false;

        //cc.log('SIN(90) =' + Math.sin(90));
    },

    onShowTaskPanel :function()
    {
        ui.instance.showTaskPanel();
    }
});
