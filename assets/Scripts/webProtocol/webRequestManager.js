import {requests, responses} from "proto";
var Router = require('router');
var HttpRequest = require('httpRequest');
var Utils = require('utils');

var webRequestManager = cc.Class({
    extends: cc.Component,

    properties: {
        url: null,
        requestNum: null,
        token: null,
    },

    statics: {
        instance : null
    },

    onLoad: function () {
        Router.instance;
        webRequestManager.instance = this;
    },

    initWebServer : function(urlAddress)
    {
        let ver = Utils.getVersion();
        if (ver.patch%2 == 0) {
            //stable
            this.url = 'https://hotpot-cn.campfiregames.cn';
        }
        else {
            //upgrade
            this.url = 'https://hotpot-cn-upgrade.campfiregames.cn';
        }

        //this.url = 'http://10.0.74.13:36000';
    },

    sendMsg: function(router, data, success, failed){
        let name = Router.instance.getNameByRouter(router);
        if (!name){
            cc.warn('send message failed, request name invalid.');
            return;
        }

        if (!!this.url) {

        }
        else {
            this.initWebServer();
        }

        cc.log('===== REQUEST NAME =' + name + '================= data =');
        cc.log(data);
        
        let message = requests[name].create(data);
        let messageBuf = requests[name].encode(message).finish();
        let httpClient = new HttpRequest(this.url);

        cc.log('URL =' + this.url);
        
        httpClient.setUrl(this.url);
        httpClient.setMaxRetryTime(3);
        httpClient.setTimeout(10000);
        httpClient.setToken(this.token);
        httpClient.setRequestNum(this.requestNum++);
        httpClient.sendProto(router, 'requests.' + name, messageBuf, (buf) => {
            let rsp = responses[name].decode(buf);
            Utils.invokeCallback(success, rsp);
        }, failed);
    },

    requestGetPmConfig : function(callback = null)
    {
        this.sendMsg(Router.instance.FetchPmConf, {}, (response) => {
            if (!!callback) {
                Utils.invokeCallback(callback, response);
            }
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    }
    ,

    requestLogin : function (params , callback, error_callback)
    {
        this.sendMsg(Router.instance.Login, params,
             (response) => {
            this.token = response.token;
            Utils.invokeCallback(callback, response);
        }, (errorResponse) => {
            cc.log(errorResponse);
            if (!!error_callback) {
                Utils.invokeCallback(error_callback, errorResponse);
            }
        });
    },

    requestStartGameLevel : function (stage, callback = null)
    {
        this.sendMsg(Router.instance.BeginStage, {
            "stageId": stage,
        }, (response) => {
            Utils.invokeCallback(callback, response);
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    },

    requestClearPiecesNew : function (msg, callback = null)
    {
        this.sendMsg(Router.instance.ClearPieces, msg, (response) => {
            Utils.invokeCallback(callback, response);
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    },

    requestCostSoup : function (step, callback = null)
    {
        this.sendMsg(Router.instance.CostSoup, {
            soup : step,		// 整形,消耗的汤数
        }, (response) => {
            Utils.invokeCallback(callback, response);
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    },

    requestGetStageHistory : function(callback = null)
    {
        this.sendMsg(Router.instance.StageHistory, {
        }, (response) => {
            Utils.invokeCallback(callback, response);
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    },

    requestBuy : function(itemId, callback = null)
    {
        this.sendMsg(Router.instance.BuyStoreItem, {
            storeId : itemId
        }, (response) => {
            Utils.invokeCallback(callback, response);
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    },

    requestCollectCoins : function(coinsNum ,isShare, callback = null)
    {
        this.sendMsg(Router.instance.CollectCoins, {
            coins : coinsNum,
            hasShare : isShare
        }, (response) => {
            Utils.invokeCallback(callback, response);
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    },

    requestGetDailyTask :function(callback = null)
    {
        this.sendMsg(Router.instance.TaskList, {
        }, (response) => {
            Utils.invokeCallback(callback, response);
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    },

    requestIncrementTask :function(tskId, val, callback = null)
    {
        this.sendMsg(Router.instance.IncrementTaskProgress, {
            taskId : tskId,
            incVale : val
        }, (response) => {
            Utils.invokeCallback(callback, response);
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    },

    requestFetchTaskReward :function(tskId, callback = null)
    {
        this.sendMsg(Router.instance.FetchTaskReward, {
            taskId : tskId
        }, (response) => {
            Utils.invokeCallback(callback, response);
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    },

    requestWechatAuth :function(val, callback = null)
    {
        this.sendMsg(Router.instance.WechatAuth, {
            code : val
        }, (response) => {
            Utils.invokeCallback(callback, response);
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    },

    requestGetClientInfo : function(callback = null)
    {
        this.sendMsg(Router.instance.ClientInfoGet, {
        }, (response) => {
            Utils.invokeCallback(callback, response);
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    },

    requestSetClientInfo : function(params,callback = null)
    {
        this.sendMsg(Router.instance.ClientInfoSet, params, (response) => {
            Utils.invokeCallback(callback, response);
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    }
    ,

    requestShare : function(val, callback = null)
    {
        this.sendMsg(Router.instance.Share, {
            shareType : val
        }, (response) => {
            if (!!callback) {
                Utils.invokeCallback(callback, response);
            }
        }, (errorResponse) => {
            cc.log(errorResponse);
        });
    }
});
