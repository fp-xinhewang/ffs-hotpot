import {netutils} from "proto";
var Md5 = require("md5");
var Utils = require('utils');
var base64Arraybuffer = require('base64-arraybuffer');

var HttpRequest = cc.Class({
    extends: cc.Component,

    properties: {
        _url            : null,                     // 地址
        _uri            : null,                     // uri
        _router         : null,                     // 路由
        _data           : null,                     // 数据
        _token          : null,                     // token
        _data           : null,                     // 数据
        _reqNum         : 0,                        // 请求数
        _retryTime      : 0,                        // 重试次数
        _maxRetryTime   : 0,                        // 最大重试次数
        _timeout        : 0,                        // 超时时间,ms
        _time           : 0,                        // 时间
    },

    setUrl: function(url){
        this._url = url;
    },

    setMaxRetryTime(maxRetryTimes) {
        this._maxRetryTime = maxRetryTimes;
    },

    setToken: function(token){
        if (!!token){
            this._token = token;
        }
    },

    setRequestNum: function(reqNum){
        this._reqNum = reqNum;
    },

    setTimeout: function(timeout){
        this._timeout = timeout;
    },

    needRetry: function(){
        return this._retryTime < this._maxRetryTime;
    },

    _elapse: function(msg){
        cc.log('%s elaspe time:%dms', msg, Date.now() - this._time);
        this._time = Date.now();
    },

    _send: function(router, buf, success, fail){
        this._time = Date.now();
        let url = this._url + router;
        
        let xhr = cc.loader.getXMLHttpRequest();
        if (this._timeout > 0){
            xhr.timeout = this._timeout;
        }
        xhr.responseType = "arraybuffer";
        xhr.onload = (e)=> {
            var arrayBuffer = xhr.response;
            if (arrayBuffer) {
                let uint8Array = new Uint8Array(arrayBuffer);
                let rsp = netutils.Response.decode(uint8Array);
                if (rsp.raw.uri == "netutils.ErrorResponse") {
                    Utils.invokeCallback(fail, netutils.ErrorResponse.decode(rsp.raw.raw));
                    return;
                }
                this._elapse(router);
                Utils.invokeCallback(success, rsp.raw.raw);
            }
            else {
                Utils.invokeCallback(fail, netutils.ErrorResponse.create({
                    msg:"UNKNOWN",
                    code:netutils.ErrorCode['UNKNOWN'],
                 }));
            }
        }
        xhr.onerror = (e)=>{
            if (this.needRetry()){
                this._retryTime++;
                cc.warn('retry request, router:%s, retryTimes:%d', router, this._retryTime);
                this._send(router, buf, success, fail);
            } else {
                Utils.invokeCallback(e, null);
            }
        }
        xhr.ontimeout = (e)=>{
            if (this.needRetry()){
                this._retryTime++;
                cc.warn('retry request, router:%s, retryTimes:%d', router, this._retryTime);
                this._send(router, buf, success, fail);
            } else {
                Utils.invokeCallback(e, null);
            }
        };
        
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/x-protobuf");
        xhr.setRequestHeader("Token", this._token);
        xhr.send(base64Arraybuffer.encode(buf));
    },

    sendProto: function(router, uri, data, success, fail){
        let now = Date.now();
        let rawAny = netutils.RawAny.create({uri:uri, raw:data});
        let reqData = {reqNum:this._reqNum, time:now, raw:rawAny};
        let req = netutils.Request.create(reqData);
        let buf = netutils.Request.encode(req).finish();
        this._send(router, buf, success, fail)
    },
});