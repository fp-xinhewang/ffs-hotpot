import {requests} from 'proto';

var Router = cc.Class({
    extends: cc.Component,

    statics: {
        instance : null
    },

    ctor: function(){
        this._mapping();
    },

    onLoad: function () {
        webRequestManager.instance = this;
    },

    properties: {
        RouterMapping:          null,
        Login:                  '/game/login',
        BeginStage:             '/game/stage/begin',
        ClearPieces:            '/game/stage/clear_pieces',
        CostSoup:               '/game/stage/cost_soup',
        StageHistory:           '/game/stage/history',
        BuyStoreItem:           '/game/store/buy',
        CollectCoins:           '/game/player/collect_coins',
        TaskList:               '/game/task/list',
        IncrementTaskProgress:  '/game/task/increment',
        FetchTaskReward:        '/game/task/fetch_reward',
        WechatAuth:             '/wx/auth',
        ClientInfoGet:           '/game/client_info/get',
        ClientInfoSet:           '/game/client_info/put',
        ClientInfoDel:           '/game/client_info/del',
        FetchPmConf:             '/game/fetch_pm_conf',
        Share:                   '/game/player/share'
    },

    _mapping: function(){
        this.RouterMapping = {};
        this.RouterMapping[this.Login]                  = 'Login';
        this.RouterMapping[this.BeginStage]             = 'BeginStage';
        this.RouterMapping[this.ClearPieces]            = 'ClearPieces';
        this.RouterMapping[this.CostSoup]               = 'CostSoup';
        this.RouterMapping[this.StageHistory]           = 'StageHistory';
        this.RouterMapping[this.BuyStoreItem]           = 'BuyStoreItem';
        this.RouterMapping[this.CollectCoins]           = 'CollectCoins';
        this.RouterMapping[this.TaskList]               = 'ListTasks';
        this.RouterMapping[this.IncrementTaskProgress]  = 'IncrementTask';
        this.RouterMapping[this.FetchTaskReward]        = 'FetchTaskReward';
        this.RouterMapping[this.WechatAuth]             = 'WeChatAuth';
        this.RouterMapping[this.ClientInfoGet]           = 'ClientStorageGet';
        this.RouterMapping[this.ClientInfoSet]           = 'ClientStoragePut';
        this.RouterMapping[this.ClientInfoDel]           = 'ClientStorageDel';
        this.RouterMapping[this.FetchPmConf]             = 'FetchPmConf';
        this.RouterMapping[this.Share]                   = 'Share';
    },

    getNameByRouter: function(router){
        let name = this.RouterMapping[router];
        if (!name) {
            cc.warn('error router:%s', router);
        }
        return name;
    },
});

Router.instance = new Router();
module.exports = Router;