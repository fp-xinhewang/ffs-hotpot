var utils = require('utils');

var soundManager = cc.Class({
    extends: cc.Component,

    properties: {

    },

    statics: {

        instance : null
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        soundManager.instance = this;
        this.bgmMute = false;
        this.bgmLoop = true;
        this.soundMute = false;
        this.bgmName = '';
    },

    playSound : function(strName) {

        if (this.soundMute == true) {
            return;
        }

        let node = this.node.getChildByName(strName);
        if (!!node) {

            let audioSource = node.getComponent(cc.AudioSource);
            if (!!audioSource) {
                audioSource.play();
            }
        }
    },

    isSoundEnabled : function()
    {
        return !this.soundMute;
    }
    ,

    setSoundEnabled : function(val)
    {
        this.soundMute = !val;
    }
    ,

    isBgmPlaying : function()
    {
        if (!!this.bgm)
        {
            return this.bgm.isPlaying();
        }
    }
    ,
    
    isBgmEnabled : function()
    {
        return !this.bgmMute;
    }
    ,

    setBgmEnabled : function(val)
    {
        this.bgmMute = !val;
        if (!!this.bgm) {
            this.bgm.mute = this.bgmMute;
        }
    }
    ,

    setBgmLoop : function(val)
    {
        this.bgmLoop = val;
        if (!!this.bgm) {
            this.bgm.loop = this.bgmLoop;
        }
    }
    ,

    playBgm : function(strName) {

        if (strName == null) {
            if (!!this.bgm) {

                this.bgm.play();

                this.bgm.loop = this.bgmLoop;
                this.bgm.mute = this.bgmMute;

                return this.bgm;
            }
        }

        this.bgmName = strName;
        this.bgm = utils.createMusic(this.node,'bgm/' + strName,true);
        this.bgm.loop = this.bgmLoop;
        this.bgm.mute = this.bgmMute;
        this.bgm.play();
        
        return this.bgm;
    },

    stopBgm : function()
    {
        if (!!this.bgm) {
            this.bgm.stop();
        }
    },

    getBgmName : function()
    {
        return this.bgmName;
    }

    // update (dt) {},
});
