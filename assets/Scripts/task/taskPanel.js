var soundManager = require('soundManager');
var taskManager = require('taskManager');
var game = require('game');

cc.Class({
    extends: cc.Component,

    properties: {
        maskLayer : cc.Node,
        root : cc.Node,
        taskDetailNode : cc.Node,
        taskDetailView : cc.Node,
        taskDetailNodeGuide : cc.Node,
        taskDetailViewGuide : cc.Node,
        taskItemPrefab : cc.Prefab,
        tabButton0 : cc.Node,
        tabButton1 : cc.Node,
        scrollView0 : cc.Node,
        scrollView1 : cc.Node, 
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    show :function()
    {
        //this.node.x = 0;

        this.playOpen();

        if (!!this.items)
        {
            for (let i = 0; i <this.items.length; i ++)
            {
                let itemScript = this.items[i].getComponent('taskItem');
                itemScript.refreshTask();
            }
        }

        
        this.maskLayer.width = cc.visibleRect.width;
        this.maskLayer.height = cc.visibleRect.height;
    }
    ,
    
    onTouchTab0 : function()
    {
        this.curTab = 0;
        this.switchTab();
    }
    ,

    onTouchTab1 : function()
    {
        this.curTab = 1;
        this.switchTab();
    }
    ,

    switchTab : function()
    {
        let tab00 = this.tabButton0.getChildByName('btn0');
        let tab01 = this.tabButton0.getChildByName('btn1');
        
        let tab10 = this.tabButton1.getChildByName('btn0');
        let tab11 = this.tabButton1.getChildByName('btn1');
        
        tab00.active = (this.curTab == 0);
        tab01.active = (this.curTab != 0);

        tab10.active = (this.curTab == 1);
        tab11.active = (this.curTab != 1);

        this.scrollView0.active = (this.curTab == 0);
        this.scrollView1.active = (this.curTab == 1);
    }
    ,

    playOpen : function()
    {
        if (!!this.maskLayer) {
            this.maskLayer.runAction(cc.fadeTo(0.2,128));
        }

        if (!!this.root) {
            this.root.setScale(0.5);
            this.root.runAction(cc.scaleTo(0.2,1.0).easing(cc.easeBackOut()));
        }

        soundManager.instance.playSound('panel_open');
    }
    ,

    start () {

        this.curTab = 0;
        this.switchTab();

        this.initDailyTask();
        this.initGuideTask();
        this.playOpen();

        this.maskLayer.width = cc.visibleRect.width;
        this.maskLayer.height = cc.visibleRect.height;
    },

    initDailyTask : function()
    {
        this.items = [];

        let keys = Object.keys(taskManager.instance.taskList);
        let itemNum = keys.length;

        let nodeItem0 = cc.instantiate(this.taskItemPrefab);
        let w = nodeItem0.width;
        let h = nodeItem0.height;
        this.itemHeight = h;

        let vh = this.taskDetailView.height;

        let allHeight = itemNum * h;
        if (allHeight < vh) {
            allHeight = vh;
        }

        this.taskDetailNode.setContentSize(w, allHeight);
        let idx = 0;

        for (let taskId in taskManager.instance.taskList) {

            let item = cc.instantiate(this.taskItemPrefab);
            item.y = -idx * h;

            idx ++;

            let itemScript = item.getComponent('taskItem');
            itemScript.init(taskId,false);

            this.taskDetailNode.addChild(item);

            this.items[this.items.length] = item;
        }
    }
    ,

    initGuideTask : function()
    {
        let guideStatus = game.instance.getGuideStatus();

        let idx = 0;

        if (!!guideStatus)
        {
            let maxGuideStep = guideStatus.getMaxGuideStep();
            let maxLobbyGuideStep = guideStatus.getMaxLobbyGuideStep();

            let stepCount = maxGuideStep + maxLobbyGuideStep;

            let itemNum = stepCount;

            let nodeItem0 = cc.instantiate(this.taskItemPrefab);
            let w = nodeItem0.width;
            let h = nodeItem0.height;
            this.itemHeight = h;
    
            let vh = this.taskDetailViewGuide.height;
    
            let allHeight = itemNum * h;
            if (allHeight < vh) {
                allHeight = vh;
            }
    
            this.taskDetailNodeGuide.setContentSize(w, allHeight);

            for (let i = 0; i < stepCount; i ++) {

                let item = cc.instantiate(this.taskItemPrefab);
                item.y = -idx * h;

                idx ++;

                let itemScript = item.getComponent('taskItem');
                itemScript.init(i,true);

                this.taskDetailNodeGuide.addChild(item);

                this.items[this.items.length] = item;
            }
        }
    }
    ,

    onCloseButton : function()
    {
        soundManager.instance.playSound('button');

        this.node.destroy();
        //this.node.x = 10000;
    },
    // update (dt) {},
});
