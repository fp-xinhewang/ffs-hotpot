var webRequestManager = require('webRequestManager');

var taskManager = cc.Class({
    extends: cc.Component,

    properties: {

    },

    statics: {

        instance : null
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {

        taskManager.instance = this;
        this.gameInstance = null;
        this.taskList = {};
        this.currentTime = new Date();
        this.curDay = this.currentTime.getUTCDate();
        this.timeAcc = 0;
        this.taskQueue = {};

        this.taskTypeToName = {
            'finish_order' : '完成订单',
            'earn_coin' : '赚取金币',
            'eliminate_element' : '消除元素',
            'play_level' : '完成关卡'
        };
    },

    refreshDailyTask : function()
    {
        webRequestManager.instance.requestGetDailyTask(
            (rsp)=>{

                cc.log('----------------- DAILY TASK ----------------');

                cc.log(rsp);

                taskManager.instance.setTaskList(rsp.tasks);
            }
        );
    }
    ,

    isTaskClaimed : function(taskId)
    {
        if (!!this.taskList[taskId]) {

            let num = 0;
            for (let rewardId in this.taskList[taskId].Reward)
            {
                num ++;
            }

            if (num == 0) {
                return true;
            }
        }
        return false;
    }
    ,

    isTaskFinished : function(taskId)
    {
        if (!!this.taskList[taskId]) {

            let retValue = this.taskList[taskId].finished;
            return retValue;
        }

        return false;
    }
    ,

    setTaskList : function(lists)
    {

        this.taskList = lists;

        for (let taskId in this.taskList)
        {
            if (this.taskList[taskId].Value == null) {
                this.taskList[taskId].Value = 0;
            }
            
            if (this.taskList[taskId].Value >= this.taskList[taskId].TargetValue) {
                this.taskList[taskId].finished = true;
            }else{
                this.taskList[taskId].finished = false;
            }
        }
    }
    ,

    upDateTask : function(taskType, increValue)
    {      
        if (!!this.taskQueue[taskType]) {
            this.taskQueue[taskType] += increValue;
        }
        else {
            this.taskQueue[taskType] = increValue;
        }

        for (let taskId in this.taskList)
        {
            if (this.taskList[taskId].TaskType == taskType &&
                this.taskList[taskId].finished == false) {
                this.taskList[taskId].Value += increValue;

                if (this.taskList[taskId].Value >= this.taskList[taskId].TargetValue) {
                    this.taskList[taskId].finished = true;
                }
            }
        }
    }
    ,

    flushTask : function()
    {
        for (let taskId in this.taskList)
        {
            let taskType = this.taskList[taskId].TaskType;

            if (!!this.taskQueue[taskType] &&
                this.taskList[taskId].finished == false) { 
                this.incrementTask(this.taskList[taskId].TaskId, this.taskQueue[taskType]);
            }
        }

        this.taskQueue = {};
    }
    ,

    incrementTask :function(taskId, incValue)
    {
        webRequestManager.instance.requestIncrementTask(taskId,incValue);
    }
    ,

    claimReward : function(taskId)
    {
        if (!!this.taskList[taskId]) {

            for (let rewardId in this.taskList[taskId].Reward)
            {
                if (rewardId == 19) {
                    if (!!this.gameInstance) {
                        let num = this.taskList[taskId].Reward[rewardId];
                        this.gameInstance.addCoins(num);

                        this.taskList[taskId].Reward = {};
                    }
                }
            }

            webRequestManager.instance.requestFetchTaskReward(taskId);
        }
    }
    ,

    start () {

    },

    update (dt) 
    {
        this.timeAcc += dt;
        if (this.timeAcc > 1) {

            let day0 = this.currentTime.getUTCDate();
            if (day0 != this.curDay)
            {
                this.flushTask();
                this.refreshDailyTask();
                this.curDay = day0;
            }

            this.timeAcc = 0;
        }

    },
});
