var taskManager = require('taskManager');
var game = require('game');

cc.Class({
    extends: cc.Component,

    properties: {

        taskName : cc.Label,
        taskCount : cc.Label,
        claimButton : cc.Button,
        buttonName : cc.Label,
        guideFinishedNode : cc.Node,
        guideUnFinishedNode : cc.Node,
        backNode0 : cc.Node,
        backNode1 : cc.Node,
        coinsNumber : cc.Label,
        coinsNode : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:
    init : function(tskId, isGuideTask)
    {
        this.taskId = tskId;
        this.isGuideTask = isGuideTask;

        if (this.isGuideTask == true) {
            this.refreshGuideTask();
        }
        else {
            this.refreshTask();
        }
    }
    ,

    refreshGuideTask : function()
    {
        this.coinsNode.active = false;
        this.claimButton.node.active = false;
        this.taskCount.node.active = false;
        this.coinsNumber.node.active = false;

        let guideStatus = game.instance.getGuideStatus();

        if (!!guideStatus)
        {
            let maxGuideStep = guideStatus.getMaxGuideStep();
            
            let finished = false;
            let taskName = '';

            if (this.taskId < maxGuideStep) {
                let curStep = guideStatus.getGuideStep();
                if (curStep > this.taskId) {
                    finished = true;
                }

                taskName = guideStatus.getStepDesc(this.taskId);
            }
            else{
                let curStep = guideStatus.getLobbyGuideStep();
                if (curStep + maxGuideStep > this.taskId) {
                    finished = true;
                }

                taskName = guideStatus.getLobbyStepDesc(this.taskId - maxGuideStep);
            }

            if (finished == true) {
                //完成了
                this.guideFinishedNode.active = true;
                this.guideUnFinishedNode.active = false;
            }
            else
            {
                this.guideFinishedNode.active = false;
                this.guideUnFinishedNode.active = true;
            }

            this.backNode0.active = !finished;
            this.backNode1.active = finished;
 
            this.taskName.string = taskName;
        }
    }
    ,

    refreshTask : function()
    {
        this.hasClaimed = taskManager.instance.isTaskClaimed(this.taskId);

        let taskObject = taskManager.instance.taskList[this.taskId];

        let tStr = taskObject.TaskType;
        tStr = taskManager.instance.taskTypeToName[tStr];

        this.taskName.string = tStr;

        let val0 = taskObject.Value;
        let val1 = taskObject.TargetValue;

        this.taskCount.string = val0.toString() + '/' + val1.toString();

        this.backNode0.active = true;
        this.backNode1.active = false;

        if (this.hasClaimed) 
        {
            this.claimButton.node.active = false;
            this.guideFinishedNode.active = true;
            this.backNode0.active = false;
            this.backNode1.active = true;
        }
        else if(taskManager.instance.isTaskFinished(this.taskId) == false)
        {
            this.claimButton.interactable = false;
        }

        for (let rewardId in taskObject.Reward)
        {
            if (rewardId == 19) {
                this.coinsCollect = taskObject.Reward[rewardId];
                this.coinsNumber.string = '+' + this.coinsCollect
            }
        }
    }
    ,

    onButtonClaim : function()
    {
        if (this.hasClaimed == false) {
            if (taskManager.instance.isTaskFinished(this.taskId)) {

                taskManager.instance.claimReward(this.taskId);

                if (this.coinsCollect != null)
                {
                    let lobbyScene = game.instance.getLobbyScene();
                    if (!!lobbyScene) {
                        lobbyScene.collectCoinsAnimation(this.claimButton.node);
                    }
                }

                this.hasClaimed = true;
                this.claimButton.node.active = false;
                this.guideFinishedNode.active = true;
                this.backNode0.active = false;
                this.backNode1.active = true;
                
                this.claimButton.interactable = false;
            }
        }
    }
    ,

    start () {

    },

    // update (dt) {},
});
