var game = require('game');

cc.Class({
    extends: cc.Component,

    properties: {

        guideTipPrefab : cc.Prefab,
        guideTipsNode : cc.Node,
        fingerNode : cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        
        let guideStatus = game.instance.getGuideStatus();
        if (!!guideStatus) {
            if (guideStatus.isInLobbyGuideStatus() == false) {
                this.node.active = false;
                return;
            }
        }
        
        this.guideTipLayer = cc.instantiate(this.guideTipPrefab);

        this.guideTipsNode.addChild(this.guideTipLayer);

        this.fingerNode.active = false;

        let tipScript = this.guideTipLayer.getComponent('guideTip');
        if (!!tipScript) {
            tipScript.setGuideMode(1);
            tipScript.guideLayerInstance = this;
        }

        this.stepFlags = [];

        this.step = guideStatus.getLobbyGuideStep();
        this.subStep = 0;

        this.scheduleFunc = null;

        this.delaySec = 0;
        this.tim = 0;
        this.timAcc = 0;
    },

    scheduleDelaySeconds : function()
    {
        this.timAcc += this.tim;
        if (this.timAcc > this.delaySec) {
            this.updateToNextSubStep();
        }
    }
    ,

    delay : function(sec)
    {
        this.delaySec = sec;

        this.scheduleFunction(this.scheduleDelaySeconds);
    }
    ,

    scheduleFunction : function(func)
    {
        this.scheduleFunc = func;
    }
    ,

    scheduleWaitForBuyPanelOpen : function()
    {
        let lobbySceneInstance = game.instance.getLobbyScene();
        if (!!lobbySceneInstance) {
            if (lobbySceneInstance.isStorePanelOpened()) {
                this.updateToNextSubStep();
            }
        }
    }
    ,

    scheduleWaitForBuyPanelClosed : function()
    {
        let lobbySceneInstance = game.instance.getLobbyScene();
        if (!!lobbySceneInstance) {
            if (lobbySceneInstance.isStorePanelOpened() == false) {
                this.updateToNextSubStep();
            }
        }
    }
    ,

    scheduleWaitForBuyDecoration : function()
    {
        let lobbySceneInstance = game.instance.getLobbyScene();
        if (!!lobbySceneInstance) {
            if (lobbySceneInstance.hasBuyDecoration()) {
                this.updateToNextSubStep();
            }
        }
    }
    ,

    scheduleWaitForCollectCoins : function()
    {
        let lobbySceneInstance = game.instance.getLobbyScene();
        if (!!lobbySceneInstance) {
            if (lobbySceneInstance.hasCollectCoins()) {
                this.updateToNextSubStep();
            }
        }        
    }
    ,

    updateToNextStep : function()
    {
        let guideStatus = game.instance.getGuideStatus();
        if (!!guideStatus) {

            let isInGuide = guideStatus.lobbyStepToNext();
            
            if (isInGuide) {

                this.step ++;
                this.subStep = 0;

                this.scheduleFunc = null;

                this.updateStep();
            }
            else {
                this.node.active =false;
            }
        }
    }
    ,

    updateToNextSubStep : function()
    {
        this.scheduleFunc = null;

        this.subStep ++;
        this.updateStep();
    }
    ,

    updateBuyDecoration : function()
    {
        if (this.subStep == 0)
        {
            this.subStep ++;
            
            this.delay(0.3);

            return;
        }

        if (this.subStep == 2) {

            this.subStep ++;

            let tipScript = this.guideTipLayer.getComponent('guideTip');
            if (!!tipScript) {
                tipScript.showTip(0);
                this.guideTipsNode.active = true;
            }
            
            this.showHandToBuyButton();

            this.scheduleFunction(this.scheduleWaitForBuyPanelOpen);
            return;
        }

        if (this.subStep == 4) 
        {
            this.subStep ++;
            this.hideHand();
            this.guideTipsNode.active =false;
            
            this.scheduleFunction(this.scheduleWaitForBuyDecoration);
            return;
        }

        if (this.subStep == 6)
        {
            this.subStep ++;

            this.updateToNextStep();
            return;
        }
    }
    ,

    updateCollectCoins : function()
    {
        if (this.subStep == 0)
        {
            this.subStep ++;
            
            this.delay(0.3);

            return;
        }

        if (this.subStep == 2)
        {
            this.subStep ++;

            let tipScript = this.guideTipLayer.getComponent('guideTip');
            if (!!tipScript) {
                tipScript.showTip(1);
                this.guideTipsNode.active = true;
            }

            this.showHandToPosButton();
            this.scheduleFunction(this.scheduleWaitForCollectCoins);
            return;
        }

        if (this.subStep == 4)
        {
            this.subStep ++;

            this.delay(10.0);
            this.updateToNextStep();
        }
    }
    ,

    updateStep : function()
    {
        if (this.step == 0) 
        {
            this.updateBuyDecoration();
            return;
        }

        if (this.step == 1)
        {
            this.updateCollectCoins();
            return;
        }
    }
    ,

    enableTouch : function()
    {

    }
    ,

    disableTouch : function()
    {

    }
    ,

    hideHand : function()
    {
        this.fingerNode.stopAllActions();
        this.fingerNode.active = false;
    }
    ,

    showHandToButton : function(val)
    {
        let lobbySceneInstance = game.instance.getLobbyScene();

        if (!!lobbySceneInstance) {
            let pos = lobbySceneInstance.getButtonPosition(val);

            this.fingerNode.stopAllActions();
            this.fingerNode.active = true;

            let pos1 = this.fingerNode.parent.convertToNodeSpace(pos);
            this.fingerNode.position = pos1;

            let fingerScript = this.fingerNode.getComponent('finger');
            if (!!fingerScript) {
                fingerScript.setAnimationEnabled(true);
            }
        }
    }
    ,

    showHandToBuyButton : function()
    {
        this.showHandToButton(1);
    }
    ,

    showHandToPosButton : function()
    {
        this.showHandToButton(2);
    }
    ,

    update (dt) {

        if (!!this.scheduleFunc) {
            this.tim = dt;
            this.scheduleFunc();
        }

        this.updateStep();
    },
});
