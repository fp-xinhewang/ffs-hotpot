var configLoader = require('configLoader');
var webRequestManager = require('webRequestManager');
var gio = require("gio-ming");
var utils = require('utils');

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    initAll () {

        this.inGuideStatus = true;
        this.inLobbyGuideStatus = true;

        this.guideStep = 0;
        this.lobbyGuideStep = 0;

        this.strDescArray =[
            '消除3个以上相同涮菜',
            '完成1次客人订单',
            '1次连接消除7个以上涮菜',
            '满足1个包场客人',
            '1次连接消除9个以上涮菜，产生炸弹',
            '使用1次炸弹'
        ];

        this.strDescArray2 = [
            '购买1个餐厅装饰品',
            '点击收银台收集金币'
        ];

        this.maxGuideStep = this.strDescArray.length;
        this.maxLobbyGuideStep = this.strDescArray2.length;
    },

    getMaxGuideStep : function()
    {
        return this.maxGuideStep;
    }
    ,

    getMaxLobbyGuideStep : function()
    {
        return this.maxLobbyGuideStep;
    }
    ,

    updateCollectCoins : function()
    {
        if (this.lobbyGuideStep == 1 &&
            this.isInLobbyGuideStatus == true) {

            this.lobbyStepToNext();
        }
    }
    ,

    getStepDesc : function(idx)
    {
        if (idx < 0) {
            idx = 0;
        }

        if (idx >= this.strDescArray.length) {
            idx = this.strDescArray.length - 1;
        }

        let retStr = this.strDescArray[idx];

        return retStr;
    }
    ,

    getLobbyStepDesc : function(idx)
    {
        if (idx < 0) {
            idx = 0;
        }

        if (idx >= this.strDescArray2.length) {
            idx = this.strDescArray2.length - 1;
        }

        let retStr = this.strDescArray2[idx];

        return retStr;
    }
    ,

    getGuideStep : function()
    {
        return this.guideStep;
    }
    ,

    getLobbyGuideStep : function()
    {
        return this.lobbyGuideStep;
    }
    ,

    initGuideStatus : function(clientInfo)
    {
        this.initAll();

        this.clientInfo = null;
        if (!!clientInfo) {
            this.clientInfo = clientInfo;

            for (let key in clientInfo) {
                if (key == 'mainGuideStep') {
                    this.guideStep = Math.floor(clientInfo[key].Data);
                }
                else if (key == 'lobbyGuideStep') {
                    this.lobbyGuideStep = Math.floor(clientInfo[key].Data);
                }
            }
        }

        if (this.guideStep == 0) {
            if (utils.isWeChat()) {
                gio('startGuide', 
                    {'step' : (this.guideStep + 1)}
                );
            }else{
                cc.log('startGuide ' + (this.guideStep + 1));
            }
        }

        if (this.lobbyGuideStep == 0) {
            if (utils.isWeChat()) {
                gio('startGuide', 
                    {'step' : (this.maxGuideStep + this.lobbyGuideStep + 1)}
                );
            }else{
                cc.log('startGuide ' + (this.maxGuideStep + this.lobbyGuideStep + 1));
            }
        }

        this.inGuideStatus = (this.guideStep < this.maxGuideStep);
        this.inLobbyGuideStatus = (this.lobbyGuideStep < this.maxLobbyGuideStep);
    }
    ,

    sendToServer : function(keyStr,val)
    {
        let guideMap = 
        {
            'key' : keyStr,
            'val' : val + "",
            'timeout' : 0
        }

        cc.log('------------------ SET CLIENT----REQUEST:');
        cc.log(guideMap);

        webRequestManager.instance.requestSetClientInfo(guideMap, (rsp)=>{
            cc.log('------------------ SET CLIENT----RESPONSE:');
            cc.log(rsp);
        });
    }
    ,

    stepToNext : function()
    {
        if (this.inGuideStatus == false) {
            return;
        }

        if (utils.isWeChat()) {
            gio('finishGuide', 
                {'step' : (this.guideStep + 1)}
            );
        }else{
            cc.log('finishGuide ' + (this.guideStep + 1));
        }

        this.guideStep ++;

        if (this.guideStep > 5) {
            this.inGuideStatus = false;
        }else{
            if (utils.isWeChat()) {
                gio('startGuide', 
                    {'step' : (this.guideStep + 1)}
                );
            }else{
                cc.log('startGuide ' + (this.guideStep + 1));
            }
        }

        this.sendToServer('mainGuideStep', this.guideStep);

        if (this.guideStep > 6) {
            this.guideStep = 6;
        }

        return this.inGuideStatus;
    }
    ,

    lobbyStepToNext : function()
    {
        if (this.inLobbyGuideStatus == false) {
            return;
        }

        if (utils.isWeChat()) {
            gio('finishGuide', 
                {'step' : (this.maxGuideStep + this.lobbyGuideStep + 1)}
            );
        }else{
            cc.log('finishGuide ' + (this.maxGuideStep + this.lobbyGuideStep + 1));
        }

        this.lobbyGuideStep ++;      

        if (this.lobbyGuideStep > 1) {
            this.inLobbyGuideStatus = false;
        }
        else{
            if (utils.isWeChat()) {
                gio('startGuide', 
                    {'step' : (this.maxGuideStep + this.lobbyGuideStep + 1)}
                );
            }else{
                cc.log('startGuide ' + (this.maxGuideStep + this.lobbyGuideStep + 1));
            }
        }

        this.sendToServer('lobbyGuideStep',this.lobbyGuideStep);

        if (this.lobbyGuideStep > 2) {
            this.lobbyGuideStep = 2;
        }

        return this.inLobbyGuideStatus;
    }
    ,

    isInGuideStatus : function()
    {
        return this.inGuideStatus;
    }
    ,

    isInLobbyGuideStatus : function()
    {
        return this.inLobbyGuideStatus;
    }
    ,

    /*
    getReward : function(step = null)
    {
        if (step == null) {
            step = this.guideStep;
        }

        let config = configLoader.instance.getGuideConfig(step);
        if (!!config) {
            return config.Reward;
        }

        return null;
    }
    ,
    */

    getCurrentStep : function() {
        return this.guideStep;
    }
    ,

    getLobbyCurrentStep : function()
    {
        return this.lobbyGuideStep;
    }
    ,

    setStep : function()
    {
        
    }

    // update (dt) {},
});
