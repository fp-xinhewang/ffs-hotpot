var utils = require('utils');
var game = require('game');
var configLoader = require('configLoader');

cc.Class({
    extends: cc.Component,

    properties: {

        guideTipPrefab : cc.Prefab,
        guideTipsNode : cc.Node,
        fingerNode : cc.Node,
        maskLayer : cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
    
        let guideStatus = game.instance.getGuideStatus();
        if (!!guideStatus) {
            if (guideStatus.isInGuideStatus() == false) {
                this.node.active = false;
                return;
            }
        }
        
        this.tim = 0;
        this.guideTipLayer = cc.instantiate(this.guideTipPrefab);

        this.guideTipsNode.addChild(this.guideTipLayer);

        this.fingerNode.active = false;

        let tipScript = this.guideTipLayer.getComponent('guideTip');
        if (!!tipScript) {
            tipScript.setGuideMode(0);
            tipScript.guideLayerInstance = this;
        }

        this.stepFlags = [];

    },

    enableTouch2 : function()
    {
        this.maskLayer.active = false;
    }
    ,

    enableTouch : function()
    {
        let fade0 = cc.fadeOut(0.25);
        let callback = cc.callFunc(this.enableTouch2, this);
        let seq = cc.sequence(fade0, callback);

        this.maskLayer.runAction(seq);
    }
    ,

    disableTouch : function()
    {
        this.maskLayer.active = true;
        this.maskLayer.runAction(cc.fadeTo(0.25,0));
    }
    ,

    initialize : function()
    {
        this.showStep();
    }
    ,

    getSortArray : function(points)
    {
        let startIdx = -1;
        
        let left = 1000;
        let right = -1000;
        let bottom = 1000;
        let top = -1000;

        for (let i =0; i < points.length; i ++)
        {
            let x0 = points[i].x;
            let y0 = points[i].y;

            if (x0 < left) {
                left = x0;
            }

            if (x0 > right) {
                right = x0;
            }

            if (y0 < bottom) {
                bottom = y0;
            }

            if (y0 > top) {
                top = y0;
            }
        }

        let w0 = Math.abs(right - left);
        let h0 = Math.abs(top - bottom);

        if (w0 > h0)
        {
            let xMin = 1000;
            for (let i =0; i < points.length; i ++) {
                if (points[i].x < xMin) {
                    xMin = points[i].x;
                    startIdx = i;
                }
            }
        }
        else {
            let yMax = -1000;
            for (let i =0; i < points.length; i ++) {
                if (points[i].y > yMax) {
                    yMax = points[i].y;
                    startIdx = i;
                }
            }
        }

        let pointsArray = [];
        let pointsArrayHad = [];

        for (let i =0; i< points.length; i ++) {
            pointsArrayHad[i] = false;
        }

        if (startIdx > -1) {

            pointsArray[pointsArray.length] = points[startIdx];
            pointsArrayHad[startIdx] = true;

            while (pointsArray.length < points.length) {

                let dist = 1000;
                let nextIdx = -1;
                let basePoint = pointsArray[pointsArray.length -1];

                for (let i = 0; i < points.length; i ++) {
                    if (pointsArrayHad[i] == false) {

                        let d0 = Math.hypot(basePoint.x - points[i].x, basePoint.y - points[i].y);

                        if (d0 < dist) {
                            dist = d0;
                            nextIdx = i;
                        }
                    }
                }

                if (nextIdx > -1) {

                    pointsArray[pointsArray.length] = points[nextIdx];
                    pointsArrayHad[nextIdx] = true;
                }
            }
        }

        return pointsArray;
    }
    ,

    playHandMoveAnimation : function()
    {
        let posArray = [];
        let gameLevelScript = game.instance.getGameLevelIntance();
        if (!!gameLevelScript) {
            let objs = gameLevelScript.get3LinkedObjects();
            for (let i = 0; i < objs.length; i ++){
                let w = objs[i].width*0.5;
                let h = objs[i].height*0.5;

                let pos = objs[i].convertToWorldSpace(new cc.Vec2(w,h));

                posArray[posArray.length] = pos;
            }
        }

        let points = this.getSortArray(posArray);

        let pos0 = this.fingerNode.parent.convertToNodeSpace(points[0]);
        this.fingerNode.position = pos0;

        let moveToArray = [];

        moveToArray[moveToArray.length] = cc.fadeIn(0.001);
        moveToArray[moveToArray.length] = cc.moveTo(0.001, pos0);

        let pntLen = points.length;
        for (let i = 1; i < pntLen; i ++)
        {
            let pos = this.fingerNode.parent.convertToNodeSpace(points[i]);

            let moveTo = cc.moveTo(0.4, pos);
            moveToArray[moveToArray.length] = moveTo;
        }

        moveToArray[moveToArray.length] = cc.delayTime(0.8);
        moveToArray[moveToArray.length] = cc.fadeOut(0.001);

        var seq = cc.repeatForever(cc.sequence(moveToArray));

        let fingerScript = this.fingerNode.getComponent('finger');
        if (!!fingerScript) {
            fingerScript.setAnimationEnabled(false);
        }

        this.fingerNode.opacity = 0;
        this.fingerNode.active = true;
        this.fingerNode.runAction(seq);
    }
    ,

    playTouchBombAnimation : function()
    {
        let posArray = [];
        let gameLevelScript = game.instance.getGameLevelIntance();
        if (!!gameLevelScript) {

            let objs = gameLevelScript.getBombObjects();

            for (let i = 0; i < objs.length; i ++){
                let w = objs[i].width*0.5;
                let h = objs[i].height*0.5;

                let pos = objs[i].convertToWorldSpace(new cc.Vec2(w,h));

                posArray[posArray.length] = pos;
            }

            if (posArray.length == 0) {
                return;
            }

            cc.log(posArray);

            let pos = this.fingerNode.parent.convertToNodeSpace(posArray[0]);

            let fingerScript = this.fingerNode.getComponent('finger');
            if (!!fingerScript) {
                fingerScript.setAnimationEnabled(true);
            }

            this.fingerNode.opacity = 255;
            this.fingerNode.active = true;

            this.fingerNode.stopAllActions();
            this.fingerNode.position = pos;
        }
    }
    ,

    hideHand : function()
    {
        this.fingerNode.stopAllActions();
        this.fingerNode.active = false;
    }
    ,

    showStep : function()
    {
        let guideStatus = game.instance.getGuideStatus();
        if (!!guideStatus) {

            let curStep = guideStatus.getCurrentStep();

            let tipScript = this.guideTipLayer.getComponent('guideTip');
            if (!!tipScript) {
                tipScript.showTip(curStep);
                this.guideTipsNode.active = true;
            }

            if (curStep == 0) {

                let delayTime = cc.delayTime(3);
                let callbackFunc = cc.callFunc(this.playHandMoveAnimation,this);
                let seq = cc.sequence(delayTime, callbackFunc);

                this.node.runAction(seq);

            }
            else if (curStep == 5) {

                let delayTime = cc.delayTime(3.5);
                let callbackFunc = cc.callFunc(this.playTouchBombAnimation,this);
                let seq = cc.sequence(delayTime, callbackFunc);

                this.node.runAction(seq);
            }
        }
    }
    ,

    runToNextStep : function()
    {
        this.showStep();
    }
    ,

    /*
    getReward : function(reward)
    {
        let gameLevelScript = game.instance.getGameLevelIntance();

        if (!!gameLevelScript) {
            gameLevelScript.addGuideStepReward(reward);
        }
    }
    ,

    */
    finishAllGuide : function()
    {
        this.node.active = false;
    }
    ,

    finishCurrent : function(step)
    {
        let guideStatus = game.instance.getGuideStatus();
        if (!!guideStatus) {
            //let reward = guideStatus.getReward();
            //this.getReward(reward);
            
            let isInGuide = guideStatus.stepToNext();
            let curStep = guideStatus.getCurrentStep();

            this.node.stopAllActions();

            cc.log('----------------- STEP TO NEXT ----------- =' + curStep);
            if (isInGuide == true) {

                let delay = cc.delayTime(1.75);
                let callback = cc.callFunc(this.runToNextStep, this);
                
                let seq = cc.sequence(delay, callback);
                this.node.runAction(seq);
    
                this.stepFlags[step] = true;
            }
            else {
                let delay = cc.delayTime(1.75);
                let callback = cc.callFunc(this.finishAllGuide, this);

                let seq = cc.sequence(delay, callback);
                this.node.runAction(seq);
            }

            let tipScript = this.guideTipLayer.getComponent('guideTip');
            if (!!tipScript) {
                tipScript.finishCurrentStep();
            }
            
        }
    }
    ,

    updateStep : function(objs, finishOrder, finishBoss,eventSource)
    {        
        cc.log('------- GUIDE LAYER UPDATE STEP --------------');
        cc.log(objs);

        this.hideHand();
        
        let guideStatus = game.instance.getGuideStatus();
        if (!!guideStatus) {

            if (guideStatus.isInGuideStatus() != true) {
                return;
            }

            let curStep = guideStatus.getCurrentStep();

            cc.log('------- CURRENT STEP =' + curStep + '------------------');

            if (this.stepFlags[curStep] == true) {
                return;
            }

            if (curStep == 0) {
                //消除任意3个
                if (eventSource == 'hand') {
                    this.finishCurrent();
                }
                return;
            }
            else if (curStep == 1) {
                //完成客人订单
                if (finishOrder == true) {
                    this.finishCurrent();
                    return;
                }
            }
            else if (curStep == 2) {

                //消除7个得传染元素
                if (eventSource == 'hand') 
                {
                    let config = configLoader.instance.ConfigSpecConf.ConfigSpecs[0];
                    for(var key in objs){
                        if (objs[key] >= config.AutoMatchNum) {
                            this.finishCurrent();
                            return;
                        }
                    }
                }
            }
            else if (curStep == 3) {

                //完成boss订单
                if (finishBoss == true) {

                    this.finishCurrent();
                    return;
                }
            }
            else if (curStep == 4) {

                //消除9个得炸弹
                if (eventSource == 'hand') {
                    for(var key in objs){
                        if (objs[key] >= 9) {
                            this.finishCurrent();
                            return;
                        }
                    }
                }
            }
            else if (curStep == 5) {

                //使用炸弹
                if (eventSource == 'bomb') {
                    this.finishCurrent();
                    return;
                }
            }
        }
    }
    ,

    update (dt) {

        this.tim += dt;

        if (this.tim > 1.9 && this.tim < 10) {
            this.initialize();
            this.tim = 100;
        }
    },
});
