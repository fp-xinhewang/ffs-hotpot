

cc.Class({
    extends: cc.Component,

    properties: {

        font0 : cc.Node,
        font1 : cc.Node,
        tip0 : cc.Node,
        tip1 : cc.Node,
        tip2 : cc.Node,
        tip3 : cc.Node,
        tip4 : cc.Node,
        tip5 : cc.Node,
        lobbyTip0 : cc.Node,
        lobbyTip1 : cc.Node,
        back0 : cc.Node,
        back1 : cc.Node,
        stampNode : cc.Node,

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        this.guideMode = this.guideMode || 0;
        this.currentTip = this.currentTip || 0;
        this.disableTouch();
        this.guideLayerInstance = this.guideLayerInstance || null;
    }
    ,

    setGuideMode : function(val)
    {
        //0 : 消除场景新手
        //1 : 大厅场景新手

        this.guideMode = val;

        this.font0.active = (this.guideMode == 0);
        this.font1.active = (this.guideMode == 1);
    }
    ,

    getGuideStepNumber : function()
    {
        if (this.guideMode == 0) {
            return 6;
        }

        return 2;
    }
    ,

    enableTouch : function()
    {
        if (!!this.guideLayerInstance) {
            this.guideLayerInstance.enableTouch();
        }
    }
    ,

    disableTouch : function()
    {
        if (!!this.guideLayerInstance) {
            this.guideLayerInstance.disableTouch();
        }
    }
    ,

    showTip : function(stepIdx)
    {
        let guideNumber = this.getGuideStepNumber();

        if (stepIdx >= guideNumber) {
            return;
        }
        
        this.currentTip = stepIdx;

        let nodeName = 'tip';
        if (this.guideMode == 1) {
            nodeName = 'lobbyTip';
        }

        for (let i =0; i < guideNumber; i ++) {
            if (i == stepIdx) {
                this[nodeName + i].active = true;
                this[nodeName + i].scale = 1;
            }
            else {
                this[nodeName + i].active = false;
            }
        }

        this.node.stopAllActions();
        this.back1.stopAllActions();
        this.back0.stopAllActions();
        this[nodeName + this.currentTip].stopAllActions();

        this.stampNode.active = false;

        this.back0.opacity = 255;
        this.back1.opacity = 0;

        this.node.opacity = 0;
        this.node.y = 200;

        let delay0 = cc.delayTime(0.5);

        let move0 = cc.moveTo(0.7, new cc.Vec2(0,0)).easing(cc.easeBackOut());
        let fade0 = cc.fadeTo(0.25,255);
        let spawn0 = cc.spawn(move0,fade0);

        let timDisplay = 1.5;
        let delay1 = cc.delayTime(timDisplay);

        let move1 = cc.moveTo(0.7, new cc.Vec2(0,300)).easing(cc.easeBackOut());

        let seq = cc.sequence(delay0, spawn0, delay1, move1);
        this.node.runAction(seq);

        let dlyTim = timDisplay + 1.5;
        let delay2 = cc.delayTime(dlyTim);
        let fade1 = cc.fadeIn(0.25);
        let seq2 = cc.sequence(delay2,fade1);
        this.back1.runAction(seq2);

        let delay3 = cc.delayTime(dlyTim);
        let fade2 = cc.fadeOut(0.25);
        let seq3 = cc.sequence(delay3,fade2);
        this.back0.runAction(seq3);

        let w0 = this[nodeName + this.currentTip].width;
        let w1 = this.back1.width;

        let s0 = w1/w0*0.8;
        let delay4 = cc.delayTime(dlyTim);
        let scale0 = cc.scaleTo(0.25,s0);
        let seq4 = cc.sequence(delay4, scale0);
        this[nodeName + this.currentTip].runAction(seq4);

        this.disableTouch();

        let delay5 = cc.delayTime(dlyTim);
        let callback0 = cc.callFunc(this.enableTouch,this);
        let seq5 = cc.sequence(delay5, callback0);
        this.node.runAction(seq5);
    }
    ,

    finishCurrentStep : function()
    {
        this.node.stopAllActions();
        this.stampNode.stopAllActions();

        this.stampNode.active = true;

        this.stampNode.scale = 2.0
        this.stampNode.opacity = 0;
        let scale0 = cc.scaleTo(0.3, 1).easing(cc.easeBackOut());
        let fade0 = cc.fadeIn(0.1);
        let spawn = cc.spawn(scale0, fade0);

        this.stampNode.runAction(spawn);

        let delay0 = cc.delayTime(1.25);
        let fade1 = cc.fadeTo(0.25, 0);
        let move0 = cc.moveTo(0.7, new cc.Vec2(0,400)).easing(cc.easeBackOut());

        let spawn0 = cc.spawn(fade1, move0);
        let seq = cc.sequence(delay0, spawn0);
        this.node.runAction(seq);
    }
    ,

    hideTip : function()
    {
        this.node.active = false;
    }

    // update (dt) {},
});
