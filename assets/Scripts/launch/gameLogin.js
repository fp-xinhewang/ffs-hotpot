var utils = require('utils');
var webRequestManager = require('webRequestManager');
var game = require('game');
var DianDianSdk = require('diandian-sdk-min');
var gio = require("gio-ming");

var gameLogin = cc.Class({
    extends: cc.Component,

    properties: {

    },

    statics: {

        instance : null
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        gameLogin.instance = this;

        this.isWeChat = utils.isWeChat();
        this.wechatCode = '';

        this.responsePlayerData = null;

        this.resourceFinished = this.resourceFinished || false;
        this.webLoginFinished = this.webLoginFinished || false;
        this.configFinished = this.configFinished || false;

        this.launched = false;

        if (this.isWeChat == true) {
            this.ddSdk = new DianDianSdk.DianDianSdk().shareInstance();
            this.weChatLogin();
        }
    },

    startGameByHand : function()
    {
        if (this.isWeChat == true) {
            this.ddSdk = new DianDianSdk.DianDianSdk().shareInstance();
            this.weChatLogin();    
        }
        else {
            //gameLogin.instance.startGameByHand();
        }
    }
    ,

    onConfigLoaderFinished : function()
    {
        this.configFinished = true;
    }
    ,

    onResourceLoaderFinished : function()
    {
        this.resourceFinished = true;
    }
    ,

    onWebLoginFinished : function()
    {
        this.webLoginFinished = true;
    }
    ,

    startLocalLogin : function(name)
    {
        var self = this;

        let loginParams = {
            platform : 0,
            code : name
        };

        webRequestManager.instance.requestLogin(loginParams, 
            (rsp)=>{

                cc.log('----------------- LOGIN by local----------------');
                cc.log(rsp);

                self.responsePlayerData = rsp;
                self.webLoginFinished = true;
            }
        );
    }
    ,

    weChatLogin: function()
    {
        cc.log('OS = ' + cc.sys.os);

        var self = this;
        if (this.isWeChat) {

            cc.log('Start wechat login !!!');

            wx.login({
                success(res) {
                  if (res.code) {
                    // 发起网络请求

                    cc.log('======= WECHAT RES CODE = ' + res.code);

                    this.wechatCode = res.code;
                    self.startLogin(res.code);

                  } else {
                    console.log('微信登录失败！' + res.errMsg)
                  }
                }
            })
        }
    }
    ,
    
    startLogin : function(name)
    {
        var self = this;

        webRequestManager.instance.requestWechatAuth(name, (rsp)=>{

            cc.log('----------------- WX AUTH ----------------');
            cc.log(rsp);

            let openID = rsp.openid;
            let unionId = rsp.unionid || '';

            gio('identify', openID, unionId);
            self.startFunplusSDKLogin(openID, unionId);

        });
    }
    ,

    startFunplusSDKLogin : function(openID, unionId)
    {
        /*
        gameId: 字符串串串串，由Funplus分配 
        gameKey: 字符串串串串，由Funplus分配 
        isSandox: 布尔型，true表示sandbox，false表示production

        Game ID: 20017
        Game Key:593e4f38d75f6e752425b487e7031e53

        */

        var self = this;
        let isSandox = true;

        if (utils.isWeChat() == true){
            isSandox = false;
        }

        this.ddSdk.init('20017', '593e4f38d75f6e752425b487e7031e53', isSandox);

        /*

        openid：微信分配的 openid，（普通⽤用户的标识，对当前开发者帐号唯⼀一。⼀一个 openid 对应⼀一个公众 号） 
        unionid：微信分配的 unionid，获取不不到则传空字符串串（⽤用户统⼀一标识。针对⼀一个微信开放平台帐号下 
                        的应⽤用，同⼀一⽤用户的 unionid 是唯⼀一的。） 
        access_token: 登录时的签名算法，防⽌止被攻击, 计算⽅方式（openid + unionid +'de#$#sDEWkdfjsd'）

        */

        var openid = openID;
        var unionid = unionId;
        let str0 = openid + unionid + 'de#$#sDEWkdfjsd';

        var access_token = utils.md5(str0);

        cc.log('openid =' + openid + ' , unionid =' + unionid + ' ,access_token =' + access_token);

        function loginSuccessCallback(rsp) 
        {  
            cc.log('----------------- SDK LOGIN ----------------');
            cc.log(rsp);

            /*
            fpid:"42483"
            is_new:false
            session_key:"2af89mrRquwY5P5GGaTyPd97pTcnfYdGaaDR700dbllRYxUC1t"
            */

            self.startLoginViaFFSSDK( rsp.fpid, rsp.session_key);            
        }
        
        function loginFailCallback(res)
        {  
            cc.log(res);
        }
        
        this.ddSdk.loginWithSns(openid, unionid, access_token, loginSuccessCallback,loginFailCallback);
    }
    ,

    startLoginViaFFSSDK : function(fid,sessionKey)
    {
        let loginParams = {

            platform : 2,
            platformId : fid,
            code : sessionKey
        };

        cc.log('-----login by sdk params ------');
        cc.log(loginParams);

        var self = this;

        webRequestManager.instance.requestLogin(loginParams, 
            (rsp)=>{

                cc.log('----------------- LOGIN by funplus sdk----------------');
                cc.log(rsp);

                self.responsePlayerData = rsp;
                self.webLoginFinished = true;

                if (rsp.isNew){
                    gio('setUserId', rsp.player.base.uid);
                }
            },

            (error_rsp)=>{

                cc.log('------------------ 重新登陆 ------------------');
                this.ddSdk.logout();
                self.weChatLogin();
            }
        );
    }
    ,

    update (dt) 
    {
        if (this.resourceFinished == true && 
            this.webLoginFinished == true &&
            this.configFinished == true &&
                this.launched == false) 
        {
            this.launched = true;

            game.instance.startGame(this.responsePlayerData);
        }
    },
});
