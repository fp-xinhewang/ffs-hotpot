var utils = require('utils');
var gameLogin = require('gameLogin');
var webRequestManager = require('webRequestManager');

cc.Class({
    extends: cc.Component,

    properties: {
        root : cc.Node,
        prgressBar : cc.ProgressBar,
        startButton : cc.Button,
        title : cc.Label,
        loginNameEdit : cc.EditBox,
        serverUrlEdit : cc.EditBox,
        versionLabel : cc.Label
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        /*
            微信登陆
        */

        this.loginNameEdit.string = 'user_random_' + utils.randInt(10000);// + '_' + utils.randInt(1000);
        this.urlChoose = false;
        this.nameChoose = false;

        this.startButton.interactable = false;
        this.startButton.node.opacity = 0;

        this.prgressBar.progress = 0;

        let self = this;
        
        let ver = utils.getVersion();
        self.versionLabel.node.active = true;
        self.versionLabel.string = 'ver:' + ver.versionString;

        if (utils.isWeChat() == true) 
        {
            self.startButton.active = false;
            self.root.active = false;
            if (ver.major == 0 || ver.patch%2 != 0) {
                self.versionLabel.node.active = true;
            }else {
                self.versionLabel.node.active = false;
            }
        }


        cc.loader.loadResDir('./', 

            (completedCount, totalCount, item)=>
            {
                //cc.log('completedCount ='+completedCount+'totalCount='+totalCount);
                let t = completedCount/totalCount;
                if (t < 0.1) {
                    t = 0.1;
                }
                self.prgressBar.progress = t;
                let s = Math.floor(t * 100);
                self.title.string = s.toString() + '%';
            },

            ((error, resource, urls)=>{

                //cc.log('finished!!!!!!!!!!!!!!!!!');
                self.prgressBar.progress = 1;

                if (utils.isWeChat() == false) {
                    self.startButton.interactable = true;
                    self.startButton.node.runAction(cc.fadeIn(0.5));
                    self.prgressBar.node.runAction(cc.fadeOut(0.5));                
                    
                }
                else {
                    self.title.string = '正在进入火锅店...';
                }

                gameLogin.instance.onResourceLoaderFinished();
            }));

    },

    
    onSwitchName : function()
    {
        this.nameChoose = !this.nameChoose;
        if (this.nameChoose) {
            this.loginNameEdit.string = 'user_default';
        }
        else{
            this.loginNameEdit.string = 'user_random_' + utils.randInt(10000);// + '_' + utils.randInt(1000);
        }
    }
    ,

    onLoginButton : function() {
        
        if (utils.isWeChat() == true) { 
            gameLogin.instance.startGameByHand();
        }
        else {
            let loginName = this.loginNameEdit.string;
            cc.log('Login name = ' + loginName);
    
            if (loginName == '') {
                ui.instance.showAlert('名字不能为空!');
            }
            else {
    
                let url = this.serverUrlEdit.string;
                webRequestManager.instance.initWebServer(url);
                gameLogin.instance.startLocalLogin(loginName);
            }
        }
    }
    ,

    update (dt) 
    {

    }

});
