
function npcOrder(){
    
    this.npcId = 10001;
    this.npcUniqueId = 0;
    this.orderId = 1;
    this.wantEat = 0;
    this.wantNum = 10;
    this.coins = 0;
    this.finishNum = 0;
    this.npcNode = null;
    this.plateNode = null;
    this.isBoss = false;

    this.copyFrom = function(other)
    {
        this.npcId = other.npcId;
        this.npcUniqueId = other.npcUniqueId;
        this.orderId = other.orderId;
        this.wantEat = other.wantEat;
        this.wantNum = other.wantNum;
        this.coins = other.coins;
        this.finishNum = 0;
        this.isBoss = other.isBoss || false;
    }
}

function playerData()
{
    this.coins = 0,
    this.finishOrder = 0,
    this.exp = 0
    this.level = 0;
    this.uid = 0;
} 

function getCoins()
{
    return playData.coins;
}

function addCoins(num)
{
    playData.coins += num;

    return playData.coins;
}

module.exports = {

    npcOrder : npcOrder,
    playerData : playerData,
    getCoins : getCoins,
    addCoins : addCoins
}

