var md5Script = require('md5');

function getVersion()
{
    //major.minor.patch(主版本号.次版本号.修订版本号)
    let ma = 1;
    let min = 0;
    let pa = 0;
    let verStr = ma.toString() + '.' + min.toString() + '.' + pa.toString();

    let retStruct = {
        major : ma,
        minor : min,
        patch : pa,
        versionString : verStr
    }

    return retStruct;
};

var createSprite = function(localPath, callback = null)
{
    var node = new cc.Node();
    var call_back = callback;

    var sprite = node.addComponent(cc.Sprite);
    cc.loader.loadRes(localPath, cc.SpriteFrame, function (err, spFrame) {
        sprite.spriteFrame = spFrame

        if (call_back != null) {
            call_back();
        }
    });

    return node;
}

var createSpriteFromPlist = function(localPath , frameName)
{
    var node = new cc.Node();
    var sprite = node.addComponent(cc.Sprite);
    cc.loader.loadRes(localPath, cc.SpriteAtlas, function (err, atlas) {
        var frame = atlas.getSpriteFrame(frameName);
        sprite.spriteFrame = frame
    });

    return node;
}

var createMusic = function(node,localPath,autoPlay = true)
{    
    var audioSource = node.getComponent(cc.AudioSource);
    if (audioSource == null) {
        audioSource = node.addComponent(cc.AudioSource);
    }

    cc.loader.loadRes(localPath, cc.AudioClip, function (err, audioClip) {
        audioSource.clip = audioClip;
        if (autoPlay == true) {
            audioSource.play();
        }
    });

    return audioSource;

}
/*
 // TODO - DELME after 2.1
 js.value(cc.RawAsset, 'wasRawAssetType', function (ctor) {
    return ctor === cc.Texture2D ||
           ctor === cc.AudioClip ||
           ctor === cc.ParticleAsset ||
           ctor === cc.Asset;           // since 1.10, all raw asset will import as cc.Asset
});
*/

var createParticle = function(localPath)
{
    var node = new cc.Node();
    var particleNode = node.addComponent(cc.ParticleSystem);

    cc.loader.loadRes(localPath, cc.ParticleAsset, function (err, particle) {
        particleNode.file = particle;
    });

    particleNode.autoRemoveOnFinish = true;

    return node;
}

var createSpineNode = function( path, defaultAnimation, loop, skin, callback)
{
    if(null == loop)
    {
        loop = true;
    }

    var node = new cc.Node();
    var skData = node.addComponent(sp.Skeleton);

    cc.loader.loadRes(path, sp.SkeletonData, 
        function onProcess (completeCount, totalCount, item) {

        }, 
        function onComplete (err, res) {
            skData.skeletonData = res;
            skData.setAnimation(0, defaultAnimation, loop);
            skData.premultipliedAlpha = false;

            if(skin && skin != '')
            {
                skData.setSkin(skin);
            }

            if(callback)
            {
                callback(node, defaultAnimation);
            }
        }
    );

    return node;
}

var createLabel = function(str)
{
    var nodeLabel = new cc.Node();
    var label = nodeLabel.addComponent(cc.Label);
    label.string = str;

    return nodeLabel;
}

var isWeChat = function()
{
    if (cc.sys.platform == cc.sys.WECHAT_GAME) {
        return true;
    }

    return false;
}

var md5 = function(str)
{
    let retValue = md5Script(str).toString();
    return retValue;
}

var isDebugMode = function()
{
    return true;
}

var createAutoFadeLabel = function(str, parent, pos)
{
    var nodeLabel = new cc.Node();
    nodeLabel.width = 100;
    nodeLabel.height = 100;

    var label = nodeLabel.addComponent(cc.Label);

    parent.addChild(nodeLabel);

    cc.loader.loadRes('font/fntCoins', cc.Font, function (err, font) {
        label.font = font;
    });
    
    label.fontSize = 20;
    //label.color = new cc.Color(64, 15, 200);
    label.string = str;

    nodeLabel.setPosition(pos);
    //nodeLabel.setScale(2.0);
    //nodeLabel.color = new cc.Color(64, 15, 200);

    let delay = cc.delayTime(0.6);
    let scale_to = cc.scaleTo(0.8, 4.0);
    let move_by = cc.moveBy(0.8, cc.v2(0, 20));
    let fade_to = cc.fadeTo(0.8, 0);
    let spawn = cc.spawn(scale_to, move_by, fade_to);
    let remove_self = cc.removeSelf(true);
    let seq = cc.sequence(delay, spawn, remove_self);

    nodeLabel.runAction(seq);

    return nodeLabel;
}

var captureScreenToNode = function()
{
    let node = new cc.Node();
    let scene = cc.director.getScene();
    node.parent = scene;
    
    node.setPosition(cc.visibleRect.width * 0.5, cc.visibleRect.height * 0.5);
    let camera = node.addComponent(cc.Camera);

    //camera.cullingMask = 0xffffffff;

    let texture = new cc.RenderTexture();
    let gl = cc.game._renderContext;
    texture.initWithSize(cc.visibleRect.width, cc.visibleRect.height, gl.STENCIL_INDEX8);

    camera.targetTexture = texture;
    camera.render();

    let spriteFrame = new cc.SpriteFrame();
    spriteFrame.setTexture(texture);

    let nodeDisplay = new cc.Node();
    let sprite = nodeDisplay.addComponent(cc.Sprite);
    sprite.spriteFrame = spriteFrame;

    node.destroy();

    return nodeDisplay;
    
    /*
    let scene = cc.director.getScene();

    //nodeDisplay.setPosition(0, 100);
    //nodeDisplay.scale = 0.5;
    scene.addChild(nodeDisplay);

    nodeDisplay.setPosition(this.node.width * 0.5, this.node.height * 0.5);
    nodeDisplay.scaleX = 0.8;
    nodeDisplay.scaleY = -0.8;

    cc.log('nodeDisplay.x =' + nodeDisplay.x + ', nodeDisplay.y =' + nodeDisplay.y + ', scale = ' + nodeDisplay.scale);

    
    */
}

var numberLerpToNumberSoft = function(num0 ,num1, step = 0.1)
{
    let dist = num1 - num0;
    let s = dist * step;
    if (s < 1) {
        s = 1;
    }
    let animNum = num0 + s;

    return animNum;
}

var numberLerpToNumber = function(num0 ,num1, step = 0.1)
{
    if (num0 == num1) {
        return num0;
    }

    let dist = num1 - num0;    
    let s = dist * step;
    if (dist > 0) {
        if (s < 1) {
            s = 1;
        }
    }
    else {
        if (s > -1) {
            s = -1;
        }
    }
    let animNum = num0 + s;

    let m = num1*0.05;
    if (m < 3) {
        m = 3;
    }
    
    if (Math.abs(animNum - num1) < m) {
        animNum = num1;
    }

    return animNum;
}

var numberStepToNumber = function(num0 ,num1, step = 0.1)
{
    let labDisplay = num0 || 0;
    let numDist = num1 - labDisplay;

    if (Math.abs(numDist) < 0.2) {
        return num1;
    }
    else {
        if (numDist > 0) {
            numDist = step;
        }
        else
        {
            numDist = -step;
        }
    }

    return labDisplay + numDist;
}

var randInt = function(range)
{
    return Math.floor(Math.random()*10000)%range;
}

function jsQueue(maxSize = 32)
{
    this._queue = [];
    this.maxSize = maxSize;
    this.p0 = 0;
    this.p1 = 0;

    this.enQueue = function(val)
    {
        this._queue[this.p1] = val;

        this.p1 ++;
        this.p1 %= this.maxSize;
    }

    this.deQueue = function(val)
    {
        let ret = this._queue[this.p0];

        if (!!ret) {
            this.p0 ++;
            this.p0 %= this.maxSize;
        }

        return ret;
    }

    this.getTop = function()
    {
        let ret = this._queue[this.p0];
        return ret;
    }

    this.size = function()
    {
        if (this.p1 >= this.p0) {
            return this.p1 - this.p0;
        }
        
        return (this.p1 + this.maxSize - this.p0);
    }

    this.clear = function()
    {
        this.p0 = 0;
        this.p1 = 0;
        this._queue = [];
    }
}

function jsVector()
{
    this._queue = [];
    this.p = -1;

    this.push_back = function(val)
    {
        this.p++;

        if (!(!!this._queue[this.p])) {
            this._queue[this.p] = val;
        }
    }

    this.get = function(index)
    {
        if (this.p >= 0) {

            if (index >= 0 && index <= this.p) {
                return this._queue[index];
            }
        }

        return null;
    }

    this.set = function(index, value)
    {
        if (this.p >= 0) {

            if (index >= 0 && index <= this.p) {
                this._queue[index] = value;
            }
        }
    }

    this.size = function()
    {
        return this.p + 1;
    }

    this.clear = function()
    {
        this._queue = [];
        this.p = -1;
    }
}

function jsStack()
{
    this._queue = [];
    this.p = -1;

    this.push = function(val)
    {
        this.p++;

        if (!(!!this._queue[this.p])) {
            this._queue[this.p] = val;
        }
    }

    this.pop = function()
    {
        if (this.p >= 0) {

            return this._queue[this.p --];
        }

        return null;
    }

    this.size = function()
    {
        return this.p + 1;
    }

    this.clear = function()
    {
        this._queue = [];
        this.p = -1;
    }
}

function stringToUint8Array(str){
    var arr = [];
    for (var i = 0, j = str.length; i < j; ++i) {
      arr.push(str.charCodeAt(i));
    }
   
    var tmpUint8Array = new Uint8Array(arr);
    return tmpUint8Array
}

function invokeCallback(cb) {
    if(!!cb && typeof cb === 'function') {
        cb.apply(null, Array.prototype.slice.call(arguments, 1));
    }
};

module.exports = {
    createSprite: createSprite,
    createSpriteFromPlist : createSpriteFromPlist,
    createLabel: createLabel,
    createAutoFadeLabel: createAutoFadeLabel,
    numberLerpToNumber : numberLerpToNumber,
    numberLerpToNumberSoft : numberLerpToNumberSoft,
    numberStepToNumber : numberStepToNumber,
    createParticle : createParticle,
    createSpineNode : createSpineNode,
    randInt : randInt,
    jsStack : jsStack,
    jsVector : jsVector,
    jsQueue : jsQueue,
    stringToUint8Array : stringToUint8Array,
    createMusic : createMusic,
    captureScreenToNode : captureScreenToNode,
    invokeCallback: invokeCallback,
    isWeChat : isWeChat,
    md5 : md5,
    isDebugMode : isDebugMode,
    getVersion : getVersion
}
