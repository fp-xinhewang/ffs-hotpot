var utils = require('utils');

var touchManager = cc.Class({
    extends: cc.Component,

    properties: {
        canvas : cc.Node
    },
    
    onLoad: function () {
        var self = this;

        self.canvas.on(cc.Node.EventType.TOUCH_START, function (event) {

            var touches = event.getTouches();
            var touchLoc = touches[0].getLocation();

            if (touches.length >= 2) {
                // cc.log("MULTI TOUCH, touch count =" + touches.length);
            }

            // cc.log("TOUCH BEGIN ,position.x = " + touchLoc.x + ", y = " + touchLoc.y);
        }, self.node);

        self.canvas.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
            var touches = event.getTouches();
            var touchLoc = touches[0].getLocation();
            
            // let node = utils.createParticle('particle/cabbage_crash');
            // node.setPosition(touchLoc.x - self.canvas.width*0.5,touchLoc.y - self.canvas.height*0.5);

            // self.canvas.addChild(node);

            // cc.log("TOUCH MOVE ,position.x = " + touchLoc.x + ", y = " + touchLoc.y);
            
        }, self.node);

        self.canvas.on(cc.Node.EventType.TOUCH_END, function (event) {
            
            var touches = event.getTouches();
            var touchLoc = touches[0].getLocation();

            // let node = utils.createParticle('particle/cabbage_crash');
            // node.setPosition(touchLoc.x - self.canvas.width*0.5,touchLoc.y - self.canvas.height*0.5);

            // self.canvas.addChild(node);

            // cc.log("TOUCH END ,position.x = " + touchLoc.x + ", y = " + touchLoc.y);

        }, self.node);
    },

    start () {

    },

    // update (dt) {},
});
