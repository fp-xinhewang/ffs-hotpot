// tools.js
// Created by zacklocx on 2018-12-23

function rand_int(min, max)
{
    min = Math.ceil(min);
    max = Math.floor(max);

    return min + Math.floor(Math.random() * (max - min));
}

function rand_real(min, max)
{
    return min + Math.random() * (max - min);
}

module.exports =
{
    rand_int: rand_int,
    rand_real: rand_real
}
