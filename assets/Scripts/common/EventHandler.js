// EventHandler.js
// Created by zacklocx on 2018-12-26

cc.Class({
    extends: cc.Component,

    onLoad()
    {
        this.node.on('feed_food', function(event)
        {
            var food_map = event.getUserData();

            food_map.forEach((num, id) =>
            {
                cc.log(id + ': ' + num);
            });

            var script = event.currentTarget.getChildByName('script').getComponent('gameLevel');
            script.eattingFood(food_map);
        });

        // // add soup
        // this.node.on('add_soup', function(event)
        // {
        //     var num = event.getUserData();

        //     var script = event.currentTarget.getChildByName('script').getComponent('gameLevel');
        //     script.addStepLeft(num);
        // });

        this.node.on('stage_playCombo', function(event)
        {
            var params = event.getUserData();
            let level = params.get('level');

            var script = event.currentTarget.getChildByName('script').getComponent('gameLevel');

            script.playComboEffect(level);
        });

        //检查关卡是否结束
        this.node.on('stage_finish_check', function(event)
        {
            var script = event.currentTarget.getChildByName('script').getComponent('gameLevel');
            script.onStageFinishCheck();
        });

        this.node.on('stage_finish', function(event)
        {
            //var script = event.currentTarget.getChildByName('script').getComponent('gameLevel');
            // script.onStageFinishCheck();
            var pot = event.currentTarget.getChildByName('potContainer').getChildByName('pot');
            if(pot)
            {
                var script = pot.getComponent('FoodManager');
                script.stage_finished();
            }

            var script = event.currentTarget.getChildByName('script').getComponent('gameLevel');
            script.stageComplete();
        });

        this.node.on('drop_soup_pack', function(event)
        {
            var soup_id = event.getUserData().soupId;
            var pot = event.currentTarget.getChildByName('potContainer').getChildByName('pot');

            if(pot)
            {
                var script = pot.getComponent('FoodManager');
                script.gen_soup_pack(soup_id);
            }
        });
    }
});
