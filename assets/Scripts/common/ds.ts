// ds.ts
// Created by zacklocx on 2018-12-23

export class Queue<T>
{
    _store: T[] = [];

    empty()
    {
        return 0 == this._store.length;
    }

    push(val: T)
    {
        this._store.push(val);
    }

    pop(): T | undefined
    {
        return this._store.shift();
    }
}
