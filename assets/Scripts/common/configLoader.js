var webRequestManager = require('webRequestManager');

var configLoader = cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    statics: {

        instance : null
    },

    getBaseConfig : function()
    {
        let config = configLoader.instance.ConfigSpecConf.ConfigSpecs[0];

        return config;
    }
    ,

    getShareConfig : function(idx)
    {
        //从1开始
        let config = configLoader.instance.ShareSpecConf.ShareSpecs[idx];

        return config;
    }
    ,

    getDecorationConfig : function (itemId)
    {
        let config = configLoader.instance.DecorationSpecConf.DecorationSpecs[itemId];

        return config;
    }
    ,

    getItemConfig : function (itemId)
    {
        let config = configLoader.instance.ItemSpecConf.ItemSpecs[itemId];

        return config;
    }
    ,

    getStageConfig : function (stage)
    {
        let config = configLoader.instance.StageSpecConf.StageSpecs[stage];
        
        return config;
    }
    ,

    getNpcConfig : function (npcId)
    {
        let config = configLoader.instance.NpcSpecConf.NpcSpecs[npcId];
        
        return config;
    }
    ,

    getPieceConfig : function (itemId)
    {
        let config = configLoader.instance.PieceSpecConf.PieceSpecs[itemId];
        
        return config;
    }
    ,

    getLevelConfig : function(level)
    {
        let config = configLoader.instance.LevelSpecConf.LevelSpecs[level];

        return config;
    }
    ,

    getGuideConfig : function(step)
    {
        //step 传进来从0开始
        let stepFrom1 = step + 1;

        let config = configLoader.instance.GuideSpecConf.GuideSpecs[stepFrom1];
        return config;
    }
    ,

    getTaskConfig : function(name)
    {
        let config = configLoader.instance.TaskSpecConf.GuideSpecs[name];
        return config;
    }
    ,

    createStoreSortArray : function()
    {
        function sortFuct(a,b)
        {
            let decConfig0 = configLoader.instance.DecorationSpecConf.DecorationSpecs[a.SpecId];
            let decConfig1 = configLoader.instance.DecorationSpecConf.DecorationSpecs[b.SpecId];
            return decConfig0.UnlockLevel - decConfig1.UnlockLevel;
        }
       
        configLoader.instance.StoreSpec_conf_array = [];

        for (let key in configLoader.instance.StoreSpecConf.StoreSpecs)
        {
            let len = configLoader.instance.StoreSpec_conf_array.length;
            configLoader.instance.StoreSpec_conf_array[len] = configLoader.instance.StoreSpecConf.StoreSpecs[key];
        }

        configLoader.instance.StoreSpec_conf_array.sort(sortFuct);
    }
    ,

    onLoad: function () {
        
        configLoader.instance = this;

        webRequestManager.instance.requestGetPmConfig(
            (rsp)=> {

                cc.log('config loader ============= rsp =');
                cc.log(rsp);

                for (let key in rsp.pmconf) {
                    configLoader.instance[key] = rsp.pmconf[key];
                }

                configLoader.instance.createStoreSortArray();

                let gameLoginScript = configLoader.instance.node.getComponent('gameLogin');
                if (!!gameLoginScript) {
                    gameLoginScript.onConfigLoaderFinished();
                }
            }
        );
        

    },

});

