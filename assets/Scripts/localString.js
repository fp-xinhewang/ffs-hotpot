
function strKeyVals()
{    
    this['_keyBuyObj'] = '确定购买不?';
    this['_keyOk'] = '好的';
    this['_keyLackCoins'] = '金币不够，挣钱再回来购买';
}

var localString = cc.Class({
    statics: {
        instance : null
    }
    ,

    ctor :function() {
        this.keys = new strKeyVals();
    }
    ,

    getKeyStr :function(val)  {
        return this.keys[val];
    }
});


localString.instance = new localString();
module.exports = localString;


