var configLoader = require('configLoader');
var data = require('datas');
var utils = require('utils');
var webRequestManager = require('webRequestManager');
var taskManager = require('taskManager');
var ui = require('ui');
var gio = require("gio-ming");

var game = cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    statics: {

        instance : null
    },

    onLoad: function () {
        game.instance = this;

        // 初始化growing io
        // 1.version 小游戏的版本号
        // 2.forceLogin 小游戏是否强制要求用户登陆微信获取 openid.
        //      如果小游戏在用户打开后不及时要求用户授权获取openid和/或 unionid，
        //      但是设置了forceLogin为True，那么GroiwngIO不能采集到用户的数据，采集到的用户会偏少.
        // 3.followShare 详细跟踪微信组件中的wx. shareAppMessage事件的转发分享数据，开启后可使用分享分析功能
        // 4.debug 是否开启调试模式，可以看到采集的数据.
        if(typeof(wx) != "undefined") {

            let verStr = utils.getVersion();
            
            gio('init', '9791da760bb8846c', 'wx40f83f1131b6adb6', 
                { version: verStr.versionString, forceLogin: false, followShare:false, debug:false }
            );
        }


        this.weChatCode = '';
        this.playerData = new data.playerData();
        this.currentStage = 1;
        this.currentStageCoins = 0;
        this.currentLevel = {
            level : 1,
            expDist : 1,
            nextExp : 1
        };

        //http://www.w3school.com.cn/jsref/jsref_obj_date.asp Date用法
        this.stageHistory = {};
        this.storeHistory = {};

        this.lobbySceneInstance = null;
        this.lobbyDecorationInstance = null;

        this.bgmEnabled = true;
        this.effectSoundEnabled = true;

        this.lobbyCoinsCollectTime = 0;
    }
    ,

    getClientInfo : function(key = null)
    {
        if (!!this.clientInfo) {
            
            cc.log('-------------- CLIENT INFO ----------------');
            cc.log(this.clientInfo);

            if (key == null) {
                return this.clientInfo;
            }
            else {
                return this.clientInfo[key];
            }
        }

        return null;
    }
    ,

    startGame : function(rsp)
    {
        this.playerData.level = rsp.player.base.level;
        this.playerData.uid = rsp.player.base.uid;
        this.playerData.coins = rsp.player.coins || 0;
        this.playerData.exp = rsp.player.exp || 0;
        this.lobbyCoinsCollectTime = rsp.player.collectAt;
        this.clientInfo = rsp.player.clientInfo.Map;

        this.storeHistory = rsp.player.decorations;
        this.initStoreHistory();
        
        this.requestOther();

        let guideStatusScript = this.getComponent('guideStatus');
        if (!!guideStatusScript) {

            guideStatusScript.initGuideStatus(this.clientInfo);

            if (guideStatusScript.isInGuideStatus() == true &&
                guideStatusScript.getGuideStep() == 0)
            {
                this.startGameLevel(1);
                return;
            }
        }

        cc.director.loadScene('lobby');
    }
    ,

    isInGuideMode : function()
    {
        let guideStatusScript = this.getComponent('guideStatus');
        if (!!guideStatusScript) {
            return guideStatusScript.isInGuideStatus();
        }

        return false;
    }
    ,

    getGuideStatus : function()
    {
        let guideStatusScript = this.getComponent('guideStatus');
        if (!!guideStatusScript) {
            return guideStatusScript;
        }

        return null;
    }
    ,

    getCurrentDate : function()
    {
        return new Date();
    }
    ,

    getUTCTime : function()
    {
        let retValue = new Date().getTime()*0.001;
        retValue = Math.floor(retValue);

        return retValue;
    }
    ,

    getCollectTime : function()
    {
        return this.lobbyCoinsCollectTime;
    }
    ,

    collectCoins : function(val)
    {
        this.addCoins(val);
        this.lobbyCoinsCollectTime = this.getUTCTime();

        if (!!this.lobbySceneInstance) {
            this.lobbySceneInstance.collectCoinsAnimation();
        }
    }
    ,

    start () {
        cc.log("Game. start");
        cc.game.addPersistRootNode(this.node);
        //cc.game.removePersistRootNode(this.node);

        //this.testSwitchToStore();
        //this.testSwitchToMain();
    },

    requestOther : function()
    {
        webRequestManager.instance.requestGetStageHistory( 
            (rsp)=>{

                cc.log('----------------- HISTORY ----------------');
                

                for (let i =0; i < rsp.history.length; i ++)
                {
                    let id = rsp.history[i].stageId;
                    let coins = rsp.history[i].coins;
                    
                    this.stageHistory[id] = coins;
                }

                cc.log(this.stageHistory);
            }
        );

        taskManager.instance.gameInstance = game.instance;
        taskManager.instance.refreshDailyTask();
    }
    ,
    
    update (dt) {

    },

    getStageHistory : function(stageId)
    {
        let ret = this.stageHistory[stageId];
        if (ret == null) {
            return 0;
        }

        return ret;
    }
    ,

    updateStageHistory : function(stageId, val)
    {
        let val0 = this.stageHistory[stageId];
        if (val0 == null) {
            val0 = 0;
        }

        if (val > val0) {
            this.stageHistory[stageId] = val;
        }
    }
    ,

    getConfig : function()
    {
        return configLoader.instance;
    }
    ,

    updateLevel : function ()
    {
        let retValue = {
            level : 1,
            expDist : 1,
            nextExp : 1
        };

        let exp = game.instance.playerData.exp;

        if (exp > 0) {

            let keys = Object.keys(configLoader.instance.LevelSpecConf.LevelSpecs);
            
            let len = keys.length;

            for (let i = 1; i <= len; i ++)
            {
                let config = configLoader.instance.LevelSpecConf.LevelSpecs[i];
                if (!!config) {
                    if (exp >= config.Exp) {
                        retValue.level ++;
                        exp -= config.Exp;
                    }
                    else {
                        retValue.expDist = config.Exp;
                        retValue.nextExp = config.Exp - exp;
                        break;
                    }
                }
                else {
                    break;
                }
            }
        }

        this.currentLevel.level = retValue.level;
        this.currentLevel.expDist = retValue.expDist;
        this.currentLevel.nextExp = retValue.nextExp;
    }
    ,

    getLevel : function ()
    {
        return this.currentLevel.level;
    }
    ,

    getLevelStruct :function()
    {
        return this.currentLevel;
    }
    ,

    getGameLevelStage : function()
    {
        return this.currentStage;
    }
    ,

    setGameLevelStage : function(val)
    {
        this.currentStage = val;
    }
    ,

    startGameLevel : function(val)
    {
        cc.log('start game level =' + val);
        this.currentStage = val;
        this.currentStageCoins = 0;

        cc.director.loadScene('main');
    }
    ,

    addCoins : function(num)
    {
        game.instance.playerData.coins += num;
        return game.instance.playerData.coins;
    }
    ,

    getCoins : function()
    {
        return game.instance.playerData.coins;
    }
    ,

    addCurrentStageCoins : function(num)
    {
        game.instance.currentStageCoins += num;
    }
    ,

    getCurrentStageCoins : function()
    {
        return game.instance.currentStageCoins;
    }
    ,

    addExp : function(num)
    {
        let level0 = game.instance.getLevel();

        game.instance.playerData.exp += num;
        this.updateLevel();

        let level1 = game.instance.getLevel();

        if (level0 != level1) {
            ui.instance.showLevelupPanel();
        }
    }
    ,

    getExp : function()
    {
        return game.instance.playerData.exp;
    },

    getLobbyScene : function()
    {
        return this.lobbySceneInstance;
    },

    getLobbyDecoration : function()
    {
        return this.lobbyDecorationInstance;
    }
    ,

    getGameLevelIntance : function()
    {
        return this.gameLevelInstance;
    }
    ,

    getStoreHistory : function()
    {
        return this.storeHistory;
    },

    initStoreHistory : function()
    {
        let expSum = 0;

        for(var specId in this.storeHistory){

            let decConfig = configLoader.instance.getDecorationConfig(specId);
            if (!!decConfig) {
                expSum += decConfig.Exp;
            }
        }

        game.instance.playerData.exp = expSum;
        this.updateLevel();
    }
    ,

    getPosCoinsNumber : function()
    {
        let currentLevel = game.instance.getLevel();
        let timeNow = game.instance.getUTCTime();
        let time0 = game.instance.getCollectTime();

        let levelConfig = configLoader.instance.getLevelConfig(currentLevel);
        let collectAuto = levelConfig.collectAuto;
        let collectLimit = levelConfig.collectLimit;


        let timeDist = timeNow - time0;
        timeDist = Math.floor(timeDist * 0.1);

        let coinsNum = collectAuto *timeDist;
        if (coinsNum < 0) {
            coinsNum = 0;
        }

        if (coinsNum > collectLimit) {
            coinsNum = collectLimit;
        }

        let retStruct = {
            coins : coinsNum,
            limit : collectLimit
        };

        return retStruct;
    }
    ,

    buyStoreDecoration : function(specId,coins)
    {
        webRequestManager.instance.requestBuy(specId, (rsp)=> {

            cc.log('======================= BUY OBJECT =====');
            cc.log(rsp);

        });

        let lobbyScene = game.instance.getLobbyScene();
        if (!!lobbyScene) {
            lobbyScene.buyObject(specId);
        }

        this.addCoins(-coins);

        this.storeHistory[specId] = 1;

        let decConfig = configLoader.instance.getDecorationConfig(specId);
        if (!!decConfig) {
            this.addExp(decConfig.Exp);
        }
    }
});
