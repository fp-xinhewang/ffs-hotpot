cc.Class({
    extends: cc.Component,

    properties: {
        spineComp : sp.Skeleton
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.node.active = false;
    },

    playCombo : function(lev)
    {
        if (lev < 1)
        {
            lev = 1;
        }

        if (lev > 3) {
            lev = 3;
        }

        this.node.active = true;

        let animName = 'combo_' + lev;

        if (this.spineComp) {
            this.spineComp.setAnimation(0, animName, false);
        }
    }

    // update (dt) {},
});
