var ui = require('ui');
var utils = require('utils');

cc.Class({
    extends: cc.Component,

    properties: {
        flyToNode : cc.Node,
        flyToHeartNode : cc.Node,
        flyToHeartFrom : cc.Node,
        coinsNum : cc.Label,
        soupBarNode : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    flyHeartFinish : function()
    {
        let soupCompent = this.soupBarNode.getComponent('soupBar');
        if (!!soupCompent) {
            soupCompent.addStepLeft(1);
        }
    }
    ,

    flyHeart : function(pos)
    {
        let spr = utils.createSprite('general/broth');

        let pos0 = this.flyToHeartFrom.convertToNodeSpace(pos);

        spr.x = pos0.x;
        spr.y = pos0.y;
        spr.scale = 0.5;
        spr.opacity = 0;

        this.flyToHeartFrom.addChild(spr);

        let w = this.flyToHeartNode.width * 0.5;
        let h = this.flyToHeartNode.height * 0.5;

        let pos1 = this.flyToHeartNode.convertToWorldSpace(new cc.Vec2(w,h));
        pos1 = this.flyToHeartFrom.convertToNodeSpace(pos1);

        let delay = cc.delayTime(0.7);
        let tm = 1.0;
        
        let scale0 = cc.scaleTo(0.6, 1.5).easing(cc.easeBackOut());
        let dly0 = cc.delayTime(0.2);
        let scale1 = cc.scaleTo(0.3, 1).easing(cc.easeBackOut());
        let seq0 = cc.sequence(scale0, dly0, scale1);

        let move_to = cc.moveTo(tm, pos1).easing(cc.easeSineOut());

        let fade0 = cc.fadeIn(0.5);
        let dly = cc.delayTime(0.3);
        let fade1 = cc.fadeOut(0.3);
        let seq1 = cc.sequence(fade0, dly, fade1);

        let spawn = cc.spawn(seq0, move_to, seq1);

        var callback = cc.callFunc(this.flyHeartFinish, this);

        let remove_self = cc.removeSelf(true);
        let seq3 = cc.sequence(delay, spawn, callback, remove_self);
        
        spr.runAction(seq3);
    }
    ,

    flyCoins : function(pos0, num)
    {
        //let pos1 = this.flyToNode.position;
        //this.coinsSpine.setAnimation(0, 'start', false);
        //this.coinsSpine.node.setPosition(pos0);

        //let move_to = cc.moveTo(1.5, pos1).easing(cc.easeBackOut());
        //this.coinsSpine.node.runAction(move_to);

        this.coinsNum.string = num;
        
        this.coinsNum.node.opacity = 255;
        this.coinsNum.node.scale = 1;
        this.coinsNum.node.x = 76;
        this.coinsNum.node.y = -80;
        
        this.node.x = pos0.x;
        this.node.y = pos0.y;

        let move_by = cc.moveBy(0.8, cc.v2(0, 100)).easing(cc.easeBackOut());
        let fade_to = cc.fadeTo(0.2, 0);
        let scale_to = cc.scaleTo(0.2, 0.5).easing(cc.easeBackOut());
        let spawn = cc.spawn(fade_to, scale_to);
        let seq = cc.sequence(move_by, spawn);

        this.coinsNum.node.runAction(seq);
    }

    // update (dt) {},
});
