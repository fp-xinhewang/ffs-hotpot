var game = require('game');

cc.Class({
    extends: cc.Component,

    properties: {

        labelLevel : cc.Label,
        //labelPercent : cc.Label,
        //progressSpr : cc.Node,
        prgressBar : cc.ProgressBar,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.updateUI();
    },

    updateUI : function()
    {
        let level = game.instance.getLevelStruct();
        this.expNow = game.instance.getExp();
        this.levelNow = level.level;

        this.labelLevel.string = this.levelNow;

        let per = (level.expDist - level.nextExp)/level.expDist;
        if (per < 0) per = 0;
        if (per > 1) per = 1;

        this.prgressBar.progress = per;

        //let perValue = Math.floor(per * 100);
        //this.labelPercent.string = perValue.toString() + '%';

        //let min = 24;
        //let max = 269;

        //let width = min + per * (max - min);
        //this.progressSpr.width = width;
    }
    ,

    update :function(dt) {
        
        let exp = game.instance.getExp();
        if (this.expNow != exp){
            this.updateUI();
        }       
    }

});
