

cc.Class({
    extends: cc.Component,

    properties: {
        light0 : cc.Node,
        light1 : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () 
    {
        this.timCalc = 0;
        this.freq = 1;
        this.started = false;
        this.lightSwitch = false;

        this.light1.active = false;
    },  

    startNormalFlash () {
        this.freq = 0.5;
        this.started = true;
    },

    startQuickFlash () {
        this.freq = 0.25;
        this.started = true;
    },

    endFlash() {
        this.timCalc = 0;
        this.started = false;
        this.lightSwitch = false;

        this.light1.active = false;
    }
    ,

    update (dt) 
    {
        if (this.started) {
            this.timCalc += dt;
            if (this.timCalc >= this.freq) {

                this.light1.active = !this.lightSwitch;

                this.lightSwitch = !this.lightSwitch;
                this.timCalc = 0;
            }
        }
    },
});
