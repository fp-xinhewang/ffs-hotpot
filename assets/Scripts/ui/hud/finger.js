
cc.Class({
    extends: cc.Component,

    properties: {

        finger0 : cc.Node,
        finger1 : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        this.finger0.active = true;
        this.finger1.active = false;

        this.flag = true;
        this.tim0 = 0;
        this.animationEnabled = this.animationEnabled || false;
    },

    setAnimationEnabled : function(val)
    {
        this.animationEnabled = val;

        if (this.animationEnabled == true)
        {

        }
        else {
            this.finger0.active = true;
            this.finger1.active = false;
        }
    }
    ,

    switchFinger : function()
    {
        this.flag = !this.flag;

        this.finger0.active = this.flag;
        this.finger1.active = !this.flag;
    }
    ,

    update (dt) 
    {
        if (this.animationEnabled == true) {

            this.tim0 += dt;

            if (this.tim0 > 0.2) {

                this.switchFinger();
                this.tim0 = 0;
            }
        }
    },
});
