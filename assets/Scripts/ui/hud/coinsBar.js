var game = require('game');
var utils = require('utils');

cc.Class({
    extends: cc.Component,

    properties: {
        labCoins : cc.Label
    },

    start () {

        this.coinsNow = game.instance.getCoins();

        this.labCoins.string = this.coinsNow;
        this.refreshTime = 0;
    },

    update (dt) 
    {
        this.refreshTime += dt;
        let coinsNum = game.instance.getCoins();
        if (this.coinsNow < coinsNum && this.refreshTime > 0.2)
        {
            let animNum = utils.numberLerpToNumberSoft(this.coinsNow, coinsNum, 0.6);

            this.coinsNow = animNum;
            this.labCoins.string = Math.floor(animNum);

            this.refreshTime = 0;
        }
        else if (this.coinsNow > coinsNum)
        {
            this.coinsNow = coinsNum;
            this.labCoins.string = coinsNum;
        }
    }

});
