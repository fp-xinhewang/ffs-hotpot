// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {

        bossCurtain : sp.Skeleton
    },


    start () {

        this.bossCurtain.setCompleteListener((trackEntry) => 
        {
            var animationName = trackEntry.animation ? trackEntry.animation.name : "";
            switch (animationName) {
                case 'open':
                {
                    this.playCurtainIdle();
                    break;
                }

                case 'close':
                {
                    this.node.active = false;
                    break;
                }
            }
        });

        this.node.active = false;
    },

    playCurtainIdle : function()
    {
        this.bossCurtain.setAnimation(0, 'idle', true);
    }
    ,

    openCurtain : function()
    {
        this.node.active = true;
        this.node.opacity = 0;

        let fade0 = cc.fadeIn(0.2);
        this.node.runAction(fade0);

        this.bossCurtain.setAnimation(0, 'open', false);
    }
    ,

    closeCurtain : function()
    {
        let fade0 = cc.fadeOut(0.2);
        this.node.runAction(fade0);
        this.bossCurtain.setAnimation(0, 'close', false);
    }

    // update (dt) {},
});
