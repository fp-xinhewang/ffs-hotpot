
cc.Class({
    extends: cc.Component,

    properties: {

        heartPosNode : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        this.spineComp = this.getComponent(sp.Skeleton);

        this.spineComp.setCompleteListener((trackEntry) => 
        {
            var animationName = trackEntry.animation ? trackEntry.animation.name : "";
            switch (animationName) {
                case 'animation':
                {
                    this.hideStepLimitAddPanel();
                    break;
                }
            }
        });

        this.node.active = false;
    },

    showStepLimitAddPanel()
    {
        this.node.active = true;

        this.node.x = 0;
        this.node.y = 0;
        this.node.scale = 1;
        this.node.opacity = 255;
        this.spineComp.setAnimation(0, 'animation', false);

        /*
        let w = this.heartPosNode.width *0.5;
        let h = this.heartPosNode.height *0.5;

        let posTo = this.heartPosNode.convertToWorldSpace(new cc.Vec2(w,h));
        posTo = this.node.parent.convertToNodeSpace(posTo);

        let move0 = cc.moveTo(0.3, posTo);
        let scale0 = cc.scaleTo(0.3,0.2);
        let fade0 = cc.fadeOut(0.5);

        let dly = cc.delayTime(2.5);

        let spw = cc.spawn(move0, scale0, fade0);
        let seq = cc.sequence(dly, spw);
        this.node.runAction(seq);
        */
    }
    ,
    
    hideStepLimitAddPanel()
    {
        this.node.active = false;
    }

    // update (dt) {},
});
