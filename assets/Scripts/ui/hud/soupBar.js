

cc.Class({
    extends: cc.Component,

    properties: {
        labelNumber : cc.Label,
        soupNode : cc.Node,
        soupSpine : sp.Skeleton,
        deadingTipNode : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        this.deadingTipNode.active = false;
        this.prgressBar = this.getComponent(cc.ProgressBar);
        this.stepLeftNumber = 0;
        this.stepLeftNumberMax = 0;
        this.prgressBar.progress = 0;
        this.isAboutDeading = false;

        if (!!this.soupSpine) {
            this.soupSpine.setAnimation(0,'saodong1',true);
        }
    },

    changeToInActiveDeadTipNode : function()
    {
        this.deadingTipNode.active = this.isAboutDeading;
    }
    ,

    changeDeading : function(val)
    {
        if (this.isAboutDeading != val) {

            this.isAboutDeading = val;

            if (!!this.soupSpine) {
                if (this.isAboutDeading) {
                    this.soupSpine.setAnimation(0,'saodong2',true);
                }
                else {
                    this.soupSpine.setAnimation(0,'saodong1',true);
                }
            }

            
            if (this.isAboutDeading) {

                this.deadingTipNode.active = true;
                this.deadingTipNode.opacity = 0;
                this.deadingTipNode.runAction(cc.fadeIn(0.5));
                let deadTip = this.deadingTipNode.getComponent('deadingTip');
                if (!!deadTip)
                {
                    deadTip.startAnimation();
                }
            }
            else {
                let callback_func = cc.callFunc(this.changeToInActiveDeadTipNode,this);
                let fade0 = cc.fadeOut(0.5);
                let seq = cc.sequence(fade0, callback_func);
                this.deadingTipNode.runAction(seq);
            }
        }
    }
    ,

    addStepLeft : function(val)
    {
        let num0 = this.stepLeftNumber + val;
        
        this.updateStepLeftNumber(num0);
    }
    ,

    addStepLeftMax : function(val)
    {
        this.stepLeftNumberMax += val;
        
        this.updateStepLeftNumber(this.stepLeftNumber, true);
    }
    ,

    initialize : function(val, valMax)
    {
        this.stepLeftNumberMax = valMax;
        this.stepLeftNumber = val;
        this.labelNumber.string = this.stepLeftNumber;

        this.prgressBar.progress = this.stepLeftNumber/this.stepLeftNumberMax;
    }
    ,

    easeLabel : function(val0,val1,val2)
    {
        let scale0 = cc.scaleTo(val1, val0).easing(cc.easeBackOut());
        let scale1 = cc.scaleTo(val2, 1.0).easing(cc.easeBackOut());
        let seq = cc.sequence(scale0, scale1);

        this.labelNumber.node.runAction(seq);
    }
    ,

    easeHeartMax : function()
    {
        let big = 1.25;
        let scale0 = cc.scaleTo(0.25, big).easing(cc.easeBackOut());
        let scale1 = cc.scaleTo(0.25, 1.0).easing(cc.easeBackOut());
        let scale2 = cc.scaleTo(0.25, big).easing(cc.easeBackOut());
        let scale3 = cc.scaleTo(0.25, 1.0).easing(cc.easeBackOut());

        let seq = cc.sequence(scale0,scale1,scale2,scale3);
        this.soupNode.runAction(seq);
    }
    ,

    updateStepLeftNumber : function(val , force = false)
    {
        if (val > this.stepLeftNumberMax) {
            val = this.stepLeftNumberMax;
            this.easeHeartMax();
        }

        if (this.stepLeftNumber != val || force == true)
        {
            if (val < this.stepLeftNumber) {
                this.easeLabel(0.75,0.15,0.2);
            }
            else {
                this.easeLabel(1.25,0.2,0.25);
            }

            this.stepLeftNumber = val;
            this.labelNumber.string = this.stepLeftNumber;

            let seq = cc.sequence(cc.scaleTo(0.02, 0.5).easing(cc.easeBackOut()),
                        cc.scaleTo(0.1, 1.1).easing(cc.easeBackOut()),
                            cc.scaleTo(0.05, 1.0).easing(cc.easeBackOut()));

            this.soupNode.runAction(seq);

            this.prgressBar.progress = this.stepLeftNumber/this.stepLeftNumberMax;

            this.stepLeftNumber = val;
            this.changeDeading(this.stepLeftNumber < 4);
        }
    }

});
