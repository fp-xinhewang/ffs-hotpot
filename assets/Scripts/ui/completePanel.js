var game = require('game');
var ui = require('ui');
var utils = require('utils');
var soundManager = require('soundManager');
var wechatUIPort = require('wechatUIPort');
var configLoader = require('configLoader');

cc.Class({
    extends: cc.Component,

    properties: {
        
        maskLayer : cc.Node,
        root : cc.Node,
        labelCoinsNum : cc.Label,
        labelHistoryNum : cc.Label,
        shareCoinsTimeLabel : cc.Label,
        playCoinsBar : cc.Node,
        piggyNode : cc.Node,
        backSpine : sp.Skeleton,
        piggySpine : sp.Skeleton,
        againButton : cc.Node,
        shareButton : cc.Node,
        closeButton : cc.Node,
        iconShareNode : cc.Node,
        iconVideoNode : cc.Node,
        shareText : cc.Label
    },

    // LIFE-CYCLE CALLBACKS:

    start () {
        if (!!this.maskLayer) {
            this.maskLayer.runAction(cc.fadeTo(0.2,128));
        }

        if (!!this.root) {
            this.root.setScale(0.5);
            this.root.runAction(cc.scaleTo(0.2,1.0).easing(cc.easeBackOut()));
        }

        if (!!this.againButton) {

            this.againButton.setScale(0.0);
            let delay = cc.delayTime(0.1);
            let seq = cc.sequence(delay, cc.scaleTo(0.2,1.0).easing(cc.easeBackOut()), null);
            this.againButton.runAction(seq);
        }

        if (!!this.shareButton) {

            this.shareButton.setScale(0.0);
            let delay = cc.delayTime(0.2);
            let seq = cc.sequence(delay, cc.scaleTo(0.2,1.0).easing(cc.easeBackOut()), null);
            this.shareButton.runAction(seq);
        }

        if (!!this.closeButton)
        {
            this.closeButton.setScale(0.0);
            let delay = cc.delayTime(0.3);
            let seq = cc.sequence(delay, cc.scaleTo(0.2,1.0).easing(cc.easeBackOut()), null);
            this.closeButton.runAction(seq);
        }

        let historyCoins = game.instance.getStageHistory(this.stageId);
        this.labelHistoryNum.string = '历史最高: ' + historyCoins;

        if (!!this.piggySpine) {

            this.piggySpine.setAnimation(0,'open',false);

            this.piggySpine.setCompleteListener((trackEntry) => 
            {
                var animationName = trackEntry.animation ? trackEntry.animation.name : "";
                switch (animationName) {
                    case 'open':
                    {
                        this.piggySpine.setAnimation(0,'saodong',true);
                        break;
                    }

                }
            });
        }

        this.runFlyIconsAnimation();
        this.beginAnimation = false;

        let config = configLoader.instance.getShareConfig(2);
        this.shareCoinsTimeLabel.string = config.RewardValue.toString() + '倍';

        this.shareType = config.ShareQueue[0];

        this.iconShareNode.active = (this.shareType == 1);
        this.iconVideoNode.active = (this.shareType == 2);

        if (this.shareType == 1) {
            this.shareText.string = '分享获取';
        }
        else {
            this.shareText.string = '广告获取';
        }

        soundManager.instance.playSound('complete_stage');
        
        this.maskLayer.width = cc.visibleRect.width;
        this.maskLayer.height = cc.visibleRect.height;
    },

    actRunFlyIcons : function()
    {
        /*
        let imgPath = 'general/coins';

        for (let i = 0; i <10; i ++) {

            ui.instance.flySpriteToPosition(imgPath, this.piggyNode, this.playCoinsBar,
                Math.random(), Math.random()*0.5 + 0.5, 0.1);
        }

        */

        this.beginAnimation = true;
        game.instance.addCoins(this.coinsNumFly);
    }
    ,

    runFlyIconsAnimation :function ()
    {
        if (this.coinsNum > 0) {

            let delay = cc.delayTime(0.3);
            var callback = cc.callFunc(this.actRunFlyIcons, this);

            let seq = cc.sequence(delay, callback, null);
            this.node.runAction(seq);
        }
    }
    ,

    initUI : function(params)
    {
        this.coinsNow = 0;
        this.coinsNum = params.coins;
        this.coinsNumFly = params.coins;

        this.stageId = params.stageId;
    }
    ,

    finishedStage : function()
    {
        if (this.hasSend != true) {
            let gameLevelInstance = game.instance.getGameLevelIntance();
            if (!!gameLevelInstance) {
                gameLevelInstance.sendFinish();
            }

            this.hasSend = true;
        }
    }
    ,

    cointinueStage : function()
    {
        let gameLevelInstance = game.instance.getGameLevelIntance();
        if (!!gameLevelInstance) {
            gameLevelInstance.continueToPlay();
        }
    }
    ,

    onContinueButton : function()
    {
        this.cointinueStage();

        soundManager.instance.playSound('button');

        this.node.destroy(true);
    },

    onCloseButton : function()
    {
        this.finishedStage();

        soundManager.instance.playSound('button');

        this.node.destroy(true);

        cc.director.loadScene('lobby');
    },

    onAgainButton : function()
    {
        //cc.director.loadScene('main')

        this.finishedStage();

        soundManager.instance.playSound('button');

        let stage = game.instance.getGameLevelStage();
        game.instance.startGameLevel(stage);
    }
    ,

    addShareReward : function()
    {
        let config = configLoader.instance.getShareConfig(2);
        let coinTimes = config.RewardValue;

        this.coinsNumFly = this.coinsNum * (coinTimes - 1);
        this.runFlyIconsAnimation();

        this.coinsNum *= coinTimes;

        let gameLevelInstance = game.instance.getGameLevelIntance();
        if (!!gameLevelInstance) {
            gameLevelInstance.addShareReward(coinTimes);
        }
    }
    ,

    onShareButton : function()
    {
        let btnComp = this.shareButton.getComponent(cc.Button);
        if (!!btnComp) {
            btnComp.enableAutoGrayEffect = true;
            btnComp.interactable = false;
            this.shareButton.stopAllActions();
            this.shareButton.scale = 1;
        }

        soundManager.instance.playSound('button');

        let config = configLoader.instance.getShareConfig(2);
        let shareType = config.ShareQueue[0];
        let self = this;

        if (shareType == 1) {
            
            wechatUIPort.instance.shareToWechat((rsp)=>{                      
                if (rsp.success == true) {

                    self.addShareReward();
                }
            });

        }else if (shareType == 2) {

            wechatUIPort.instance.playAdVideo((rsp)=>{
                if (rsp.success == true) {
                    self.addShareReward();
                }
            });
        }
    }
    ,

    update : function(dt) 
    {       
        if (this.beginAnimation && this.coinsNow != this.coinsNum)
        {
            let animNum = Math.floor(utils.numberLerpToNumber(this.coinsNow, this.coinsNum,0.04*(dt/0.01667)));

            this.coinsNow = animNum;
            this.labelCoinsNum.string = animNum;
        }
    }
    ,
});
