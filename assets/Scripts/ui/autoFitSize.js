
cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        let winWidth = cc.visibleRect.width;
        let winHeight = cc.visibleRect.height;

        cc.log(' winsize .width =' + winWidth + ', wisize.height =' + winHeight);

        this.node.width = winWidth;
        this.node.height = winHeight;

        cc.log(' this.node.width =' + this.node.width + ', this.node.height =' + this.node.height);
    },

    update (dt) {
        
        let winWidth = cc.visibleRect.width;
        let winHeight = cc.visibleRect.height;

        cc.log(' winsize .width =' + winWidth + ', wisize.height =' + winHeight);

        this.node.width = winWidth;
        this.node.height = winHeight;

        cc.log(' this.node.width =' + this.node.width + ', this.node.height =' + this.node.height);
    },
});
