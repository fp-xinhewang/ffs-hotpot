
var soundManager = require('soundManager');
var utils = require('utils');
var wechatUIPort = require('wechatUIPort');
var configLoader = require('configLoader');

cc.Class({
    extends: cc.Component,

    properties: {
        maskLayer : cc.Node,
        root : cc.Node,
        prgressBar : cc.ProgressBar,
        lightNode : cc.Node,
        shareImageBk : cc.Node,
        shareImageNode : cc.Node,
        iconShareNode : cc.Node,
        iconVideoNode : cc.Node,
        shareText : cc.Label
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        if (!!this.maskLayer) {
            this.maskLayer.runAction(cc.fadeTo(0.2,128));
        }

        if (!!this.root) {
            this.root.setScale(0.5);
            this.root.runAction(cc.scaleTo(0.2,1.0).easing(cc.easeBackOut()));
        }

        this.shareImageBk.active = false;
        this.timeElapse = 10;
        this.timeAll = 10;

        var actionBy = cc.rotateBy(2,-360);
        var act = cc.repeatForever(actionBy);
        this.lightNode.runAction(act);
        this.shareTimes = this.shareTimes || 0;
        
        this.maskLayer.width = cc.visibleRect.width;
        this.maskLayer.height = cc.visibleRect.height;
    },

    setShareTimes : function(times)
    {
        this.shareTimes = times;

        let config = configLoader.instance.getShareConfig(1);
        this.shareType = config.ShareQueue[this.shareTimes];

        this.iconShareNode.active = (this.shareType == 1);
        this.iconVideoNode.active = (this.shareType == 2);

        if (this.shareType == 1) {
            this.shareText.string = '分享获取';
        }
        else {
            this.shareText.string = '广告获取';
        }
    }
    ,

    setCallBack : function(callBack)
    {
        this.shareCallBack = callBack;
    }
    ,

    onCloseButton : function()
    {
        soundManager.instance.playSound('button');
        this.node.destroy(true);

        if (!!this.shareCallBack) {
            this.shareCallBack('close');
        }
    }
    ,

    sendContinuePlay : function()
    {

    }
    ,

    onShareButton : function()
    {
        let config = configLoader.instance.getShareConfig(1);
        let self = this;

        if (!!config) {
            if (this.shareTimes < config.ShareQueue.length) {
                let shareType = config.ShareQueue[this.shareTimes];

                if (shareType == 1) {
                    wechatUIPort.instance.shareToWechat((rsp)=>{                      
                        if (rsp.success == true) {
                            if (!!self.shareCallBack) {
                                self.shareCallBack('sharedSuccess');
                                self.node.destroy(true);
                            }
                        }
                    });
                }
                else if (shareType == 2) {

                    wechatUIPort.instance.playAdVideo((rsp)=>{
                        if (rsp.success == true) {
                            if (!!self.shareCallBack) {
                                self.shareCallBack('sharedSuccess');
                                self.node.destroy(true);
                            }
                        }
                    });
                }
            }
        }

        
        /*
        this.shareImageBk.active = true;
        this.maskLayer.opacity = 255;
        this.root.opacity = 0;

        this.shareImageNode.removeAllChildren();

        let randImage = utils.randInt(10) + 1;
        let path = 'shareImage/' + randImage.toString();
        let self = this;

        //let winWidth = cc.visibleRect.width;
        let winHeight = cc.visibleRect.height;

        let imageSpr = utils.createSprite(path, ()=>{

            let w = self.shareImageBk.width;
            let h = self.shareImageBk.height;

            let pos0 = self.shareImageBk.convertToWorldSpace(new cc.Vec2(0,h));
            let pos1 = self.shareImageBk.convertToWorldSpace(new cc.Vec2(w,0));

            let params = {
                x : pos0.x,
                y : winHeight - pos0.y,
                width : pos1.x - pos0.x,
                height : pos0.y - pos1.y
            };

            wechatUIPort.instance.shareToWechat(params);
            self.recovery();
            self.node.destroy(true);
        });
        this.shareImageNode.addChild(imageSpr);
        soundManager.instance.playSound('button');

        if (!!this.shareCallBack) {
            this.shareCallBack('sharedSuccess');
        }

        */
    }
    ,

    update (dt) {
      
        if (this.timeElapse < 0) {
            this.onCloseButton();
            return;
        }

        let progress = this.timeElapse/this.timeAll;
        if (progress < 0) {
            progress = 0;
        }
        
        if (progress > 1) {
            progress > 1;
        }

        this.prgressBar.progress = progress;

        this.timeElapse -= dt;
    },
});
