var utils = require('utils');
var configLoader = require('configLoader');

var wechatUIPort = cc.Class({
    extends: cc.Component,

    properties: {
    },
    
    statics: {

        instance : null
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},
    start () {
        
        wechatUIPort.instance = this;

        this.shareDesc = {
            1 : ['加盟我的火锅店吧，一起赚大钱！','火锅店分红啦，赚了一大笔钱！'],
            2 : ['火锅店开张前三单免费，快来免费品尝！'],
            3 : ['老板火锅店的装潢，由你决定！'],
            4 : ['吃火锅减肥的秘诀，快来试一试'],
            5 : ['吃火锅比赛我快输了，快来帮帮我'],
            6 : ['下金币雨啦，快来收金币！','连成一条线，就可赚大钱！'],
            7 : ['新春吉祥，吃顿火锅怎么样！','过年最大的幸福，就是和家里一起吃火锅'],
            8 : ['火锅店被吃垮，老板负债累累','我的火锅店百废待兴，快来捧场','火锅店老板跟着小姨子跑了，快来看看'],
            9 : ['财神到，送红包，手快有，手慢无','大年初一财神到，领个红包笑一笑','玩游戏送红包，话费领不同'],
            10 : ['快来涮火锅，隔壁的小孩都馋哭了','这游戏饿的时候玩，味道好极了！']
        };

        this.sharedImageId = 0;

        let self = this;

        if (utils.isWeChat())
        {
            //显示分享菜单
            wx.showShareMenu();

            wx.onShareAppMessage(function () 
                {

                    let titleString = '';
                    let randImage = utils.randInt(10) + 1;
                    if (!!self.shareDesc[randImage]) {
                        let strArray = this.shareDesc[imageId];
                        let len = strArray.length;
                        
                        if (len == 0) {
                            titleString = strArray[0];
                        }
                        else {
                            let idx = utils.randInt(len);
                            titleString = strArray[idx];
                        }
                    }

                    //let imageUrlStr = 'https://hotpot-cn-static.campfiregames.cn/sharedImages/1.jpg'
                    return {
                        title: titleString
                        //imageUrl: imageUrlStr
                    }
                    
                    //被转发的时候
                }
            );
        }

        this.adVideoObj = null;
    }, 

    playAdVideo : function(callBack)
    {

        if (this.adVideoObj == null) {

            let config = configLoader.instance.getBaseConfig();
            if (!!config) {
                let adUnid = config.AdId;
                if (!!adUnid) {
                    if (adUnid != '0.0') {
                        this.adVideoObj = wx.createRewardedVideoAd({adUnitId: adUnid});
                    }
                }
            }
        }

        if (this.adVideoObj == null) {

            if (!!callBack) {
                callBack({
                    success : true
                });        
            }

            return;
        }
        
        this.adVideoObj.onLoad(
            ()=> 
            {
                cc.log('ad load success!');
            }
        );

        this.adVideoObj.onError(err => 
            {
                cc.log('play ad video error !!');
                cc.log(err);
            }
        );

        this.adVideoObj.show().then(
            ()=> 
            {
                cc.log('begin to play ad video');
            }
        );

        this.adVideoObj.onClose(res => {
            // 用户点击了【关闭广告】按钮
            // 小于 2.1.0 的基础库版本，res 是一个 undefined
            if (res && res.isEnded || res === undefined) {
              // 正常播放结束，可以下发游戏奖励

                if (!!callBack) {
                    callBack({
                        success : true
                    });        
                }
            } else {
                // 播放中途退出，不下发游戏奖励
                if (!!callBack) {
                    callBack({
                        success : true
                    });
                }
            }
        });
    }
    ,

    captureShare : function()
    {
        let params = {
            imgId : this.sharedImageId,
            x : 0,
            y : 0,
            width : this.sharedImage.width,
            height : this.sharedImage.height
        };

        let titleString = '';
        let imageId = params.imgId;
        if (!!this.shareDesc[imageId]) {
            let strArray = this.shareDesc[imageId];
            let len = strArray.length;
            
            if (len == 0) {
                titleString = strArray[0];
            }
            else {
                let idx = utils.randInt(len);
                titleString = strArray[idx];
            }
        }

        if (utils.isWeChat())
        {
            wx.shareAppMessage({
                title: titleString,
                imageUrl: canvas.toTempFilePathSync({
                    x : params.x,
                    y : params.y,
                    width : params.width,
                    height : params.height,
                    destWidth: 500,
                    destHeight: 400
                })
            });
        }

        if (!!this.shareCallBack) {
            this.shareCallBack(
                {
                    success : true
                }
            );
        }

        this.sharedImage.destroy(true);
        this.sharedImage = null;
    }
    ,

    shareToWechat : function(callBack = null)
    {
        this.shareCallBack = callBack;
        //微信聊天分享
        // https://developers.weixin.qq.com/minigame/dev/tutorial/open-ability/share/share.html

        let randImage = utils.randInt(10) + 1;
        let path = 'shareImage/' + randImage.toString();
        let self = this;

        this.sharedImageId = randImage;
        this.sharedImage = utils.createSprite(path, ()=>{

            let delayTime = cc.delayTime(0.02);
            let callBack = cc.callFunc(self.captureShare, self);
            let seq = cc.sequence(delayTime, callBack);

            this.node.runAction(seq);
        });

        let scene = cc.director.getScene();
        this.sharedImage.anchorX = 0.0;
        this.sharedImage.anchorY = 1.0;
        this.sharedImage.x = 0;
        this.sharedImage.y = cc.visibleRect.height;

        scene.addChild(this.sharedImage, cc.macro.MAX_ZINDEX);

        /*

        使用审核通过的转发图片
            定义

            开发者可以将转发图片提前通过 MP 系统上传，并由平台进行审核。审核通过的图片会下发对应的图片编号和图片地址，给到开发者调用。（图片编号和图片地址必须一起使用，缺一不可）
            注：审核通过的图片，并不完全代表无任何问题，线上的转发行为依然会受平台监管，请开发者遵守运营规范相关要求。
            调用

            在转发 wx.shareAppMessage 和 wx.onShareAppMessage 接口中，传入 imageUrlId 和 imageUrl 参数。

            const id = '' // 通过 MP 系统审核的图片编号
            const url = '' // 通过 MP 系统审核的图片地址
            wx.shareAppMessage({
            imageUrlId: id,
            imageUrl: url
            })

            wx.onShareAppMessage(function () {
            return {
                imageUrlId: id,
                imageUrl: url
            }
            })
        */
    }
    ,

    playAd : function()
    {
        //微信播放广告
        if (utils.isWeChat())
        {
            
        }
    }
    ,

    // update (dt) {},
});
