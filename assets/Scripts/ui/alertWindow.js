var soundManager = require('soundManager');

cc.Class({
    extends: cc.Component,

    properties: {

        root : cc.Node,
        maskLayer : cc.Node,
        closeButton : cc.Button,
        okButton : cc.Button,
        okLabel : cc.Label,
        alertString : cc.Label
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        if (!!this.maskLayer) {
            this.maskLayer.runAction(cc.fadeTo(0.2,128));
        }

        if (!!this.root) {
            this.root.setScale(0.5);
            this.root.runAction(cc.scaleTo(0.2,1.0).easing(cc.easeBackOut()));
        }
        
        soundManager.instance.playSound('alert');

        
        this.maskLayer.width = cc.visibleRect.width;
        this.maskLayer.height = cc.visibleRect.height;
    },

    setCallBack : function(cBack)
    {
        this.callBack = cBack;
    }
    ,

    setString : function(str)
    {
        this.alertString.string = str;
    }
    ,

    setButtonString : function(str) 
    {
        this.okLabel.string = str;
    }
    ,

    onOkButton : function()
    {
        this.node.destroy(true);

        if (!!this.callBack) {
            this.callBack(true);
        }

        soundManager.instance.playSound('button');
    }
    ,

    onCloseButton : function()
    {
        this.node.destroy(true);

        if (!!this.callBack) {
            this.callBack(false);
        }

        soundManager.instance.playSound('button');
    }
    ,
    // update (dt) {},
});
