var game = require('game');
var ui = require('ui')
var soundManager = require('soundManager');
var utils = require('utils');
var configLoader = require('configLoader');

cc.Class({
    extends: cc.Component,

    properties: {
        
        potTitle : cc.Label,
        potImage : cc.Node,
        maxCoinsLabel : cc.Label,
        root: cc.Node,
        themeNode0 : cc.Node,
        themeNode1 : cc.Node,
        buttonName : cc.Label,
        buttonStart : cc.Button
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},
    updateUI : function( params )
    {
        this.themeNode0.active = params.theme;
        this.themeNode1.active = !params.theme;

        this.specId = params.specId;

        let img = utils.createSprite('/pot/' + this.specId);
        this.potImage.addChild(img);

        this.maxCoinsLabel.string = game.instance.getStageHistory(this.specId);;
        this.potTitle.string = params.name;

        this.updateLevel();
    }
    ,

    updateLevel : function()
    {
        let config = configLoader.instance.getStageConfig(this.specId);

        this.buttonStart.enableAutoGrayEffect = true;
        let curLevel = game.instance.getLevel();
        if (curLevel < config.UnlockLevel)
        {
            this.buttonName.string = config.UnlockLevel.toString() + '级解锁';
            this.buttonStart.interactable = false;
        }
        else {
            this.buttonName.string = '开始挑战';
            this.buttonStart.interactable = true;
        }
    }
    ,

    updateMaxCoins : function()
    {
        this.maxCoinsLabel.string = game.instance.getStageHistory(this.specId);;
    }
    ,

    onStartButton : function()
    {
        game.instance.startGameLevel(this.specId);

        soundManager.instance.playSound('button');
    }
    ,

    onShowBoardButton : function()
    {
        ui.instance.showAlert('还木有排行榜!');

        soundManager.instance.playSound('button');
    }
    ,

    start () {

    },

    // update (dt) {},
});
