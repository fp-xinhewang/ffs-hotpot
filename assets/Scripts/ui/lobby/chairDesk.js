var utils = require('utils');
var game = require('game');

cc.Class({
    extends: cc.Component,

    properties: {
        spineComponent : sp.Skeleton,
        npcPos0 : cc.Node,
        npcPos1 : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    start () {
        this.initialize = false;
    },

    setStartSaodong : function(val)
    {
        if (this.startSaosong == val) {
            return;
        }

        let lobbyDecInstance = game.instance.getLobbyDecoration();
        if (lobbyDecInstance == null) {
            return;
        }

        this.startSaosong = val;
        if (this.startSaosong)
        {
            let anim = 'saodong' + utils.randInt(3);
            this.spineComponent.setAnimation(0, anim, false);

            this.npcPos0.active = true;
            this.npcPos1.active = true;


            let npc0 = lobbyDecInstance.getRandNpcId();
            let npc1 = lobbyDecInstance.getRandNpcId();

            this.addNpc(this.npcPos0, npc0);
            this.addNpc(this.npcPos1, npc1);
        }
        else {
            this.spineComponent.setAnimation(0, 'install', false);
            this.npcPos0.active = false;
            this.npcPos1.active = false;
        }

        this.spineComponent.setCompleteListener((trackEntry) => 
        {
            if (this.startSaosong) {
                let anim = 'saodong' + utils.randInt(3);
                this.spineComponent.setAnimation(0, anim, false);
            }
            else
            {
                this.spineComponent.setAnimation(0, 'idle', false);
            }
        });

        this.initialize = true;
    },

    addNpc :function(nodeParent, npcId)
    {
        let npcTemp = npcId;

        let npcSpine = utils.createSpineNode('npc/' + npcTemp + '/' + npcTemp, 'eating', true, '',

            (node, animation) =>{
                var spine = node.getComponent(sp.Skeleton);
                if (!!spine) {

                    spine.timeScale = Math.random() * 0.5 +0.5;

                    spine.setCompleteListener((trackEntry) => 
                    {
                        spine.timeScale = Math.random() * 0.5 +0.5;
                    });
                }
            }
        );

        nodeParent.addChild(npcSpine);
    }
    ,

    update (dt) {

        if (this.initialize == false) {
            this.setStartSaodong(true);
        }
    },
});
