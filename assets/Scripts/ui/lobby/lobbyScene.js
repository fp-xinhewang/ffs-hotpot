var game = require('game');
var soundManager = require('soundManager');
var webRequestManager = require('webRequestManager');
var utils = require('utils');
var ui = require('ui');

var lobbyScene = cc.Class({
    extends: cc.Component,

    properties: {
        canvas : cc.Node,
        storeUINode:cc.Node,
        stageUINode:cc.Node,
        canvasNode:cc.Node,
        coinsCollectNode:cc.Node,
        coinsBarNode:cc.Node,
        guideLayer : cc.Node,
        lobbyPlayButton : cc.Node,
        lobbyDecorationButton : cc.Node,
        lobbyPosButton : cc.Node,
        menuButtons : cc.Node,
        bgmEnabledFlag : cc.Node,
        effectSoundEnabledFlag : cc.Node,
        menuMaskLayer : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    isStorePanelOpened : function()
    {
        let uiComp = this.storeUINode.getComponent('storeUI');
        if (!!uiComp) {
            let ret = uiComp.isShowing();
            return ret;
        }

        return false;
    }
    ,

    hasBuyDecoration : function()
    {
        return this.alreadyBuyDecoration;
    }
    ,

    hasCollectCoins : function()
    {
        return this.alreadyCollectCoins;
    }
    ,

    isInGuide : function()
    {
        let guideStatus = game.instance.getGuideStatus();
        if (!!guideStatus) {

            let inGuideStatus = guideStatus.isInLobbyGuideStatus();
            return inGuideStatus;
        }

        return false;
    }
    ,

    onBtnOpenStoreUI : function() 
    {
        cc.log('open store button clicked!!!');

        let uiComp = this.storeUINode.getComponent('storeUI');
        uiComp.show();
    }
    ,

    onBtnPosTouch : function()
    {
        this.alreadyCollectCoins = true;
    }
    ,

    onEnterStage : function() 
    {
        let uiComp = this.stageUINode.getComponent('stageMenu');
        uiComp.show();
    }
    ,

    buyObject : function(specId)
    {
        let uiComp = this.storeUINode.getComponent('storeUI');
        uiComp.onCloseButton();

        let lobbyDec = this.node.getComponent('lobbyDecoration');
        if (!!lobbyDec) {

            lobbyDec.updateUI(specId);

            this.alreadyBuyDecoration = true;
        }
    }
    ,

    collectCoinsAnimation : function(nodeFrom = null)
    {
        let node0 = nodeFrom;
        if (node0 == null)
        {
            node0 = this.coinsCollectNode;
        }

        let imgPath = 'general/coins';

        for (let i = 0; i <10; i ++) {

            ui.instance.flySpriteToPosition(imgPath, node0, this.coinsBarNode,
                (i +1)*0.175, (i +1)*0.2, 0.1);
        }

        soundManager.instance.playSound('collect_coin');
    }
    ,

    onDestroy()
    {
        game.instance.lobbySceneInstance = null;
    }
    ,

    getButtonPosition : function(val)
    {
        let buttonNodes = [this.lobbyPlayButton, this.lobbyDecorationButton, this.lobbyPosButton];

        let node0 = buttonNodes[val];
        if (!!node0) {

            let w = node0.width * 0.5;
            let h = node0.height* 0.5;

            let pos = node0.convertToWorldSpace(new cc.Vec2(w, h));
            return pos;
        }

        return new cc.Vec2(0, 0);
    }
    ,

    start () {

        this.menuButtons.active = false;
        this.storeUINode.visible = false;

        this.alreadyBuyDecoration = false;
        this.alreadyCollectCoins = false;

        this.bgmEnabledFlag.active = !soundManager.instance.isBgmEnabled();
        this.effectSoundEnabledFlag.active = !soundManager.instance.isSoundEnabled();
        
        soundManager.instance.playBgm('lobby');
        //soundManager.instance.playBgm('lobby' + utils.randInt(2));
        game.instance.lobbySceneInstance = this;

        let guideStatus = game.instance.getGuideStatus();
        if (!!guideStatus) {
            let inGuideStatus = guideStatus.isInLobbyGuideStatus();
            if (inGuideStatus == false) {
                this.guideLayer.active = false;
            }
        }
    }
    ,

    onTestShowLevelup : function()
    {
        ui.instance.showLevelupPanel();
    }
    ,

    onSettingButton : function()
    {
        this.menuOpened = this.menuOpened || false;

        if (this.menuOpened) {
            this.menuButtons.active = false;
            this.menuMaskLayer.active = false;
        }
        else {
            this.menuButtons.active = true;
            this.menuMaskLayer.active = true;
        }

        this.menuOpened = !this.menuOpened;

        soundManager.instance.playSound('button');
    }
    ,

    onCloseSettingButton : function()
    {
        if (this.menuOpened == true) {
            this.menuOpened = false;
            this.menuButtons.active = false;
            this.menuMaskLayer.active = false;
        }
    }
    ,

    onBgmButton : function()
    {
        let bgmEnabled = soundManager.instance.isBgmEnabled();
        bgmEnabled = !bgmEnabled;
        soundManager.instance.setBgmEnabled(bgmEnabled);

        this.bgmEnabledFlag.active = !bgmEnabled;

        soundManager.instance.playSound('button');
    }
    ,

    onEffectSoundButton : function()
    {
        let effectSoundEnabled = soundManager.instance.isSoundEnabled();
        effectSoundEnabled = !effectSoundEnabled;
        soundManager.instance.setSoundEnabled(effectSoundEnabled);

        this.effectSoundEnabledFlag.active = !effectSoundEnabled;

        soundManager.instance.playSound('button');
    }
    ,

    onShowTaskPanel :function()
    {
        ui.instance.showTaskPanel();
    }
    ,

    onTestCapture : function()
    {
        ui.instance.showCapturePanel();
    }
    ,


});
