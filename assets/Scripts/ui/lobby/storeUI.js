var utils = require('utils');
var game = require('game');
var data = require('datas');
var configLoader = require('configLoader');
var soundManager = require('soundManager');

var storeUI = cc.Class({
    extends: cc.Component,

    properties: {

        root : cc.Node,
        storeView : cc.Node,
        storeContent : cc.Node,
        storeItemPrefab : cc.Prefab,
        maskLayer : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    isShowing : function()
    {
        return this.showing;
    }
    ,

    show :function()
    {
        this.node.x = 0;
        this.maskLayer.active = true;
        
        if (!!this.maskLayer) {
            this.maskLayer.runAction(cc.fadeTo(0.2,128));
        }

        if (!!this.root) {
            this.root.setScale(0.5);
            this.root.runAction(cc.scaleTo(0.2,1.0).easing(cc.easeBackOut()));
        }

        this.resort();
        soundManager.instance.playSound('panel_open');
        this.showing = true;

        this.maskLayer.width = cc.visibleRect.width;
        this.maskLayer.height = cc.visibleRect.height;
    }
    ,

    onLoad () {
        
        this.showing = false;
        this.itemHeight = 0;
        this.nodeItems = [];

        this.init();
    },

    onCloseButton : function()
    {
        soundManager.instance.playSound('button');
        
        this.maskLayer.active = false;
        this.node.x = -10000;

        this.showing = false;
    }
    ,

    resort : function()
    {

        let levNumber = game.instance.getLevel();
        let historyMap = game.instance.getStoreHistory();

        let notHas = [];
        let alreadyHas = [];

        let needUpdatePos = false;

        let lastNode = null;
        let firstNode = null;

        let theBottomY = 0;
        let theTopY = -1000;

        for (let i = 0; i < this.nodeItems.length; i ++)
        {
            let needUpdateUI = false;

            let sItem = this.nodeItems[i].getComponent("storeItem");
            sItem.showSep();

            if (this.nodeItems[i].y < theBottomY) {
                theBottomY = this.nodeItems[i].y;
                lastNode = this.nodeItems[i];
            }

            if (this.nodeItems[i].y > theTopY) {
                theTopY = this.nodeItems[i].y;
                firstNode = this.nodeItems[i];
            }

            if (sItem.unlocked == false) {

                let decConfig = configLoader.instance.getDecorationConfig(sItem.id);
                if (!!decConfig) {
                    if (levNumber >= decConfig.UnlockLevel) {
                        sItem.unlocked = true;
                        needUpdateUI = true;
                    }
                }
            }

            if (sItem.already == false)
            {
                if (historyMap[sItem.id] == 1) {
                    sItem.already = true;
                    needUpdateUI = true;
                    needUpdatePos = true;
                }
            }

            if (needUpdateUI == true)
            {
                sItem.updateUI();
            }
        
            if (sItem.already == true) {
                alreadyHas[alreadyHas.length] = i;
            }
            else {
                notHas[notHas.length] = i;
            }
        }

        if (needUpdatePos == true) 
        {
            let itemIdx = 0;
            let theTopY = -1000;

            for (let i = 0; i < notHas.length; i ++)
            {
                this.nodeItems[notHas[i]].y = - itemIdx * this.itemHeight;
                itemIdx ++;

                lastNode = this.nodeItems[notHas[i]];

                if (lastNode.y > theTopY) {
                    theTopY = lastNode.y;
                    firstNode = lastNode;
                }

            }

            for (let i = 0; i < alreadyHas.length; i ++)
            {
                this.nodeItems[alreadyHas[i]].y =  - itemIdx * this.itemHeight;
                itemIdx ++;
                
                lastNode = this.nodeItems[alreadyHas[i]];

                if (lastNode.y > theTopY) {
                    theTopY = lastNode.y;
                    firstNode = lastNode;
                }
            }
        }
        
        if (lastNode != null)
        {
            let sItem = lastNode.getComponent("storeItem");
            sItem.hideSep();
        }

        if (firstNode != null)
        {       
            for (let i = 0; i < this.nodeItems.length; i ++)
            {
                let sItem = this.nodeItems[i].getComponent("storeItem");

                if (this.nodeItems[i] != firstNode) {
                    sItem.removeHandNode();
                }
                else {

                    let lobbyInstance = game.instance.getLobbyScene();
                    if (!!lobbyInstance) {
        
                        if (lobbyInstance.isInGuide() == true) {
                            sItem.showHandToBuyButton();
                        }
                    }
                }
            }
        }
    }
    ,

    init () {

        let itemNum = configLoader.instance.StoreSpec_conf_array.length;

        let nodeItem0 = cc.instantiate(this.storeItemPrefab);
        let w = nodeItem0.width;
        let h = nodeItem0.height;
        this.itemHeight = h;

        let vh = this.storeView.height;

        let allHeight = itemNum * h;
        if (allHeight < vh) {
            allHeight = vh;
        }

        this.storeContent.setContentSize(w, allHeight);

        let historyMap = game.instance.getStoreHistory();

        let itemIdx = 0;
        let levNumber = game.instance.getLevel();

        for (let i = 0; i < itemNum; i ++)
        {
            let config = configLoader.instance.StoreSpec_conf_array[i];
            if (historyMap[config.SpecId] != 1)
            {
                let nodeItem = cc.instantiate(this.storeItemPrefab);

                let sItem = nodeItem.getComponent("storeItem");
    
                sItem.id = config.SpecId;
                sItem.Coins = config.Coins;
                sItem.already = false;
                sItem.descString = config.Desc;

                let decConfig = configLoader.instance.getDecorationConfig(config.SpecId);
                if (!!decConfig) {
                    sItem.expVal = decConfig.Exp;
                    sItem.unlockLevel = decConfig.UnlockLevel;
                    if (levNumber >= decConfig.UnlockLevel) {
                        sItem.unlocked = true;
                    }
                }
    
                nodeItem.setPosition(0, - itemIdx * h);
    
                this.storeContent.addChild(nodeItem);

                this.nodeItems[this.nodeItems.length] = nodeItem;
    
                sItem.updateUI();

                itemIdx ++;
            }
        }

        for (let i = 0; i < itemNum; i ++)
        {
            let config = configLoader.instance.StoreSpec_conf_array[i];
            if (!config) {
                break;
            }

            if (historyMap[config.SpecId] == 1)
            {
                let nodeItem = cc.instantiate(this.storeItemPrefab);

                let sItem = nodeItem.getComponent("storeItem");
    
                sItem.id = config.SpecId;
                sItem.Coins = config.Coins;
                sItem.already = true;
                sItem.descString = config.Desc;
                
                let decConfig = configLoader.instance.getDecorationConfig(config.SpecId);
                if (!!decConfig) {
                    sItem.expVal = decConfig.Exp;
                    sItem.unlockLevel = decConfig.UnlockLevel;
                    if (levNumber >= decConfig.UnlockLevel) {
                        sItem.unlocked = true;
                    }
                }

                nodeItem.setPosition(0, - itemIdx * h);
    
                this.storeContent.addChild(nodeItem);

                this.nodeItems[this.nodeItems.length] = nodeItem;
    
                sItem.updateUI();

                itemIdx ++;
            }
        }

        //this.storeContent.opacity = 0;
        //this.storeContent.runAction(cc.fadeIn(0.5));
    },
});
