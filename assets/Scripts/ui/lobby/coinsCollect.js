var game = require('game');
var configLoader = require('configLoader');
var utils = require('utils');
var ui = require('ui');
var configLoader = require('configLoader');
var webRequestManager = require('webRequestManager');

cc.Class({
    extends: cc.Component,

    properties: {

        coinsNumLabel : cc.Label,
        spineCompCoins : sp.Skeleton
    },

    start () {
        this.tim = 0;
        this.coinsNow = 0;
        this.coinsNum = 0;
        this.spineComp = this.getComponent(sp.Skeleton);
        this.labelBasePos = this.coinsNumLabel.node.y - 15;
        this.updateCoins();

        //this.coinsNum = 40000;
        this.coinsNow = this.coinsNum;
        this.hasShared = false;
    },

    addShareReward : function()
    {
        this.hasShared = true;

    }
    ,

    sendToServer : function(val)
    {
        this.updateCoins();

        let times = 1;
        if (this.hasShared == true) {
            let config = configLoader.instance.getShareConfig(3);
            times = config.RewardValue;   
        }
        let coinsAdd = this.coinsNum*times;

        game.instance.collectCoins(coinsAdd);

        webRequestManager.instance.requestCollectCoins(coinsAdd, this.hasShared, (rsp)=>{

            cc.log('==========COLLECT COINS RSP=====');
            cc.log(rsp);
        });

        this.updateCoins();
    }
    ,

    onButtonCollect : function()
    {
        this.updateCoins();

        let guideStatus = game.instance.getGuideStatus();
        if (!!guideStatus) {
            guideStatus.updateCollectCoins();
        }

        let self = this;

        if (this.coinsNum >= 1) 
        {
            this.hasShared = false;
            ui.instance.showWelcomePanel(this.coinsNum, (eventName)=>{

                if (eventName == 'sharedSuccess') {
                    self.addShareReward();
                }
                else if (eventName == 'close') {
                    self.sendToServer();
                }
                },
            );
        }
    }
    ,

    updateCoins : function()
    {
        let collectStruct = game.instance.getPosCoinsNumber()
        this.coinsNum = collectStruct.coins;
        let collectLimit = collectStruct.limit;

        if (this.coinsNum >= collectLimit)
        {
            this.coinsNum = collectLimit;

            this.spineComp.setAnimation(0,'full0', true);
            this.spineCompCoins.setAnimation(0,'full0', true);
        }
        else if (this.coinsNum > 0) {
            this.spineComp.setAnimation(0,'idle', true);
            this.spineCompCoins.setAnimation(0,'idle', true);
        }
        else {
            this.spineComp.setAnimation(0,'idle0', true);
            this.spineCompCoins.setAnimation(0,'idle0', true);
        }

        //this.coinsNumLabel.string = this.coinsNum;
    }
    ,

    update (dt) {

        this.tim += dt;

        if (this.tim >= 1) {

            this.updateCoins();

            this.tim = 0;
        }

        let c_node = this.spineComp.findBone('coins_node');

        //cc.log(' c_node.b =' + c_node.worldX + ' , c_node.c =' + c_node.worldY);
        //this.coinsNumLabel.x = c_node.worldX;
        this.coinsNumLabel.node.y = this.labelBasePos + c_node.worldY;
        //this.coinsNumLabel.node.y += dt;

        //cc.log('==== this.coinsNum =' + this.coinsNum);

        let animNum = Math.floor(utils.numberLerpToNumber(this.coinsNow, this.coinsNum,0.125*(dt/0.01667)));

        this.coinsNow = animNum;
        this.coinsNumLabel.string = animNum;
    },
});
