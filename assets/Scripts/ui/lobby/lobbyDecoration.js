var utils = require('utils');
var game = require('game');
var soundManager = require('soundManager');

cc.Class({
    extends: cc.Component,

    properties: {

        unNameNode: cc.Node,
        carpetNode: cc.Node,
        doorPictureNode: cc.Node,
        fishTankNode1: cc.Node,
        fishTankNode2: cc.Node,
        flowerPotNode1: cc.Node,
        flowerPotNode2: cc.Node,
        flowerPotNode3: cc.Node,
        wallPictureNode1: cc.Node,
        wallPictureNode2: cc.Node,
        deskChairNode1 : cc.Node,
        deskChairNode2 : cc.Node,
        tableLampNode1 : cc.Node,
        tableLampNode2 : cc.Node,
        lightNode1 : cc.Node,
        treeNode1 : cc.Node,
        treeNode2 : cc.Node,
        windowNode : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},
    onDestroy()
    {
        game.instance.lobbyDecorationInstance = null;
    }
    ,

    start () {
        
        game.instance.lobbyDecorationInstance = this;

        this.nodes = {
            specId1 : this.unNameNode,
            specId2 : this.carpetNode,
            specId3 : this.doorPictureNode,
            specId4 : this.fishTankNode1,
            specId5 : this.fishTankNode2,
            specId6 : this.flowerPotNode1,
            specId7 : this.flowerPotNode2,
            specId8 : this.flowerPotNode3,
            specId9 : this.wallPictureNode1,
            specId10 : this.wallPictureNode2,
            specId11 : this.deskChairNode1,
            specId12 : this.deskChairNode2,
            specId13 : this.tableLampNode1,
            specId14 : this.tableLampNode2,
            specId15 : this.lightNode1,
            specId16 : this.treeNode1,
            specId17 : this.treeNode2,
            specId18 : this.windowNode
        };

        let history = game.instance.getStoreHistory();

        for (let i =1; i <= 18; i ++) 
        {
            let node0 = this.nodes['specId' + i];
            if (!!node0) {

                let script = node0.getComponent('decorationAnimation');
                if (!!script) {
                    script.init(i);
                }

                if (history[i] == 1) {
                    node0.active = true;
                }
                else {
                    node0.active = false;
                }
            }
        }

        this.randNpcIdx = 0;
        this.randNpcNum = 0;

        this.randNpcDisplay();
    },

    randNpcDisplay : function()
    {
        this.npcArray = [10001,10002,10003,10004,10005,10006,10007,11000,11001];
        let arrLen = this.npcArray.length;

        for (let i = 0; i < arrLen*4; i ++) {
            let x0 = utils.randInt(arrLen);
            let y0 = utils.randInt(arrLen);

            let m = this.npcArray[x0];
            this.npcArray[x0] = this.npcArray[y0];
            this.npcArray[y0] = m;
        }

        this.randNpcIdx = 0;
        this.randNpcNum = arrLen;
    }
    ,

    getRandNpcId : function()
    {
        if (this.randNpcNum > 0) {
            let idx = (this.randNpcIdx + 1)%this.randNpcNum;
            let retId = this.npcArray[idx];

            this.randNpcIdx = idx;

            return retId;
        }

        return 10001;
    }
    ,

    updateUI : function(specId)
    {
        let node0 = this.nodes['specId' + specId];
        if (!!node0) {
            if (node0.active == false) {
                node0.active = true;

                if (specId == 11 || specId == 12) 
                {
                    let node1 = node0.getChildByName('seatNode');
                    let script = node1.getComponent('chairDesk');
                    if (!!script) {
                        script.setStartSaodong(false);
                    }
                }
                else {
                    let script = node0.getComponent('decorationAnimation');
                    if (!!script) {
                        script.playInstall();
                    }
                }

                soundManager.instance.playSound('deco_install');
            }
        }
    }

    // update (dt) {},
});
