
cc.Class({
    extends: cc.Component,

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    start () {
        this.specId = 0;
        this.spineComp = this.node.getComponent(sp.Skeleton);

        this.install = false;
        this.timeDelay = 0;
    },

    init : function(specId)
    {
        this.specId = specId;
        this.playDefaultAnimation();
    }
    ,

    playDefaultAnimation : function()
    {
        if (!!this.spineComp) {

            if (this.specId == 4) {
                this.spineComp.setAnimation(0, 'jellyfish', true);
            }
            else if (this.specId == 5) {
                this.spineComp.setAnimation(0, 'fish', true);
            }
            else {

                let animationsName = ['idle','animation','start'];

                for (let i =0; i <animationsName.length; i ++) {

                    if (this.spineComp.findAnimation(animationsName[i]) != null) {
                        this.spineComp.setAnimation(0, animationsName[i], true);
                    }
                }
            }
        }
    }
    ,

    playInstall : function() {
        
        if (!!this.spineComp) {

            if (this.specId == 4) {
                this.spineComp.setAnimation(0, 'install_jellyfish', false);
            }
            else if (this.specId == 5) {
                this.spineComp.setAnimation(0, 'install_fish', false);
            }else 
            {
                this.spineComp.setAnimation(0, 'install', false);
            }

            this.spineComp.setCompleteListener((trackEntry) => 
            {
                var animationName = trackEntry.animation ? trackEntry.animation.name : "";
                switch (animationName) {
                    case 'install':
                    case 'install_jellyfish':
                    case 'install_fish':
                    {
                        this.playDefaultAnimation();
                        break;
                    }
                }
            });
        }
    }
});
