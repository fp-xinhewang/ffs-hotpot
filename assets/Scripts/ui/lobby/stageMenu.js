var configLoader = require('configLoader');
var soundManager = require('soundManager');

cc.Class({
    extends: cc.Component,

    properties: {

        root : cc.Node,
        maskLayer : cc.Node,
        viewNode : cc.Node,
        contNode : cc.Node,
        stageItemPrefab : cc.Prefab
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    show : function()
    {
        this.node.x = 0;
        this.maskLayer.active = true;

        if (!!this.maskLayer) {
            this.maskLayer.runAction(cc.fadeTo(0.2,128));
        }

        if (!!this.root) {
            this.root.setScale(0.5);
            this.root.runAction(cc.scaleTo(0.2,1.0).easing(cc.easeBackOut()));
        }

        this.refreshItems();

        soundManager.instance.playSound('panel_open');

        this.maskLayer.width = cc.visibleRect.width;
        this.maskLayer.height = cc.visibleRect.height;
    }
    ,

    refreshItems : function()
    {
        for (let i = 0; i < this.nodeItems.length; i ++)
        {
            let itemScript = this.nodeItems[i].getComponent('stageItem');
            if (!!itemScript) {
                itemScript.updateMaxCoins();
                itemScript.updateLevel();
            }
        }
    }
    ,

    start () {

        this.nodeItems = [];

        let keys = Object.keys(configLoader.instance.StageSpecConf.StageSpecs);
        
        let itemNum = keys.length;

        let nodeItem0 = cc.instantiate(this.stageItemPrefab);
        let w = nodeItem0.width;
        let h = nodeItem0.height;

        let vh = this.viewNode.height;

        let allHeight = itemNum * h;
        if (allHeight < vh) {
            allHeight = vh;
        }

        this.contNode.setContentSize(w, allHeight);

        let themeRed = true;
        for (let i = 0; i < itemNum; i ++)
        {
            let config = configLoader.instance.StageSpecConf.StageSpecs[i+1];
            if (!config) {
                break;
            }
            
            let nodeItem = cc.instantiate(this.stageItemPrefab);
            nodeItem.setPosition(0, - i * h);
            this.contNode.addChild(nodeItem);

            let stageItem = nodeItem.getComponent('stageItem');
            let params = {
                specId : config.SpecId,
                theme : themeRed,
                name : config.Name
            };

            stageItem.updateUI(params);

            themeRed = !themeRed;

            this.nodeItems[this.nodeItems.length] = nodeItem;
        }

    },

    onCloseButton : function()
    {
        soundManager.instance.playSound('button');

        this.maskLayer.active = false;
        this.node.x = 10000;
        //this.node.removeFromParent(true);
    }
    ,

    update (dt) {

    },
});
