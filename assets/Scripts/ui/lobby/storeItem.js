var configLoader = require('configLoader');
var ui = require('ui');
var game = require('game');
var localStr = require('localString');
var soundManager = require('soundManager');
var webRequestManager = require('webRequestManager');
var utils = require('utils');

var storeItem = cc.Class({
    extends: cc.Component,

    properties: {

        storeIcon : cc.Node,
        storeName : cc.Label,
        expValue : cc.Label,
        priceValue : cc.Label,
        itemDesc : cc.Label,
        buyButton : cc.Button,
        sepNode : cc.Node,
        fingerPrefab : cc.Prefab
    },

    // LIFE-CYCLE CALLBACKS:  

    ctor :function()
    {
        this.id = 0;
        this.expVal = 0;
        this.Coins = 0;
        this.already = false;
        this.unlockLevel = 1;
        this.unlocked = false;
        this.descString = '';
    },

    onLoad : function() {        
        this.hasIcon = false;
    },

    hideSep : function()
    {
        this.sepNode.active = false;
    }
    ,

    showSep : function()
    {
        this.sepNode.active = true;
    }
    ,

    updateUI : function() {
        
        if (this.hasIcon == false) {
            let iconNode = utils.createSprite('icons/'+this.id);
            this.storeIcon.addChild(iconNode);
        }

        this.buyButton.enableAutoGrayEffect = true;

        let config = configLoader.instance.getItemConfig(this.id);
        if (!!config) {
            this.storeName.string = config.Name;
        }

        let expStr = '+' + this.expVal.toString();
        this.expValue.string = expStr;

        if (this.already) {
            this.priceValue.string = '已购买';
            this.buyButton.interactable = false;
        }else{

            if (this.unlocked) {
                this.priceValue.string = this.Coins;
                this.buyButton.interactable = true;
            }
            else {
                this.priceValue.string = this.unlockLevel.toString() + '级解锁';
                this.buyButton.interactable = false;
            }
        }

        this.itemDesc.string = this.descString;

        /*
        let coinsNow = 10000;//game.instance.getCoins();

        if (this.Coins > coinsNow) {
            this.buyButton.transition = 2;
            this.buyButton.interactable = false;
        }
        else {
            this.buyButton.transition = 1;
            this.buyButton.interactable = true;
        }
        */

    }
    ,

    showHandToBuyButton : function()
    {
        let buyHistory = game.instance.getStoreHistory();
        let buyNum = 0;
        for (let key in buyHistory) {
            buyNum ++;
        }
        if (buyNum > 0) {

            this.removeHandNode();
            
            return;
        }

        if (this.handNode == null) {

            this.handNode = cc.instantiate(this.fingerPrefab);

            this.buyButton.node.addChild(this.handNode);

            let w = this.buyButton.node.width;
            let h = this.buyButton.node.height;
        
            this.handNode.x = - w * 0.1;
            this.handNode.y = - h * 0.2;

            this.handNode.scale = 0.8;

            let fingerScript = this.handNode.getComponent('finger');
            if (!!fingerScript) {
                fingerScript.setAnimationEnabled(true);
            }
        }
    }
    ,

    removeHandNode : function()
    {
        if (this.handNode != null) {
            this.handNode.destroy();
            this.handNode = null;
        }
    }
    ,

    btnBuyPressed : function ()
    {
        let coinsNow = game.instance.getCoins();

        if (this.Coins > coinsNow) {

            let str0 = localStr.instance.getKeyStr('_keyLackCoins');
            let str1 = localStr.instance.getKeyStr('_keyOk');

            ui.instance.showAlert(str0,str1);
        }
        else {
            let str0 = localStr.instance.getKeyStr('_keyBuyObj');
            let str1 = localStr.instance.getKeyStr('_keyOk');
    
            ui.instance.showAlert(str0,str1,(isOk)=>{
                if (isOk) {
                    
                    game.instance.buyStoreDecoration(this.id,this.Coins);
                }
                else{
                    cc.log('CLOSE!!!');
                }
            });
        }

        soundManager.instance.playSound('button');
    }
    ,

    start () {

    },

    // update (dt) {},
});
