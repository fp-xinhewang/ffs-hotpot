

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        let winWidth = cc.visibleRect.width;
        let winHeight = cc.visibleRect.height;

        let m = winHeight/winWidth;
        
        if (m > 1.99) {
            let widget = this.getComponent(cc.Widget);
            if (!!widget)
            {
                widget.top += 60;
                widget.bottom += 60;
            }
        }
    },

    // update (dt) {},
});
