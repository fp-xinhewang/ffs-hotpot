
cc.Class({
    extends: cc.Component,

    properties: {

    },

    start () {

        let scale0 = cc.scaleTo(0.1,1.1);//.easing(cc.easeBackOut());
        let scale1 = cc.scaleTo(0.1,1);//.easing(cc.easeBackOut());
        let scale2 = cc.scaleTo(0.1,1.1);//.easing(cc.easeBackOut());
        let scale3 = cc.scaleTo(0.1,1);//.easing(cc.easeBackOut());
        let dly = cc.delayTime(1.5);

        let seq = cc.sequence(scale0, scale1, scale2, scale3, dly);
        let act = cc.repeatForever(seq);
        
        this.node.runAction(act);
    },

    // update (dt) {},
});
