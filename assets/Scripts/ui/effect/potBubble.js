let utils = require('utils');

cc.Class({
    extends: cc.Component,

    properties: {
        
        //bubbles : cc.Node[10]
    },

    // LIFE-CYCLE CALLBACKS:

    getRandomPosition : function ()
    {
        let radius = Math.floor(this.node.width * 0.5);
        let randRadus = utils.randInt(radius);
        let randAngle = utils.randInt(360);
        let radianAngle = randAngle/(180.0/3.1415926);

        let x0 = randRadus * Math.cos(radianAngle);
        let y0 = randRadus * Math.sin(radianAngle);

        return { x : x0 , y : y0};
    }
    ,

    onLoad () 
    {
        this.bubblePopTimeTick = 0;
        this.bubblePopTime = [];
        this.bubbleAnimStatus = [];

        let childsNum = this.node.children.length;

        for (let i = 0; i < this.node.children.length; i ++)
        {
            this.bubbleAnimStatus[i] = false;
            this.bubblePopTime[i] = Math.random()*childsNum;
            this.node.children[i].active = false;
        }

    },    

    start () {
        this.node.runAction(cc.fadeIn(10.0));
    },

    update (dt) 
    {
        this.bubblePopTimeTick += dt;

        let childsNum = this.node.children.length;

        for (let i = 0; i < childsNum; i ++)
        {
            if (this.bubbleAnimStatus[i] == false) {

                if (this.bubblePopTimeTick > this.bubblePopTime[i]) {
                    var spine = this.node.children[i].getComponent(sp.Skeleton);
                    if (!!spine) {
                        spine.setAnimation(2, 'animation', true);
                    }

                    this.node.children[i].active = true;
                    this.bubbleAnimStatus[i] = true;
                }
            }
        }
    },
});
