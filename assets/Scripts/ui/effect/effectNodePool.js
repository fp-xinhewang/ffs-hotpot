var utils = require('utils');
var game = require('game');

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.crashEffect = {};
        this.timeNow = 0;
        this.timeTick = 0; 
    },

    createNodeItem : function(crashName, pos, animationName, isDish)
    {
        let ret = {};
        ret.playTime = this.timeNow;
        ret.isPlaying = true;

        let animPath = '';
        if (isDish == true)
        {
            let stageId = game.instance.getGameLevelStage();
            animPath = 'fx/' + stageId + '/' + crashName + '/' + crashName;
        }
        else{
            animPath = 'fx/' + crashName + '/' + crashName;
        }

        let fx_node = utils.createSpineNode(animPath, animationName, false);
        fx_node.setPosition(pos);
        this.node.addChild(fx_node);

        ret.node = fx_node;

        return ret;
    }
    ,

    playCrash : function(crashName, pos, animationName = 'start', isDish = false)
    {
        let crashNameKey = crashName;
        if (isDish == true) {
            let stageId = game.instance.getGameLevelStage();
            crashNameKey = crashName + '_' + stageId
        }

        if (this.crashEffect[crashNameKey] == null) {
            this.crashEffect[crashNameKey] = {};
            this.crashEffect[crashNameKey].nodeArrays = [];
            this.crashEffect[crashNameKey].nodeArrays[0] = this.createNodeItem(crashName, pos, animationName,isDish);
            return;
        }

        let arr = this.crashEffect[crashNameKey].nodeArrays;
        let arr_len = arr.length;

        for (let i = 0; i < arr_len; i ++)
        {
            if (arr[i].isPlaying == false) {

                arr[i].node.active = true;
                let spine = arr[i].node.getComponent(sp.Skeleton);
                spine.setAnimation(0, animationName, false);
                arr[i].isPlaying = true;
                arr[i].playTime = this.timeNow;
                arr[i].node.setPosition(pos);
                return;
            }
        }

        this.crashEffect[crashNameKey].nodeArrays[arr_len] = this.createNodeItem(crashName, pos, animationName, isDish);
    }
    ,

    update (dt) {

        this.timeNow += dt;
        this.timeTick += dt;

        if (this.timeTick > 0.5) {


            for (let key in this.crashEffect) {

                let l = this.crashEffect[key].nodeArrays.length;
                for (let i = 0; i < l; i ++)
                {
                    if (this.crashEffect[key].nodeArrays[i].isPlaying == true) {

                        let tim = this.timeNow - this.crashEffect[key].nodeArrays[i].playTime;
                        if (tim > 0.9) {
                            this.crashEffect[key].nodeArrays[i].node.active =false;
                            this.crashEffect[key].nodeArrays[i].isPlaying = false;
                        }
                    }
                }
            }

            this.timeTick = 0;
        }
    },
});
