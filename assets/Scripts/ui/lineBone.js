var utils = require('utils');

cc.Class({
    extends: cc.Component,

    properties: {
        lineBonesNode : cc.Node,
        connectNumNode : cc.Node,
        connectNumLabel : cc.Label
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.pointsNode = [];
        this.linesNode = [];
        this.lastLen = 0;

        this.connectNumNode.active = false;
    },

    getPoint : function(idx)
    {
        if (this.pointsNode[idx] == null)
        {
            this.pointsNode[idx] = utils.createSprite('effect/line_point');
            this.lineBonesNode.addChild(this.pointsNode[idx]);

            return this.pointsNode[idx];
        }

        return this.pointsNode[idx];
    }
    ,

    getLine : function(idx)
    {
        if (this.linesNode[idx] == null)
        {
            this.linesNode[idx] = utils.createSprite('effect/line_connect');
            this.linesNode[idx].anchorY = 1;

            this.lineBonesNode.addChild(this.linesNode[idx]);

            return this.linesNode[idx];
        }

        return this.linesNode[idx];
    }
    ,

    clearNodes()
    {
        this.lastLen = 0;
        this.connectNumNode.active = false;

        for (let i =0; i < this.pointsNode.length; i ++)
        {
            if (!!this.pointsNode[i]) {
                this.pointsNode[i].stopAllActions();
                this.pointsNode[i].runAction(cc.fadeOut(0.1));
            }
        }

        for (let i =0; i < this.linesNode.length; i ++)
        {
            if (!!this.linesNode[i]) {
                this.linesNode[i].stopAllActions();
                this.linesNode[i].runAction(cc.fadeOut(0.05));
            }
        }
    }
    ,

    setPoints : function(pointsArray)
    {
        if (!!pointsArray)
        {
            let num = pointsArray.length;
            if (num > 0) {
                if (this.connectNumNode.active == false)
                {
                    this.connectNumNode.active = true;
                    this.connectNumNode.scale = 0;
                    this.connectNumNode.runAction(cc.scaleTo(0.2, 1).easing(cc.easeBackOut()));
                }
            }

            this.connectNumLabel.string = num;
            this.connectNumNode.x = pointsArray[num - 1].x;
            this.connectNumNode.y = pointsArray[num - 1].y + this.connectNumNode.height*0.3;

            for (let i =0; i < num; i ++)
            {
                let pt = this.getPoint(i);
                if (!!pt)
                {
                    pt.stopAllActions();
                    pt.scale = 1;
                    pt.runAction(cc.fadeIn(0.05));

                    if (i >= this.lastLen)
                    {
                        pt.scale = 0;
                        pt.runAction(cc.scaleTo(0.2,1).easing(cc.easeBackOut()));
                    }

                    pt.x = pointsArray[i].x;
                    pt.y = pointsArray[i].y;
                }

                if (i < num -1) {
                    let line = this.getLine(i);

                    if (!!line)
                    {
                        line.stopAllActions();
                        line.runAction(cc.fadeIn(0.1));

                        let pt0 = pointsArray[i];
                        let pt1 = pointsArray[i+1];

                        line.x = pt0.x;
                        line.y = pt0.y;
                        
                        let d = Math.hypot(pt1.x - pt0.x, pt1.y -pt0.y);

                        let dx = pt1.x - pt0.x;
                        let dy = pt1.y - pt0.y;

                        let dx0 = Math.abs(dx);
                        let dy0 = Math.abs(dy);                        

                        let m = 180.0/3.1415926;

                        if (dx <= 0 && dy <= 0) {
                            line.angle = 360 - Math.asin( dx0 / d ) * m;
                        }
                        else if (dx <=0 && dy >=0)
                        {
                            line.angle = 360 - (Math.acos( dx0 / d ) * m + 90);
                        }
                        else if (dx >=0 && dy >=0)
                        {
                            line.angle = 360 - (Math.asin( dx0 / d ) * m + 180);
                        }
                        else
                        {
                            line.angle = 360 - (Math.asin( dy0 / d ) * m + 270);
                        }
                        
                        line.height = d;
                    }
                }
            }

            for (let i =num; i < this.pointsNode.length; i ++)
            {
                if (!!this.pointsNode[i]) {
                    
                    this.pointsNode[i].stopAllActions();
                    this.pointsNode[i].runAction(cc.fadeOut(0.1));
                    this.pointsNode[i].runAction(cc.scaleTo(0.1,0));
                }
            }

            for (let i =num-1; i < this.linesNode.length; i ++)
            {
                if (!!this.linesNode[i]) {
                    this.linesNode[i].stopAllActions();
                    this.linesNode[i].runAction(cc.fadeOut(0.05));
                }
            }

            this.lastLen = num;
        }
    }
    // update (dt) {},
});
