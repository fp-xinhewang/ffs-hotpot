var utils = require('utils');
var soundManager = require('soundManager');

cc.Class({
    extends: cc.Component,

    properties: {
        root : cc.Node,
        maskLayer : cc.Node,
        captureArea : cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        let x0 = this.node.x;
        this.node.x = -10000;

        let nodeSprite = utils.captureScreenToNode();

        nodeSprite.scaleX = this.captureArea.width / nodeSprite.width;
        nodeSprite.scaleY = - this.captureArea.height / nodeSprite.height;

        this.captureArea.addChild(nodeSprite);

        this.node.x = x0;

        if (!!this.maskLayer) {
            this.maskLayer.runAction(cc.fadeTo(0.2,128));
        }

        if (!!this.root) {
            this.root.setScale(0.5);
            this.root.runAction(cc.scaleTo(0.2,1.0).easing(cc.easeBackOut()));
        }

        soundManager.instance.playSound('panel_open');

        
        this.maskLayer.width = cc.visibleRect.width;
        this.maskLayer.height = cc.visibleRect.height;
    },

    onCloseButton : function()
    {
        soundManager.instance.playSound('button');

        this.node.destroy(true);
    },

    onShareButton : function()
    {
        soundManager.instance.playSound('button');

        this.node.destroy(true);
    }

    // update (dt) {},
});
