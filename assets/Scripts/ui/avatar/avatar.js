var utils = require('utils');
var configLoader = require('configLoader');
var soundManager = require('soundManager');
var ui = require('ui');

cc.Class({
    extends: cc.Component,

    properties: {
        plateNode : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.animationName = ['idle_1','idle_2','eating','appear','disappear'];
        this.animationPlayCount = 0;
        this.disappearSpine = null;
        this.plateNode = null;
        this.newerNpcId = 0;
        this.isFinished = false;
        this.displayNpcId = this.displayNpcId || 0;
    },

    showPlate : function()
    {
        if (!!this.plateNode) {
            let spine0 = this.plateNode.getComponent('plate');
            if (spine0) {
                spine0.show();
                spine0.playAppear();
            }
        }
    }
    ,

    playBossAnimation : function()
    {

    }
    ,

    init : function (npcId)
    {
        if (this.npcId != npcId)
        {
            if (!!this.npcNode) {

                this.npcNode.removeFromParent(true);
                this.npcNode = null;
            }

            let oldId = this.displayNpcId;

            let isBoss = false;
            let npcConfig = configLoader.instance.getNpcConfig(npcId);
            if (!!npcConfig) {
                if (npcConfig.BossIs == true) {
                    isBoss = true;
                    this.displayNpcId = npcId;
                    if (this.displayNpcId >= 11000)
                    {
                        if (this.displayNpcId > 11002) {
                            this.displayNpcId = 11002;
                        }
                    }
                }
            }

            if (isBoss == false)
            {
                this.displayNpcId = ui.instance.getNextDisplayNpcIdFromPool();
            }

            if (oldId > 10000 && oldId < 11000) {
                ui.instance.removeNpcDisplayIdFromPool(oldId);
            }
            
            this.npcId = npcId;
            this.isFinished = false;
            this.showPlate();
            //this.displayNpcId = 11002;

            let npcSpine = utils.createSpineNode('npc/' + this.displayNpcId + '/' + this.displayNpcId, 'appear', false, '',
                (node, animation) =>{
                    var spine = node.getComponent(sp.Skeleton);
                    if (!!spine) {

                        spine.setMix('idle_1', 'idle_2', 0.2);
                        spine.setMix('idle_2', 'idle_1', 0.2);
                        spine.setMix('idle_1', 'eating', 0.2);
                        spine.setMix('idle_2', 'eating', 0.2);
                        spine.timeScale = 0.5;

                        spine.setCompleteListener((trackEntry) => 
                        {
                            var animationName = trackEntry.animation ? trackEntry.animation.name : "";
                            this.animationPlayCount ++;
                            switch (animationName) {
                                case 'appear':
                                case 'hehe' :
                                {
                                    this.playIdle();
                                    break;
                                }

                                case 'disappear':
                                {
                                    this.npcId = 0;

                                    if (this.newerNpcId == 0) {
                                        this.npcNode.active = false;
                                        if (!!this.hideCallBack) {
                                            this.hideCallBack();
                                        }
                                    }
                                    else {
                                        this.init(this.newerNpcId);
                                    }

                                    return;
                                }

                                case 'happy':
                                {
                                    this.playDisappear();
                                    break;
                                }

                                case 'finish':
                                {
                                    this.playHappy();
                                    break;
                                }

                                case 'eating':
                                {
                                    if (this.animationPlayCount >= 3) {
                                        this.playIdle();
                                    }
                                }

                                /*
                                case 'idle_1':
                                case 'idle_2':
                                {
                                    this.playIdle();
                                }
                                */
                            }
                        });
                    }
                }
            );

            if (this.displayNpcId == 10006) {
                npcSpine.scale = 0.93;
            }else{
                npcSpine.scale = 1.0;
            }

            this.node.addChild(npcSpine);
            this.npcNode = npcSpine;
            this.npcNode.active = true;
        }
    }
    ,

    playIdle : function()
    {
        //let idle = utils.randInt(2);
        this.playAnimation(this.animationName[0],true);
    }
    ,

    playDisappear : function()
    {
        this.playAnimation('disappear',false);
    }
    ,

    playHappy : function()
    {
        /*
        let npcConfig = configLoader.instance.getNpcConfig(this.npcId);
        if (!!npcConfig) {
            if (npcConfig.BossIs == true) {
                this.playDisappear();
            }
            else {
                
            }
        }
        */

        this.playAnimation('happy', false);
    }
    ,

    playDisappointed : function()
    {
        if (this.isFinished) {
            return;
        }

        var skData = this.npcNode.getComponent(sp.Skeleton);
        if (!!skData)
        {
            if (skData.animation == 'eating') {
                return;    
            }
        }

        this.playAnimation('hehe', false);
    }
    ,
    
    playEatting : function()
    {
        if (this.isFinished) {
            return;
        }

        var skData = this.npcNode.getComponent(sp.Skeleton);
        if (!!skData)
        {
            if (skData.animation == 'eating') {
                this.animationPlayCount = 0;
                return;    
            }
        }

        this.playAnimation('eating');
    }
    ,

    playDisappearAndHide : function(callBack = null)
    {
        if (!!this.plateNode) {
            let spine0 = this.plateNode.getComponent('plate');
            if (spine0) {
                spine0.playDisappear();       
            }
        }

        this.newerNpcId = 0;
        this.playHappy();
        this.hideCallBack = callBack;
        this.isFinished = true;

        if (!!this.plateNode) {
            let spine0 = this.plateNode.getComponent('plate');
            if (spine0) {
                spine0.hide();
            }
        }

    }
    ,

    playOlderDisappearAndNewerAppear : function(oldNpcId, newerNpcId)
    {
        if (this.npcNode.active == false) {
            this.npcNode.active = true;
            this.init(newerNpcId);
            return;
        }

        if (!!this.plateNode) {
            let spine0 = this.plateNode.getComponent('plate');
            if (spine0) {
                spine0.playDisappear();
            }
        }

        this.newerNpcId = newerNpcId;
        this.playHappy();
        this.isFinished = true;

        if (!!this.plateNode) {
            let spine0 = this.plateNode.getComponent('plate');
            if (spine0) {
                spine0.hide();
            }
        }
    }

    ,
    playAnimation : function (anim, loop = true)
    {
        var skData = this.npcNode.getComponent(sp.Skeleton);
        
        if (!!skData && !!anim)
        {
            this.animationPlayCount = 0;
            skData.timeScale = 1;
            skData.setAnimation(0, anim, loop);
        }
    }
});
