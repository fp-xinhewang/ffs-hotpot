var utils = require('utils');

cc.Class({
    extends: cc.Component,

    properties: {
        plateNode : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        let plateNode = utils.createSpineNode('npc/panzi/panzi', 'appear',false, '',
            (node, animation)=>{
                if (!!node) {
                    let spine0 = node.getComponent(sp.Skeleton);
                    if (spine0) {
                        spine0.timeScale = 0.5;
                        spine0.setCompleteListener((trackEntry) => 
                        {
                            var animationName = trackEntry.animation ? trackEntry.animation.name : "";
                            if (animationName == 'disappear' || 
                                animationName == 'appear') {

                                this.playIdle();
                            }
                        });
                    }
                }

        });
        this.plateNode.addChild(plateNode);
        this.plateNode = plateNode;

        this.dishKindPic = this.node.getChildByName('dishKind');
        this.leftDishNode = this.node.getChildByName('dishLeftNum');
    }
    ,

    playIdle : function()
    {
        if (!!this.plateNode) {
            let spine0 = this.plateNode.getComponent(sp.Skeleton);
            if (spine0) {
                spine0.setAnimation(0, 'idle', false);
            }
        }
    }
    ,

    hide : function()
    {
        //this.node.active =false;
        this.node.runAction(cc.fadeOut(0.1));
        this.dishKindPic.active = false;
        this.leftDishNode.active = false;
    }
    ,

    show : function()
    {
        this.node.runAction(cc.fadeIn(0.1));
        //this.node.active = true;
        this.dishKindPic.active = true;
        this.leftDishNode.active = true;

        this.dishKindPic.scale = 0;
        this.dishKindPic.runAction( cc.scaleTo(0.25,0.8).easing(cc.easeBackOut()))

        this.leftDishNode.scale = 0;
        this.leftDishNode.runAction( cc.scaleTo(0.25,1.0).easing(cc.easeBackOut()))
    }
    ,

    playAppear : function()
    {
        if (!!this.plateNode) {
            let spine0 = this.plateNode.getComponent(sp.Skeleton);
            if (spine0) {
                spine0.setAnimation(0, 'appear', false);
            }
        }
    }
    ,

    playDisappear : function()
    {
        if (!!this.plateNode) {
            let spine0 = this.plateNode.getComponent(sp.Skeleton);
            if (spine0) {
                spine0.setAnimation(0, 'disappear', false);
            }
        }
    }
    ,

    // update (dt) {},
});
