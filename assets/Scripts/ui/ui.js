var utils = require('utils');
var game = require('game');

var ui = cc.Class({
    extends: cc.Component,

    properties: {
        alertPrefab : cc.Prefab,
        alertTipPrefab : cc.Prefab,
        completePrefab : cc.Prefab,
        taskPanelPrefab : cc.Prefab,
        capturePanelPrefab : cc.Prefab,
        welcomePanelPrefab : cc.Prefab,
        sharePanelPrefab : cc.Prefab,
        levelupPrefab : cc.Prefab
    },

    statics: {

        instance : null
    },

    start () {
        this.npcPoolArray = [10001,10002,10003,10004,10005,10006,10007];
        this.npcCur = {};
    },
    
    removeNpcDisplayIdFromPool : function(npcId)
    {
        this.npcCur[npcId] = null;
    }
    ,

    clearNpcDisplayPool : function()
    {
        this.npcCur = {};
    }
    ,

    getNextDisplayNpcIdFromPool : function()
    {
        let retId = 10001;

        let len = this.npcPoolArray.length;
        let npcArray = [];

        for (let i =0; i < len; i ++)
        {
            let npcId = this.npcPoolArray[i];
            if (this.npcCur[npcId] == null) {
                npcArray[npcArray.length] = npcId;
            }
        }

        let len2 = npcArray.length;
        if (len2 > 0) {
            let idx = utils.randInt(len2);
            retId = npcArray[idx];
        }

        this.npcCur[retId] = 1;

        return retId;
    }
    ,

    onLoad: function () {
        ui.instance = this;
    }
    ,

    showLevelupPanel : function()
    {
        let panel = cc.instantiate(this.levelupPrefab);       
        let scene = cc.director.getScene();

        let winWidth = cc.visibleRect.width;
        let winHeight = cc.visibleRect.height;

        panel.setPosition(winWidth * 0.5, winHeight * 0.5);
        scene.addChild(panel);
    }
    ,

    showSharePanel : function(shareTimes , callBack = null)
    {
        let panel = cc.instantiate(this.sharePanelPrefab);       
        let scene = cc.director.getScene();

        let winWidth = cc.visibleRect.width;
        let winHeight = cc.visibleRect.height;

        panel.setPosition(winWidth * 0.5, winHeight * 0.5);

        let panelScript = panel.getComponent('sharePanel');
        if (!!panelScript) {
            panelScript.setShareTimes(shareTimes);
            panelScript.setCallBack(callBack);
        }
 
        scene.addChild(panel);
    }
    ,

    showWelcomePanel : function(coinsNum ,callBack)
    {
        let panel = cc.instantiate(this.welcomePanelPrefab);
        let script = panel.getComponent('welcomePanel');

        if (!!script) {
            script.initSharePanel(coinsNum, callBack);
        }
        
        let scene = cc.director.getScene();

        let winWidth = cc.visibleRect.width;
        let winHeight = cc.visibleRect.height;

        panel.setPosition(winWidth * 0.5, winHeight * 0.5);
        scene.addChild(panel);
    }
    ,
    
    showAlert : function(str,btnStr = null,callBack = null)
    {
        let alert = cc.instantiate(this.alertPrefab);
        let winComp = alert.getComponent('alertWindow');
        if (!!winComp) {
            winComp.setString(str);

            if (btnStr) {
                winComp.setButtonString(btnStr);
            }

            if (callBack) {
                winComp.setCallBack(callBack);
            }
        }

        let scene = cc.director.getScene();

        let winWidth = cc.visibleRect.width;
        let winHeight = cc.visibleRect.height;

        alert.setPosition(winWidth * 0.5, winHeight * 0.5);
        scene.addChild(alert);
    }
    ,

    showAlertTip : function(str)
    {
        let alert = cc.instantiate(this.alertTipPrefab);
        let winComp = alert.getComponent('alertTip');
        if (!!winComp) {
            winComp.setString(str);
        }

        let scene = cc.director.getScene();

        let winWidth = cc.visibleRect.width;
        let winHeight = cc.visibleRect.height;

        alert.setPosition(winWidth * 0.5, winHeight * 0.5);
        scene.addChild(alert);
    }
    ,

    flySpriteToPosition : function(imgPath, nodeFrom, nodeTo, dur0 = 0.2, dur1 = 0.8, dur2 = 0.3)
    {
        //imgPath = 'general/coins';
        let coin_icon = utils.createSprite(imgPath);
        
        let scene = cc.director.getScene();
        scene.addChild(coin_icon);

        let icon_size0 = nodeFrom.getContentSize();
        let icon_anchor0 = nodeFrom.getAnchorPoint();
        let icon_pos0 = cc.v2(icon_size0.width * icon_anchor0.x, icon_size0.height * icon_anchor0.y);
        let src_pos = nodeFrom.convertToWorldSpace(icon_pos0);

        coin_icon.setPosition(src_pos.x, src_pos.y);

        let icon_size = nodeTo.getContentSize();
        let icon_anchor = nodeTo.getAnchorPoint();
        let icon_pos = cc.v2(icon_size.width * icon_anchor.x, icon_size.height * icon_anchor.y);
        let target_pos = nodeTo.convertToWorldSpace(icon_pos);

        //var bezier = [cc.v2(0, windowSize.height / 2), cc.v2(300, -windowSize.height / 2), cc.v2(300, 100)];
        //var bezierTo = cc.bezierTo(2, bezier);

        coin_icon.opacity = 0;
        let fadeIn = cc.fadeIn(dur0);
        let move_to = cc.moveTo(dur1, target_pos).easing(cc.easeBackOut());
        //let delay2 = cc.delayTime(dur2);
        let fadeOut = cc.fadeOut(dur2);
        let remove_self = cc.removeSelf(true);
        let seq = cc.sequence(move_to, fadeOut, remove_self, null);
        let act = cc.spawn(seq,fadeIn);

        coin_icon.runAction(act);
    }
    ,

    showTaskPanel : function()
    {
        let panel = cc.instantiate(this.taskPanelPrefab);

        let scene = cc.director.getScene();

        let winWidth = cc.visibleRect.width;
        let winHeight = cc.visibleRect.height;

        panel.setPosition(winWidth * 0.5, winHeight * 0.5);
        scene.addChild(panel);
    }
    ,

    showCapturePanel : function()
    {
        let panel = cc.instantiate(this.capturePanelPrefab);

        let scene = cc.director.getScene();

        let winWidth = cc.visibleRect.width;
        let winHeight = cc.visibleRect.height;

        panel.setPosition(winWidth * 0.5, winHeight * 0.5);
        scene.addChild(panel);
    }

    /*
    captureScreen : function()
    {
        let node = new cc.Node();
        node.parent = cc.director.getScene();
        

        let camera = node.addComponent(cc.Camera);

        // 设置你想要的截图内容的 cullingMask
        camera.cullingMask = 0xffffffff;

        // 新建一个 RenderTexture，并且设置 camera 的 targetTexture 为新建的 RenderTexture，这样 camera 的内容将会渲染到新建的 RenderTexture 中。
        let texture = new cc.RenderTexture();
        let gl = cc.game._renderContext;
        // 如果截图内容中不包含 Mask 组件，可以不用传递第三个参数
        texture.initWithSize(cc.visibleRect.width, cc.visibleRect.height, gl.STENCIL_INDEX8);
        camera.targetTexture = texture;

        // 渲染一次摄像机，即更新一次内容到 RenderTexture 中
        camera.render();

        // 这样我们就能从 RenderTexture 中获取到数据了
        let data = texture.readPixels();

        // 接下来就可以对这些数据进行操作了
        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext('2d');
        canvas.width = texture.width;
        canvas.height = texture.height;

        let rowBytes = width * 4;
        for (let row = 0; row < height; row++) {
            let srow = height - 1 - row;
            let imageData = ctx.createImageData(width, 1);
            let start = srow*width*4;
            for (let i = 0; i < rowBytes; i++) {
                imageData.data[i] = data[start+i];
            }

            ctx.putImageData(imageData, 0, row);
        }

        let dataURL = canvas.toDataURL("image/jpeg");
        let img = document.createElement("img");
        img.src = dataURL;
    }
    */

    // update (dt) {},
});
