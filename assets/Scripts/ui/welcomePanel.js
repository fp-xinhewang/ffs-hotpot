var utils = require('utils');
var wechatUIPort = require('wechatUIPort');
var ui = require('ui');
var soundManager = require('soundManager');
var configLoader = require('configLoader');
var game = require('game');

cc.Class({
    extends: cc.Component,

    properties: {
        maskLayer : cc.Node,
        root : cc.Node,
        coinsNumber : cc.Label,
        coinsShareTimes : cc.Label,
        shareButton : cc.Node,
        iconShareNode : cc.Node,
        iconVideoNode : cc.Node,
        shareText : cc.Label
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.coinsCount = this.coinsCount || 0;

        if (!!this.maskLayer) {
            this.maskLayer.runAction(cc.fadeTo(0.2,128));
        }

        if (!!this.root) {
            this.root.setScale(0.5);
            this.root.runAction(cc.scaleTo(0.2,1.0).easing(cc.easeBackOut()));
        }

        let config = configLoader.instance.getShareConfig(3);
        this.coinsShareTimes.string = config.RewardValue.toString() + '倍';
        
        this.maskLayer.width = cc.visibleRect.width;
        this.maskLayer.height = cc.visibleRect.height;

        this.coinsRewardTo = 0;
        this.coinsTimes = 1;

        this.shareType = config.ShareQueue[0];
        this.shareReward = config.RewardValue; 

        this.iconShareNode.active = (this.shareType == 1);
        this.iconVideoNode.active = (this.shareType == 2);

        if (this.shareType == 1) {
            this.shareText.string = '分享获取';
        }
        else {
            this.shareText.string = '广告获取';
        }
    },

    initSharePanel : function(num, callback)
    {
        this.coinsCount = num;
        this.coinsNumber.string = num;
        this.callback = callback;
    }
    ,

    onCloseButton : function()
    {
        if (!!this.callback) {
            this.callback('close');
        }

        this.node.stopAllActions();

        this.node.destroy(true);
        soundManager.instance.playSound('button');
    }
    ,

    delayToClose : function()
    {
        let dlyTime = cc.delayTime(3.0);
        let callBack = cc.callFunc(this.onCloseButton, this);

        let seq = cc.sequence(dlyTime, callBack);
        this.node.runAction(seq);
    }
    ,

    onTouchShareButton : function()
    {
        let self = this;

        let btnComp = this.shareButton.getComponent(cc.Button);
        if (!!btnComp) {
            btnComp.enableAutoGrayEffect = true;
            btnComp.interactable = false;
            this.shareButton.stopAllActions();
            this.shareButton.scale = 1;
        }

        let shareType = this.shareType;

        if (shareType == 1) {
            wechatUIPort.instance.shareToWechat((rsp)=>{
                
                if (rsp.success == true) {

                    self.coinsTimes = self.shareReward;
                    if (!!self.callback) {
                        self.callback('sharedSuccess');
                    }
                }

                self.delayToClose();
            });
        }
        else if (shareType == 2) {
            //播放广告
            wechatUIPort.instance.playAdVideo((rsp)=>{                      
                if (rsp.success == true) {

                    self.coinsTimes = self.shareReward;
                    if (!!self.callback) {
                        self.callback('sharedSuccess');
                    }
                }
                
                self.delayToClose();
            });
        }
        //
    }
    ,

    update (dt) {

        let coins = game.instance.getPosCoinsNumber();
        let num = coins.coins * this.coinsTimes;
        
        if (this.coinsCount != num)
        {
            let animNum = Math.floor(utils.numberLerpToNumber(this.coinsCount, num,0.04*(dt/0.01667)));

            this.coinsCount = animNum;
            this.coinsNumber.string = animNum;
        }
        
    },
});
