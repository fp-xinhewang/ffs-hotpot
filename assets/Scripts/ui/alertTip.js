cc.Class({
    extends: cc.Component,

    properties: {

        root : cc.Node,
        alertTip : cc.Label
    },

    finishMove : function()
    {
        this.node.destroy();
    }
    ,

    start () {

        if (!!this.root) {

            let pos0 = new cc.Vec2(0, -200);
            let pos1 = new cc.Vec2(0, 0);

            let fade0 = cc.fadeIn(0.2);
            let move0 = cc.moveTo(0.35, pos0).easing(cc.easeBackOut());
            let spawn0 = cc.spawn(fade0, move0);
            let delay0 = cc.delayTime(1.75);

            let fade1 = cc.fadeOut(0.2);
            let move1 = cc.moveTo(0.35, pos1).easing(cc.easeBackOut());
            let spawn1 = cc.spawn(fade1, move1);
            var callback = cc.callFunc(this.finishMove, this);

            let seq = cc.sequence(spawn0, delay0, spawn1, callback);

            this.root.y = 0;
            this.root.opacity = 0;

            this.root.runAction(seq);
        }
    },

    setString : function(str)
    {
        this.alertTip.string = str;
    }

});
