var soundManager = require('soundManager');
var game = require('game');
var configLoader = require('configLoader');
var utils = require('utils');
var soundManager = require('soundManager');

cc.Class({
    extends: cc.Component,

    properties: {
        maskLayer : cc.Node,
        root : cc.Node,
        backSpine : sp.Skeleton,
        frontSpine : sp.Skeleton,
        newObjsView : cc.Node,
        newObjsContent : cc.Node,
        levelNumber : cc.Label
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        if (!!this.maskLayer) {
            this.maskLayer.runAction(cc.fadeTo(0.2,128));
        }

        if (!!this.root) {
            this.root.setScale(0.5);
            this.root.runAction(cc.scaleTo(0.2,1.0).easing(cc.easeBackOut()));

            this.root.opacity = 0;
            this.root.runAction(cc.fadeIn(0.5));
        }

        if (!!this.backSpine) {

            this.backSpine.setAnimation(0,'open',false);

            this.backSpine.setCompleteListener((trackEntry) => 
            {
                var animationName = trackEntry.animation ? trackEntry.animation.name : "";
                switch (animationName) {
                    case 'open':
                    {
                        this.backSpine.setAnimation(0,'idle',true);
                        break;
                    }

                }
            });
        }

        if (!!this.frontSpine) {

            this.frontSpine.setAnimation(0,'open',false);

            this.frontSpine.setCompleteListener((trackEntry) => 
            {
                var animationName = trackEntry.animation ? trackEntry.animation.name : "";
                switch (animationName) {
                    case 'open':
                    {
                        this.frontSpine.setAnimation(0,'idle',true);
                        break;
                    }

                }
            });
        }

        let newObjs = this.getNewObjects();
        let objNum = newObjs.length;

        if (objNum > 0)
        {
            let w = 106;
            let x0 = 0;
            let w0 = w * objNum;
            if (w0 < this.newObjsView.width)
            {
                x0 = (this.newObjsView.width - w0) * 0.5;
            }

            for (let i =0; i < objNum; i ++)
            {
                let itemId = newObjs[i];

                let iconBack = utils.createSprite('levelup/panel_levelup1');
                
                let iconNode = utils.createSprite('icons/'+itemId);
                iconBack.addChild(iconNode);

                iconBack.x = w * (i + 0.5) + x0;

                this.newObjsContent.addChild(iconBack);
            }
        }

        this.maskLayer.width = cc.visibleRect.width;
        this.maskLayer.height = cc.visibleRect.height;

        soundManager.instance.playSound('levelup');
    },

    getNewObjects : function()
    {
        let currentLevel = game.instance.getLevel();

        this.levelNumber.string = currentLevel;

        let itemNum = configLoader.instance.StoreSpec_conf_array.length;

        let objsIds = [];
        for (let i = 0; i < itemNum; i ++)
        {
            let config = configLoader.instance.StoreSpec_conf_array[i];
            let decConfig = configLoader.instance.getDecorationConfig(config.SpecId);
            if (!!decConfig) {
                if (currentLevel == decConfig.UnlockLevel)
                {
                    objsIds[objsIds.length] = config.SpecId;
                }
            }
        }

        return objsIds;
    }
    ,


    onCloseButton : function()
    {
        soundManager.instance.playSound('button');

        this.node.destroy();
        //this.node.x = 10000;
    },
    // update (dt) {},
});
