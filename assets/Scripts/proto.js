/*eslint-disable block-scoped-var, no-redeclare, no-control-regex, no-prototype-builtins*/
"use strict";

var protobuf = require("protobuf");var $protobuf = protobuf;

var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.gamekits = (function() {

    var gamekits = {};

    gamekits.PlatformType = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "Local"] = 0;
        values[valuesById[1] = "WeChat"] = 1;
        values[valuesById[2] = "Funplus"] = 2;
        return values;
    })();

    gamekits.GenderType = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "Unknown"] = 0;
        values[valuesById[1] = "Male"] = 1;
        values[valuesById[2] = "Female"] = 2;
        return values;
    })();

    gamekits.ItemType = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "Min"] = 0;
        values[valuesById[1] = "Decoration"] = 1;
        values[valuesById[2] = "Coins"] = 2;
        return values;
    })();

    gamekits.BasePlayerData = (function() {

        function BasePlayerData(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        BasePlayerData.prototype.uid = $util.Long ? $util.Long.fromBits(0,0,true) : 0;
        BasePlayerData.prototype.platform = 0;
        BasePlayerData.prototype.nickname = "";
        BasePlayerData.prototype.level = 0;
        BasePlayerData.prototype.gender = 0;
        BasePlayerData.prototype.avatarUrl = "";

        BasePlayerData.create = function create(properties) {
            return new BasePlayerData(properties);
        };

        BasePlayerData.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.uid != null && m.hasOwnProperty("uid"))
                w.uint32(8).uint64(m.uid);
            if (m.platform != null && m.hasOwnProperty("platform"))
                w.uint32(16).int32(m.platform);
            if (m.nickname != null && m.hasOwnProperty("nickname"))
                w.uint32(26).string(m.nickname);
            if (m.level != null && m.hasOwnProperty("level"))
                w.uint32(32).int32(m.level);
            if (m.gender != null && m.hasOwnProperty("gender"))
                w.uint32(40).int32(m.gender);
            if (m.avatarUrl != null && m.hasOwnProperty("avatarUrl"))
                w.uint32(50).string(m.avatarUrl);
            return w;
        };

        BasePlayerData.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.gamekits.BasePlayerData();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.uid = r.uint64();
                    break;
                case 2:
                    m.platform = r.int32();
                    break;
                case 3:
                    m.nickname = r.string();
                    break;
                case 4:
                    m.level = r.int32();
                    break;
                case 5:
                    m.gender = r.int32();
                    break;
                case 6:
                    m.avatarUrl = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return BasePlayerData;
    })();

    gamekits.OrderData = (function() {

        function OrderData(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        OrderData.prototype.orderId = 0;
        OrderData.prototype.pieceId = 0;
        OrderData.prototype.needPieceNum = 0;
        OrderData.prototype.hasPieceNum = 0;
        OrderData.prototype.coins = 0;

        OrderData.create = function create(properties) {
            return new OrderData(properties);
        };

        OrderData.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.orderId != null && m.hasOwnProperty("orderId"))
                w.uint32(8).int32(m.orderId);
            if (m.pieceId != null && m.hasOwnProperty("pieceId"))
                w.uint32(16).int32(m.pieceId);
            if (m.needPieceNum != null && m.hasOwnProperty("needPieceNum"))
                w.uint32(24).int32(m.needPieceNum);
            if (m.hasPieceNum != null && m.hasOwnProperty("hasPieceNum"))
                w.uint32(32).int32(m.hasPieceNum);
            if (m.coins != null && m.hasOwnProperty("coins"))
                w.uint32(40).int32(m.coins);
            return w;
        };

        OrderData.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.gamekits.OrderData();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.orderId = r.int32();
                    break;
                case 2:
                    m.pieceId = r.int32();
                    break;
                case 3:
                    m.needPieceNum = r.int32();
                    break;
                case 4:
                    m.hasPieceNum = r.int32();
                    break;
                case 5:
                    m.coins = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return OrderData;
    })();

    gamekits.NpcData = (function() {

        function NpcData(p) {
            this.orders = [];
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        NpcData.prototype.npcUniqueId = 0;
        NpcData.prototype.npcId = 0;
        NpcData.prototype.isBoss = false;
        NpcData.prototype.orders = $util.emptyArray;

        NpcData.create = function create(properties) {
            return new NpcData(properties);
        };

        NpcData.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.npcUniqueId != null && m.hasOwnProperty("npcUniqueId"))
                w.uint32(8).int32(m.npcUniqueId);
            if (m.npcId != null && m.hasOwnProperty("npcId"))
                w.uint32(16).int32(m.npcId);
            if (m.isBoss != null && m.hasOwnProperty("isBoss"))
                w.uint32(24).bool(m.isBoss);
            if (m.orders != null && m.orders.length) {
                for (var i = 0; i < m.orders.length; ++i)
                    $root.gamekits.OrderData.encode(m.orders[i], w.uint32(34).fork()).ldelim();
            }
            return w;
        };

        NpcData.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.gamekits.NpcData();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.npcUniqueId = r.int32();
                    break;
                case 2:
                    m.npcId = r.int32();
                    break;
                case 3:
                    m.isBoss = r.bool();
                    break;
                case 4:
                    if (!(m.orders && m.orders.length))
                        m.orders = [];
                    m.orders.push($root.gamekits.OrderData.decode(r, r.uint32()));
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return NpcData;
    })();

    gamekits.StageRecord = (function() {

        function StageRecord(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        StageRecord.prototype.stageId = 0;
        StageRecord.prototype.coins = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        StageRecord.create = function create(properties) {
            return new StageRecord(properties);
        };

        StageRecord.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.stageId != null && m.hasOwnProperty("stageId"))
                w.uint32(8).int32(m.stageId);
            if (m.coins != null && m.hasOwnProperty("coins"))
                w.uint32(16).uint64(m.coins);
            return w;
        };

        StageRecord.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.gamekits.StageRecord();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.stageId = r.int32();
                    break;
                case 2:
                    m.coins = r.uint64();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return StageRecord;
    })();

    gamekits.ClearDetail = (function() {

        function ClearDetail(p) {
            this.pieces = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ClearDetail.prototype.pieces = $util.emptyObject;

        ClearDetail.create = function create(properties) {
            return new ClearDetail(properties);
        };

        ClearDetail.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.pieces != null && m.hasOwnProperty("pieces")) {
                for (var ks = Object.keys(m.pieces), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]).uint32(16).int32(m.pieces[ks[i]]).ldelim();
                }
            }
            return w;
        };

        ClearDetail.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.gamekits.ClearDetail(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.pieces === $util.emptyObject)
                        m.pieces = {};
                    k = r.int32();
                    r.pos++;
                    m.pieces[k] = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ClearDetail;
    })();

    gamekits.TaskData = (function() {

        function TaskData(p) {
            this.Reward = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        TaskData.prototype.TaskId = 0;
        TaskData.prototype.TaskType = "";
        TaskData.prototype.TargetValue = 0;
        TaskData.prototype.SpecId = 0;
        TaskData.prototype.Value = 0;
        TaskData.prototype.Reward = $util.emptyObject;

        TaskData.create = function create(properties) {
            return new TaskData(properties);
        };

        TaskData.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.TaskId != null && m.hasOwnProperty("TaskId"))
                w.uint32(8).int32(m.TaskId);
            if (m.TaskType != null && m.hasOwnProperty("TaskType"))
                w.uint32(18).string(m.TaskType);
            if (m.TargetValue != null && m.hasOwnProperty("TargetValue"))
                w.uint32(24).int32(m.TargetValue);
            if (m.SpecId != null && m.hasOwnProperty("SpecId"))
                w.uint32(32).int32(m.SpecId);
            if (m.Value != null && m.hasOwnProperty("Value"))
                w.uint32(40).int32(m.Value);
            if (m.Reward != null && m.hasOwnProperty("Reward")) {
                for (var ks = Object.keys(m.Reward), i = 0; i < ks.length; ++i) {
                    w.uint32(50).fork().uint32(8).int32(ks[i]).uint32(16).int32(m.Reward[ks[i]]).ldelim();
                }
            }
            return w;
        };

        TaskData.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.gamekits.TaskData(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.TaskId = r.int32();
                    break;
                case 2:
                    m.TaskType = r.string();
                    break;
                case 3:
                    m.TargetValue = r.int32();
                    break;
                case 4:
                    m.SpecId = r.int32();
                    break;
                case 5:
                    m.Value = r.int32();
                    break;
                case 6:
                    r.skip().pos++;
                    if (m.Reward === $util.emptyObject)
                        m.Reward = {};
                    k = r.int32();
                    r.pos++;
                    m.Reward[k] = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return TaskData;
    })();

    gamekits.ClientStorageStateVal = (function() {

        function ClientStorageStateVal(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ClientStorageStateVal.prototype.Data = "";
        ClientStorageStateVal.prototype.Timeout = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
        ClientStorageStateVal.prototype.TimeUpdated = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        ClientStorageStateVal.create = function create(properties) {
            return new ClientStorageStateVal(properties);
        };

        ClientStorageStateVal.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.Data != null && m.hasOwnProperty("Data"))
                w.uint32(10).string(m.Data);
            if (m.Timeout != null && m.hasOwnProperty("Timeout"))
                w.uint32(16).int64(m.Timeout);
            if (m.TimeUpdated != null && m.hasOwnProperty("TimeUpdated"))
                w.uint32(24).int64(m.TimeUpdated);
            return w;
        };

        ClientStorageStateVal.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.gamekits.ClientStorageStateVal();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.Data = r.string();
                    break;
                case 2:
                    m.Timeout = r.int64();
                    break;
                case 3:
                    m.TimeUpdated = r.int64();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ClientStorageStateVal;
    })();

    gamekits.ClientStorageState = (function() {

        function ClientStorageState(p) {
            this.Map = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ClientStorageState.prototype.Map = $util.emptyObject;

        ClientStorageState.create = function create(properties) {
            return new ClientStorageState(properties);
        };

        ClientStorageState.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.Map != null && m.hasOwnProperty("Map")) {
                for (var ks = Object.keys(m.Map), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(10).string(ks[i]);
                    $root.gamekits.ClientStorageStateVal.encode(m.Map[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        ClientStorageState.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.gamekits.ClientStorageState(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.Map === $util.emptyObject)
                        m.Map = {};
                    k = r.string();
                    r.pos++;
                    m.Map[k] = $root.gamekits.ClientStorageStateVal.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ClientStorageState;
    })();

    gamekits.Message = (function() {

        function Message(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        Message.prototype.MsgId = $util.Long ? $util.Long.fromBits(0,0,true) : 0;
        Message.prototype.Title = "";
        Message.prototype.Content = "";
        Message.prototype.Type = 0;
        Message.prototype.ParamsType = 0;
        Message.prototype.ParamsJson = "";
        Message.prototype.RewardJson = "";
        Message.prototype.IsRead = 0;
        Message.prototype.StartTime = $util.Long ? $util.Long.fromBits(0,0,true) : 0;
        Message.prototype.EndTime = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        Message.create = function create(properties) {
            return new Message(properties);
        };

        Message.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.MsgId != null && m.hasOwnProperty("MsgId"))
                w.uint32(8).uint64(m.MsgId);
            if (m.Title != null && m.hasOwnProperty("Title"))
                w.uint32(18).string(m.Title);
            if (m.Content != null && m.hasOwnProperty("Content"))
                w.uint32(26).string(m.Content);
            if (m.Type != null && m.hasOwnProperty("Type"))
                w.uint32(32).uint32(m.Type);
            if (m.ParamsType != null && m.hasOwnProperty("ParamsType"))
                w.uint32(40).uint32(m.ParamsType);
            if (m.ParamsJson != null && m.hasOwnProperty("ParamsJson"))
                w.uint32(50).string(m.ParamsJson);
            if (m.RewardJson != null && m.hasOwnProperty("RewardJson"))
                w.uint32(58).string(m.RewardJson);
            if (m.IsRead != null && m.hasOwnProperty("IsRead"))
                w.uint32(64).int32(m.IsRead);
            if (m.StartTime != null && m.hasOwnProperty("StartTime"))
                w.uint32(72).uint64(m.StartTime);
            if (m.EndTime != null && m.hasOwnProperty("EndTime"))
                w.uint32(80).uint64(m.EndTime);
            return w;
        };

        Message.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.gamekits.Message();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.MsgId = r.uint64();
                    break;
                case 2:
                    m.Title = r.string();
                    break;
                case 3:
                    m.Content = r.string();
                    break;
                case 4:
                    m.Type = r.uint32();
                    break;
                case 5:
                    m.ParamsType = r.uint32();
                    break;
                case 6:
                    m.ParamsJson = r.string();
                    break;
                case 7:
                    m.RewardJson = r.string();
                    break;
                case 8:
                    m.IsRead = r.int32();
                    break;
                case 9:
                    m.StartTime = r.uint64();
                    break;
                case 10:
                    m.EndTime = r.uint64();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return Message;
    })();

    gamekits.ShareType = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "MinShareType"] = 0;
        values[valuesById[1] = "AddSoup"] = 1;
        values[valuesById[2] = "StageCoins"] = 2;
        values[valuesById[3] = "CollectCoins"] = 3;
        return values;
    })();

    gamekits.ShareWay = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "None"] = 0;
        values[valuesById[1] = "Share"] = 1;
        values[valuesById[2] = "Adv"] = 2;
        return values;
    })();

    gamekits.Conf = (function() {

        function Conf(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        Conf.prototype.ConfigSpecConf = null;
        Conf.prototype.DecorationSpecConf = null;
        Conf.prototype.GuideSpecConf = null;
        Conf.prototype.ItemSpecConf = null;
        Conf.prototype.LevelSpecConf = null;
        Conf.prototype.MessageSpecConf = null;
        Conf.prototype.NpcSpecConf = null;
        Conf.prototype.OrderSpecConf = null;
        Conf.prototype.PieceSpecConf = null;
        Conf.prototype.ShareSpecConf = null;
        Conf.prototype.StageSpecConf = null;
        Conf.prototype.StoreSpecConf = null;
        Conf.prototype.TaskSpecConf = null;

        Conf.create = function create(properties) {
            return new Conf(properties);
        };

        Conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.ConfigSpecConf != null && m.hasOwnProperty("ConfigSpecConf"))
                $root.rawdata.ConfigSpec_conf.encode(m.ConfigSpecConf, w.uint32(10).fork()).ldelim();
            if (m.DecorationSpecConf != null && m.hasOwnProperty("DecorationSpecConf"))
                $root.rawdata.DecorationSpec_conf.encode(m.DecorationSpecConf, w.uint32(18).fork()).ldelim();
            if (m.GuideSpecConf != null && m.hasOwnProperty("GuideSpecConf"))
                $root.rawdata.GuideSpec_conf.encode(m.GuideSpecConf, w.uint32(26).fork()).ldelim();
            if (m.ItemSpecConf != null && m.hasOwnProperty("ItemSpecConf"))
                $root.rawdata.ItemSpec_conf.encode(m.ItemSpecConf, w.uint32(34).fork()).ldelim();
            if (m.LevelSpecConf != null && m.hasOwnProperty("LevelSpecConf"))
                $root.rawdata.LevelSpec_conf.encode(m.LevelSpecConf, w.uint32(42).fork()).ldelim();
            if (m.MessageSpecConf != null && m.hasOwnProperty("MessageSpecConf"))
                $root.rawdata.MessageSpec_conf.encode(m.MessageSpecConf, w.uint32(50).fork()).ldelim();
            if (m.NpcSpecConf != null && m.hasOwnProperty("NpcSpecConf"))
                $root.rawdata.NpcSpec_conf.encode(m.NpcSpecConf, w.uint32(58).fork()).ldelim();
            if (m.OrderSpecConf != null && m.hasOwnProperty("OrderSpecConf"))
                $root.rawdata.OrderSpec_conf.encode(m.OrderSpecConf, w.uint32(66).fork()).ldelim();
            if (m.PieceSpecConf != null && m.hasOwnProperty("PieceSpecConf"))
                $root.rawdata.PieceSpec_conf.encode(m.PieceSpecConf, w.uint32(74).fork()).ldelim();
            if (m.ShareSpecConf != null && m.hasOwnProperty("ShareSpecConf"))
                $root.rawdata.ShareSpec_conf.encode(m.ShareSpecConf, w.uint32(82).fork()).ldelim();
            if (m.StageSpecConf != null && m.hasOwnProperty("StageSpecConf"))
                $root.rawdata.StageSpec_conf.encode(m.StageSpecConf, w.uint32(90).fork()).ldelim();
            if (m.StoreSpecConf != null && m.hasOwnProperty("StoreSpecConf"))
                $root.rawdata.StoreSpec_conf.encode(m.StoreSpecConf, w.uint32(98).fork()).ldelim();
            if (m.TaskSpecConf != null && m.hasOwnProperty("TaskSpecConf"))
                $root.rawdata.TaskSpec_conf.encode(m.TaskSpecConf, w.uint32(106).fork()).ldelim();
            return w;
        };

        Conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.gamekits.Conf();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.ConfigSpecConf = $root.rawdata.ConfigSpec_conf.decode(r, r.uint32());
                    break;
                case 2:
                    m.DecorationSpecConf = $root.rawdata.DecorationSpec_conf.decode(r, r.uint32());
                    break;
                case 3:
                    m.GuideSpecConf = $root.rawdata.GuideSpec_conf.decode(r, r.uint32());
                    break;
                case 4:
                    m.ItemSpecConf = $root.rawdata.ItemSpec_conf.decode(r, r.uint32());
                    break;
                case 5:
                    m.LevelSpecConf = $root.rawdata.LevelSpec_conf.decode(r, r.uint32());
                    break;
                case 6:
                    m.MessageSpecConf = $root.rawdata.MessageSpec_conf.decode(r, r.uint32());
                    break;
                case 7:
                    m.NpcSpecConf = $root.rawdata.NpcSpec_conf.decode(r, r.uint32());
                    break;
                case 8:
                    m.OrderSpecConf = $root.rawdata.OrderSpec_conf.decode(r, r.uint32());
                    break;
                case 9:
                    m.PieceSpecConf = $root.rawdata.PieceSpec_conf.decode(r, r.uint32());
                    break;
                case 10:
                    m.ShareSpecConf = $root.rawdata.ShareSpec_conf.decode(r, r.uint32());
                    break;
                case 11:
                    m.StageSpecConf = $root.rawdata.StageSpec_conf.decode(r, r.uint32());
                    break;
                case 12:
                    m.StoreSpecConf = $root.rawdata.StoreSpec_conf.decode(r, r.uint32());
                    break;
                case 13:
                    m.TaskSpecConf = $root.rawdata.TaskSpec_conf.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return Conf;
    })();

    return gamekits;
})();

$root.netutils = (function() {

    var netutils = {};

    netutils.RawAny = (function() {

        function RawAny(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        RawAny.prototype.uri = "";
        RawAny.prototype.raw = $util.newBuffer([]);

        RawAny.create = function create(properties) {
            return new RawAny(properties);
        };

        RawAny.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.uri != null && m.hasOwnProperty("uri"))
                w.uint32(10).string(m.uri);
            if (m.raw != null && m.hasOwnProperty("raw"))
                w.uint32(18).bytes(m.raw);
            return w;
        };

        RawAny.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.netutils.RawAny();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.uri = r.string();
                    break;
                case 2:
                    m.raw = r.bytes();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return RawAny;
    })();

    netutils.Request = (function() {

        function Request(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        Request.prototype.reqNum = $util.Long ? $util.Long.fromBits(0,0,true) : 0;
        Request.prototype.time = $util.Long ? $util.Long.fromBits(0,0,true) : 0;
        Request.prototype.raw = null;

        var $oneOfFields;

        Object.defineProperty(Request.prototype, "packet", {
            get: $util.oneOfGetter($oneOfFields = ["raw"]),
            set: $util.oneOfSetter($oneOfFields)
        });

        Request.create = function create(properties) {
            return new Request(properties);
        };

        Request.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.reqNum != null && m.hasOwnProperty("reqNum"))
                w.uint32(8).uint64(m.reqNum);
            if (m.time != null && m.hasOwnProperty("time"))
                w.uint32(16).uint64(m.time);
            if (m.raw != null && m.hasOwnProperty("raw"))
                $root.netutils.RawAny.encode(m.raw, w.uint32(26).fork()).ldelim();
            return w;
        };

        Request.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.netutils.Request();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.reqNum = r.uint64();
                    break;
                case 2:
                    m.time = r.uint64();
                    break;
                case 3:
                    m.raw = $root.netutils.RawAny.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return Request;
    })();

    netutils.Response = (function() {

        function Response(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        Response.prototype.ver = 0;
        Response.prototype.raw = null;

        var $oneOfFields;

        Object.defineProperty(Response.prototype, "packet", {
            get: $util.oneOfGetter($oneOfFields = ["raw"]),
            set: $util.oneOfSetter($oneOfFields)
        });

        Response.create = function create(properties) {
            return new Response(properties);
        };

        Response.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.ver != null && m.hasOwnProperty("ver"))
                w.uint32(8).int32(m.ver);
            if (m.raw != null && m.hasOwnProperty("raw"))
                $root.netutils.RawAny.encode(m.raw, w.uint32(18).fork()).ldelim();
            return w;
        };

        Response.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.netutils.Response();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.ver = r.int32();
                    break;
                case 2:
                    m.raw = $root.netutils.RawAny.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return Response;
    })();

    netutils.ErrorResponse = (function() {

        function ErrorResponse(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ErrorResponse.prototype.code = 0;
        ErrorResponse.prototype.message = "";

        ErrorResponse.create = function create(properties) {
            return new ErrorResponse(properties);
        };

        ErrorResponse.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.code != null && m.hasOwnProperty("code"))
                w.uint32(8).int32(m.code);
            if (m.message != null && m.hasOwnProperty("message"))
                w.uint32(18).string(m.message);
            return w;
        };

        ErrorResponse.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.netutils.ErrorResponse();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.code = r.int32();
                    break;
                case 2:
                    m.message = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ErrorResponse;
    })();

    netutils.ErrorCode = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "OK"] = 0;
        values[valuesById[1] = "UNKNOWN"] = 1;
        values[valuesById[2] = "INVALID_PACKET"] = 2;
        values[valuesById[3] = "INVALID_SIGN"] = 3;
        values[valuesById[4] = "INVALID_PARAMS"] = 4;
        values[valuesById[5] = "INVALID_PLATFORM"] = 5;
        values[valuesById[6] = "DUPLICATE_REQUEST"] = 6;
        values[valuesById[100] = "TIMEOUT"] = 100;
        values[valuesById[1000] = "PLAYER_NOT_FOUND"] = 1000;
        values[valuesById[1001] = "PLAYER_LEVEL_NOT_ENOUGH"] = 1001;
        values[valuesById[1002] = "PLAYER_COINS_NOT_ENOUGH"] = 1002;
        values[valuesById[1003] = "PLAYER_COLLECT_TOO_MANY_COINS"] = 1003;
        values[valuesById[2000] = "STAGE_NOT_FOUND"] = 2000;
        values[valuesById[2001] = "STAGE_HAS_ENDED"] = 2001;
        values[valuesById[2002] = "STAGE_SHARE_NOT_ENOUGH"] = 2002;
        values[valuesById[2100] = "NPC_NOT_FOUND"] = 2100;
        values[valuesById[2101] = "NPC_HAS_NOT_MOVE"] = 2101;
        values[valuesById[2200] = "ORDER_NOT_FOUND"] = 2200;
        values[valuesById[2201] = "ORDER_HAS_COMPLETE"] = 2201;
        values[valuesById[2202] = "ORDER_HAS_NOT_COMPLETE"] = 2202;
        values[valuesById[2300] = "SOUP_PACK_NOT_ENOUGH"] = 2300;
        values[valuesById[2301] = "SOUP_NOT_ENOUGH"] = 2301;
        values[valuesById[3000] = "STORE_NOT_FOUND"] = 3000;
        values[valuesById[3001] = "STORE_HAS_BOUGHT"] = 3001;
        values[valuesById[4001] = "TASK_NOT_FOUND"] = 4001;
        values[valuesById[5001] = "CLIENT_STORAGE_KEY_LIMIT"] = 5001;
        values[valuesById[5002] = "CLIENT_STORAGE_VALUE_LIMIT"] = 5002;
        values[valuesById[5003] = "CLIENT_STORAGE_COUNT_LIMIT"] = 5003;
        return values;
    })();

    return netutils;
})();

$root.rawdata = (function() {

    var rawdata = {};

    rawdata.ConfigSpec = (function() {

        function ConfigSpec(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ConfigSpec.prototype.InitCoins = 0;
        ConfigSpec.prototype.BossDistance = 0;
        ConfigSpec.prototype.LowParameter = 0;
        ConfigSpec.prototype.UpParameter = 0;
        ConfigSpec.prototype.BombSwitch = false;
        ConfigSpec.prototype.ComobSwitch = false;
        ConfigSpec.prototype.MatchDistance = 0;
        ConfigSpec.prototype.DailyStorysNum = 0;
        ConfigSpec.prototype.AutoTip = 0;
        ConfigSpec.prototype.AutoMatchNum = 0;
        ConfigSpec.prototype.MinMatchNum = 0;
        ConfigSpec.prototype.DecayNum = 0;
        ConfigSpec.prototype.AdId = "";

        ConfigSpec.create = function create(properties) {
            return new ConfigSpec(properties);
        };

        ConfigSpec.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.InitCoins != null && m.hasOwnProperty("InitCoins"))
                w.uint32(8).int32(m.InitCoins);
            if (m.BossDistance != null && m.hasOwnProperty("BossDistance"))
                w.uint32(16).int32(m.BossDistance);
            if (m.LowParameter != null && m.hasOwnProperty("LowParameter"))
                w.uint32(24).int32(m.LowParameter);
            if (m.UpParameter != null && m.hasOwnProperty("UpParameter"))
                w.uint32(32).int32(m.UpParameter);
            if (m.BombSwitch != null && m.hasOwnProperty("BombSwitch"))
                w.uint32(40).bool(m.BombSwitch);
            if (m.ComobSwitch != null && m.hasOwnProperty("ComobSwitch"))
                w.uint32(48).bool(m.ComobSwitch);
            if (m.MatchDistance != null && m.hasOwnProperty("MatchDistance"))
                w.uint32(56).int32(m.MatchDistance);
            if (m.DailyStorysNum != null && m.hasOwnProperty("DailyStorysNum"))
                w.uint32(64).int32(m.DailyStorysNum);
            if (m.AutoTip != null && m.hasOwnProperty("AutoTip"))
                w.uint32(72).int32(m.AutoTip);
            if (m.AutoMatchNum != null && m.hasOwnProperty("AutoMatchNum"))
                w.uint32(80).int32(m.AutoMatchNum);
            if (m.MinMatchNum != null && m.hasOwnProperty("MinMatchNum"))
                w.uint32(88).int32(m.MinMatchNum);
            if (m.DecayNum != null && m.hasOwnProperty("DecayNum"))
                w.uint32(96).int32(m.DecayNum);
            if (m.AdId != null && m.hasOwnProperty("AdId"))
                w.uint32(106).string(m.AdId);
            return w;
        };

        ConfigSpec.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.ConfigSpec();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.InitCoins = r.int32();
                    break;
                case 2:
                    m.BossDistance = r.int32();
                    break;
                case 3:
                    m.LowParameter = r.int32();
                    break;
                case 4:
                    m.UpParameter = r.int32();
                    break;
                case 5:
                    m.BombSwitch = r.bool();
                    break;
                case 6:
                    m.ComobSwitch = r.bool();
                    break;
                case 7:
                    m.MatchDistance = r.int32();
                    break;
                case 8:
                    m.DailyStorysNum = r.int32();
                    break;
                case 9:
                    m.AutoTip = r.int32();
                    break;
                case 10:
                    m.AutoMatchNum = r.int32();
                    break;
                case 11:
                    m.MinMatchNum = r.int32();
                    break;
                case 12:
                    m.DecayNum = r.int32();
                    break;
                case 13:
                    m.AdId = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ConfigSpec;
    })();

    rawdata.ConfigSpec_conf = (function() {

        function ConfigSpec_conf(p) {
            this.ConfigSpecs = [];
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ConfigSpec_conf.prototype.ConfigSpecs = $util.emptyArray;

        ConfigSpec_conf.create = function create(properties) {
            return new ConfigSpec_conf(properties);
        };

        ConfigSpec_conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.ConfigSpecs != null && m.ConfigSpecs.length) {
                for (var i = 0; i < m.ConfigSpecs.length; ++i)
                    $root.rawdata.ConfigSpec.encode(m.ConfigSpecs[i], w.uint32(10).fork()).ldelim();
            }
            return w;
        };

        ConfigSpec_conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.ConfigSpec_conf();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    if (!(m.ConfigSpecs && m.ConfigSpecs.length))
                        m.ConfigSpecs = [];
                    m.ConfigSpecs.push($root.rawdata.ConfigSpec.decode(r, r.uint32()));
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ConfigSpec_conf;
    })();

    rawdata.DecorationSpec = (function() {

        function DecorationSpec(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        DecorationSpec.prototype.SpecId = 0;
        DecorationSpec.prototype.Exp = 0;
        DecorationSpec.prototype.UnlockLevel = 0;

        DecorationSpec.create = function create(properties) {
            return new DecorationSpec(properties);
        };

        DecorationSpec.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.SpecId != null && m.hasOwnProperty("SpecId"))
                w.uint32(8).int32(m.SpecId);
            if (m.Exp != null && m.hasOwnProperty("Exp"))
                w.uint32(16).int32(m.Exp);
            if (m.UnlockLevel != null && m.hasOwnProperty("UnlockLevel"))
                w.uint32(24).int32(m.UnlockLevel);
            return w;
        };

        DecorationSpec.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.DecorationSpec();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.SpecId = r.int32();
                    break;
                case 2:
                    m.Exp = r.int32();
                    break;
                case 3:
                    m.UnlockLevel = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return DecorationSpec;
    })();

    rawdata.DecorationSpec_conf = (function() {

        function DecorationSpec_conf(p) {
            this.DecorationSpecs = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        DecorationSpec_conf.prototype.DecorationSpecs = $util.emptyObject;

        DecorationSpec_conf.create = function create(properties) {
            return new DecorationSpec_conf(properties);
        };

        DecorationSpec_conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.DecorationSpecs != null && m.hasOwnProperty("DecorationSpecs")) {
                for (var ks = Object.keys(m.DecorationSpecs), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]);
                    $root.rawdata.DecorationSpec.encode(m.DecorationSpecs[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        DecorationSpec_conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.DecorationSpec_conf(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.DecorationSpecs === $util.emptyObject)
                        m.DecorationSpecs = {};
                    k = r.int32();
                    r.pos++;
                    m.DecorationSpecs[k] = $root.rawdata.DecorationSpec.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return DecorationSpec_conf;
    })();

    rawdata.GuideSpec = (function() {

        function GuideSpec(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        GuideSpec.prototype.Guide = 0;

        GuideSpec.create = function create(properties) {
            return new GuideSpec(properties);
        };

        GuideSpec.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.Guide != null && m.hasOwnProperty("Guide"))
                w.uint32(8).int32(m.Guide);
            return w;
        };

        GuideSpec.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.GuideSpec();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.Guide = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return GuideSpec;
    })();

    rawdata.GuideSpec_conf = (function() {

        function GuideSpec_conf(p) {
            this.GuideSpecs = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        GuideSpec_conf.prototype.GuideSpecs = $util.emptyObject;

        GuideSpec_conf.create = function create(properties) {
            return new GuideSpec_conf(properties);
        };

        GuideSpec_conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.GuideSpecs != null && m.hasOwnProperty("GuideSpecs")) {
                for (var ks = Object.keys(m.GuideSpecs), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]);
                    $root.rawdata.GuideSpec.encode(m.GuideSpecs[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        GuideSpec_conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.GuideSpec_conf(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.GuideSpecs === $util.emptyObject)
                        m.GuideSpecs = {};
                    k = r.int32();
                    r.pos++;
                    m.GuideSpecs[k] = $root.rawdata.GuideSpec.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return GuideSpec_conf;
    })();

    rawdata.ItemSpec = (function() {

        function ItemSpec(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ItemSpec.prototype.SpecId = 0;
        ItemSpec.prototype.Name = "";
        ItemSpec.prototype.Desc = "";
        ItemSpec.prototype.Type = 0;
        ItemSpec.prototype.TypeId = 0;

        ItemSpec.create = function create(properties) {
            return new ItemSpec(properties);
        };

        ItemSpec.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.SpecId != null && m.hasOwnProperty("SpecId"))
                w.uint32(8).int32(m.SpecId);
            if (m.Name != null && m.hasOwnProperty("Name"))
                w.uint32(18).string(m.Name);
            if (m.Desc != null && m.hasOwnProperty("Desc"))
                w.uint32(26).string(m.Desc);
            if (m.Type != null && m.hasOwnProperty("Type"))
                w.uint32(32).int32(m.Type);
            if (m.TypeId != null && m.hasOwnProperty("TypeId"))
                w.uint32(40).int32(m.TypeId);
            return w;
        };

        ItemSpec.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.ItemSpec();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.SpecId = r.int32();
                    break;
                case 2:
                    m.Name = r.string();
                    break;
                case 3:
                    m.Desc = r.string();
                    break;
                case 4:
                    m.Type = r.int32();
                    break;
                case 5:
                    m.TypeId = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ItemSpec;
    })();

    rawdata.ItemSpec_conf = (function() {

        function ItemSpec_conf(p) {
            this.ItemSpecs = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ItemSpec_conf.prototype.ItemSpecs = $util.emptyObject;

        ItemSpec_conf.create = function create(properties) {
            return new ItemSpec_conf(properties);
        };

        ItemSpec_conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.ItemSpecs != null && m.hasOwnProperty("ItemSpecs")) {
                for (var ks = Object.keys(m.ItemSpecs), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]);
                    $root.rawdata.ItemSpec.encode(m.ItemSpecs[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        ItemSpec_conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.ItemSpec_conf(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.ItemSpecs === $util.emptyObject)
                        m.ItemSpecs = {};
                    k = r.int32();
                    r.pos++;
                    m.ItemSpecs[k] = $root.rawdata.ItemSpec.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ItemSpec_conf;
    })();

    rawdata.LevelSpec = (function() {

        function LevelSpec(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        LevelSpec.prototype.Level = 0;
        LevelSpec.prototype.Exp = 0;
        LevelSpec.prototype.collectAuto = 0;
        LevelSpec.prototype.collectLimit = 0;

        LevelSpec.create = function create(properties) {
            return new LevelSpec(properties);
        };

        LevelSpec.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.Level != null && m.hasOwnProperty("Level"))
                w.uint32(8).int32(m.Level);
            if (m.Exp != null && m.hasOwnProperty("Exp"))
                w.uint32(16).int32(m.Exp);
            if (m.collectAuto != null && m.hasOwnProperty("collectAuto"))
                w.uint32(24).int32(m.collectAuto);
            if (m.collectLimit != null && m.hasOwnProperty("collectLimit"))
                w.uint32(32).int32(m.collectLimit);
            return w;
        };

        LevelSpec.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.LevelSpec();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.Level = r.int32();
                    break;
                case 2:
                    m.Exp = r.int32();
                    break;
                case 3:
                    m.collectAuto = r.int32();
                    break;
                case 4:
                    m.collectLimit = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return LevelSpec;
    })();

    rawdata.LevelSpec_conf = (function() {

        function LevelSpec_conf(p) {
            this.LevelSpecs = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        LevelSpec_conf.prototype.LevelSpecs = $util.emptyObject;

        LevelSpec_conf.create = function create(properties) {
            return new LevelSpec_conf(properties);
        };

        LevelSpec_conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.LevelSpecs != null && m.hasOwnProperty("LevelSpecs")) {
                for (var ks = Object.keys(m.LevelSpecs), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]);
                    $root.rawdata.LevelSpec.encode(m.LevelSpecs[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        LevelSpec_conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.LevelSpec_conf(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.LevelSpecs === $util.emptyObject)
                        m.LevelSpecs = {};
                    k = r.int32();
                    r.pos++;
                    m.LevelSpecs[k] = $root.rawdata.LevelSpec.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return LevelSpec_conf;
    })();

    rawdata.MessageSpec = (function() {

        function MessageSpec(p) {
            this.Reward = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        MessageSpec.prototype.SpecId = 0;
        MessageSpec.prototype.Title = "";
        MessageSpec.prototype.Content = "";
        MessageSpec.prototype.Reward = $util.emptyObject;
        MessageSpec.prototype.TypeId = 0;
        MessageSpec.prototype.StartTime = "";
        MessageSpec.prototype.EndTime = "";

        MessageSpec.create = function create(properties) {
            return new MessageSpec(properties);
        };

        MessageSpec.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.SpecId != null && m.hasOwnProperty("SpecId"))
                w.uint32(8).int32(m.SpecId);
            if (m.Title != null && m.hasOwnProperty("Title"))
                w.uint32(18).string(m.Title);
            if (m.Content != null && m.hasOwnProperty("Content"))
                w.uint32(26).string(m.Content);
            if (m.Reward != null && m.hasOwnProperty("Reward")) {
                for (var ks = Object.keys(m.Reward), i = 0; i < ks.length; ++i) {
                    w.uint32(34).fork().uint32(8).int32(ks[i]).uint32(16).int32(m.Reward[ks[i]]).ldelim();
                }
            }
            if (m.TypeId != null && m.hasOwnProperty("TypeId"))
                w.uint32(40).int32(m.TypeId);
            if (m.StartTime != null && m.hasOwnProperty("StartTime"))
                w.uint32(50).string(m.StartTime);
            if (m.EndTime != null && m.hasOwnProperty("EndTime"))
                w.uint32(58).string(m.EndTime);
            return w;
        };

        MessageSpec.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.MessageSpec(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.SpecId = r.int32();
                    break;
                case 2:
                    m.Title = r.string();
                    break;
                case 3:
                    m.Content = r.string();
                    break;
                case 4:
                    r.skip().pos++;
                    if (m.Reward === $util.emptyObject)
                        m.Reward = {};
                    k = r.int32();
                    r.pos++;
                    m.Reward[k] = r.int32();
                    break;
                case 5:
                    m.TypeId = r.int32();
                    break;
                case 6:
                    m.StartTime = r.string();
                    break;
                case 7:
                    m.EndTime = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return MessageSpec;
    })();

    rawdata.MessageSpec_conf = (function() {

        function MessageSpec_conf(p) {
            this.MessageSpecs = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        MessageSpec_conf.prototype.MessageSpecs = $util.emptyObject;

        MessageSpec_conf.create = function create(properties) {
            return new MessageSpec_conf(properties);
        };

        MessageSpec_conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.MessageSpecs != null && m.hasOwnProperty("MessageSpecs")) {
                for (var ks = Object.keys(m.MessageSpecs), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]);
                    $root.rawdata.MessageSpec.encode(m.MessageSpecs[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        MessageSpec_conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.MessageSpec_conf(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.MessageSpecs === $util.emptyObject)
                        m.MessageSpecs = {};
                    k = r.int32();
                    r.pos++;
                    m.MessageSpecs[k] = $root.rawdata.MessageSpec.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return MessageSpec_conf;
    })();

    rawdata.NpcSpec = (function() {

        function NpcSpec(p) {
            this.OrderId = [];
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        NpcSpec.prototype.SpecId = 0;
        NpcSpec.prototype.OrderId = $util.emptyArray;
        NpcSpec.prototype.SoupId = 0;
        NpcSpec.prototype.BossIs = false;

        NpcSpec.create = function create(properties) {
            return new NpcSpec(properties);
        };

        NpcSpec.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.SpecId != null && m.hasOwnProperty("SpecId"))
                w.uint32(8).int32(m.SpecId);
            if (m.OrderId != null && m.OrderId.length) {
                w.uint32(18).fork();
                for (var i = 0; i < m.OrderId.length; ++i)
                    w.int32(m.OrderId[i]);
                w.ldelim();
            }
            if (m.SoupId != null && m.hasOwnProperty("SoupId"))
                w.uint32(24).int32(m.SoupId);
            if (m.BossIs != null && m.hasOwnProperty("BossIs"))
                w.uint32(32).bool(m.BossIs);
            return w;
        };

        NpcSpec.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.NpcSpec();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.SpecId = r.int32();
                    break;
                case 2:
                    if (!(m.OrderId && m.OrderId.length))
                        m.OrderId = [];
                    if ((t & 7) === 2) {
                        var c2 = r.uint32() + r.pos;
                        while (r.pos < c2)
                            m.OrderId.push(r.int32());
                    } else
                        m.OrderId.push(r.int32());
                    break;
                case 3:
                    m.SoupId = r.int32();
                    break;
                case 4:
                    m.BossIs = r.bool();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return NpcSpec;
    })();

    rawdata.NpcSpec_conf = (function() {

        function NpcSpec_conf(p) {
            this.NpcSpecs = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        NpcSpec_conf.prototype.NpcSpecs = $util.emptyObject;

        NpcSpec_conf.create = function create(properties) {
            return new NpcSpec_conf(properties);
        };

        NpcSpec_conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.NpcSpecs != null && m.hasOwnProperty("NpcSpecs")) {
                for (var ks = Object.keys(m.NpcSpecs), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]);
                    $root.rawdata.NpcSpec.encode(m.NpcSpecs[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        NpcSpec_conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.NpcSpec_conf(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.NpcSpecs === $util.emptyObject)
                        m.NpcSpecs = {};
                    k = r.int32();
                    r.pos++;
                    m.NpcSpecs[k] = $root.rawdata.NpcSpec.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return NpcSpec_conf;
    })();

    rawdata.OrderSpec = (function() {

        function OrderSpec(p) {
            this.PiecePool = [];
            this.NeedRange = [];
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        OrderSpec.prototype.SpecId = 0;
        OrderSpec.prototype.PiecePool = $util.emptyArray;
        OrderSpec.prototype.NeedRange = $util.emptyArray;
        OrderSpec.prototype.BaseCoins = 0;

        OrderSpec.create = function create(properties) {
            return new OrderSpec(properties);
        };

        OrderSpec.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.SpecId != null && m.hasOwnProperty("SpecId"))
                w.uint32(8).int32(m.SpecId);
            if (m.PiecePool != null && m.PiecePool.length) {
                w.uint32(18).fork();
                for (var i = 0; i < m.PiecePool.length; ++i)
                    w.int32(m.PiecePool[i]);
                w.ldelim();
            }
            if (m.NeedRange != null && m.NeedRange.length) {
                w.uint32(26).fork();
                for (var i = 0; i < m.NeedRange.length; ++i)
                    w.int32(m.NeedRange[i]);
                w.ldelim();
            }
            if (m.BaseCoins != null && m.hasOwnProperty("BaseCoins"))
                w.uint32(32).int32(m.BaseCoins);
            return w;
        };

        OrderSpec.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.OrderSpec();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.SpecId = r.int32();
                    break;
                case 2:
                    if (!(m.PiecePool && m.PiecePool.length))
                        m.PiecePool = [];
                    if ((t & 7) === 2) {
                        var c2 = r.uint32() + r.pos;
                        while (r.pos < c2)
                            m.PiecePool.push(r.int32());
                    } else
                        m.PiecePool.push(r.int32());
                    break;
                case 3:
                    if (!(m.NeedRange && m.NeedRange.length))
                        m.NeedRange = [];
                    if ((t & 7) === 2) {
                        var c2 = r.uint32() + r.pos;
                        while (r.pos < c2)
                            m.NeedRange.push(r.int32());
                    } else
                        m.NeedRange.push(r.int32());
                    break;
                case 4:
                    m.BaseCoins = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return OrderSpec;
    })();

    rawdata.OrderSpec_conf = (function() {

        function OrderSpec_conf(p) {
            this.OrderSpecs = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        OrderSpec_conf.prototype.OrderSpecs = $util.emptyObject;

        OrderSpec_conf.create = function create(properties) {
            return new OrderSpec_conf(properties);
        };

        OrderSpec_conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.OrderSpecs != null && m.hasOwnProperty("OrderSpecs")) {
                for (var ks = Object.keys(m.OrderSpecs), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]);
                    $root.rawdata.OrderSpec.encode(m.OrderSpecs[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        OrderSpec_conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.OrderSpec_conf(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.OrderSpecs === $util.emptyObject)
                        m.OrderSpecs = {};
                    k = r.int32();
                    r.pos++;
                    m.OrderSpecs[k] = $root.rawdata.OrderSpec.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return OrderSpec_conf;
    })();

    rawdata.PieceSpec = (function() {

        function PieceSpec(p) {
            this.Para = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        PieceSpec.prototype.SpecId = 0;
        PieceSpec.prototype.Coins = 0;
        PieceSpec.prototype.Type = 0;
        PieceSpec.prototype.Para = $util.emptyObject;
        PieceSpec.prototype.Name = "";
        PieceSpec.prototype.Desc = "";

        PieceSpec.create = function create(properties) {
            return new PieceSpec(properties);
        };

        PieceSpec.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.SpecId != null && m.hasOwnProperty("SpecId"))
                w.uint32(8).int32(m.SpecId);
            if (m.Coins != null && m.hasOwnProperty("Coins"))
                w.uint32(16).int32(m.Coins);
            if (m.Type != null && m.hasOwnProperty("Type"))
                w.uint32(24).int32(m.Type);
            if (m.Para != null && m.hasOwnProperty("Para")) {
                for (var ks = Object.keys(m.Para), i = 0; i < ks.length; ++i) {
                    w.uint32(34).fork().uint32(10).string(ks[i]).uint32(16).int32(m.Para[ks[i]]).ldelim();
                }
            }
            if (m.Name != null && m.hasOwnProperty("Name"))
                w.uint32(42).string(m.Name);
            if (m.Desc != null && m.hasOwnProperty("Desc"))
                w.uint32(50).string(m.Desc);
            return w;
        };

        PieceSpec.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.PieceSpec(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.SpecId = r.int32();
                    break;
                case 2:
                    m.Coins = r.int32();
                    break;
                case 3:
                    m.Type = r.int32();
                    break;
                case 4:
                    r.skip().pos++;
                    if (m.Para === $util.emptyObject)
                        m.Para = {};
                    k = r.string();
                    r.pos++;
                    m.Para[k] = r.int32();
                    break;
                case 5:
                    m.Name = r.string();
                    break;
                case 6:
                    m.Desc = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return PieceSpec;
    })();

    rawdata.PieceSpec_conf = (function() {

        function PieceSpec_conf(p) {
            this.PieceSpecs = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        PieceSpec_conf.prototype.PieceSpecs = $util.emptyObject;

        PieceSpec_conf.create = function create(properties) {
            return new PieceSpec_conf(properties);
        };

        PieceSpec_conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.PieceSpecs != null && m.hasOwnProperty("PieceSpecs")) {
                for (var ks = Object.keys(m.PieceSpecs), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]);
                    $root.rawdata.PieceSpec.encode(m.PieceSpecs[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        PieceSpec_conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.PieceSpec_conf(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.PieceSpecs === $util.emptyObject)
                        m.PieceSpecs = {};
                    k = r.int32();
                    r.pos++;
                    m.PieceSpecs[k] = $root.rawdata.PieceSpec.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return PieceSpec_conf;
    })();

    rawdata.ShareSpec = (function() {

        function ShareSpec(p) {
            this.ShareQueue = [];
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ShareSpec.prototype.ShareType = 0;
        ShareSpec.prototype.ShareQueue = $util.emptyArray;
        ShareSpec.prototype.RewardValue = 0;

        ShareSpec.create = function create(properties) {
            return new ShareSpec(properties);
        };

        ShareSpec.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.ShareType != null && m.hasOwnProperty("ShareType"))
                w.uint32(8).int32(m.ShareType);
            if (m.ShareQueue != null && m.ShareQueue.length) {
                w.uint32(18).fork();
                for (var i = 0; i < m.ShareQueue.length; ++i)
                    w.int32(m.ShareQueue[i]);
                w.ldelim();
            }
            if (m.RewardValue != null && m.hasOwnProperty("RewardValue"))
                w.uint32(24).int32(m.RewardValue);
            return w;
        };

        ShareSpec.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.ShareSpec();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.ShareType = r.int32();
                    break;
                case 2:
                    if (!(m.ShareQueue && m.ShareQueue.length))
                        m.ShareQueue = [];
                    if ((t & 7) === 2) {
                        var c2 = r.uint32() + r.pos;
                        while (r.pos < c2)
                            m.ShareQueue.push(r.int32());
                    } else
                        m.ShareQueue.push(r.int32());
                    break;
                case 3:
                    m.RewardValue = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ShareSpec;
    })();

    rawdata.ShareSpec_conf = (function() {

        function ShareSpec_conf(p) {
            this.ShareSpecs = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ShareSpec_conf.prototype.ShareSpecs = $util.emptyObject;

        ShareSpec_conf.create = function create(properties) {
            return new ShareSpec_conf(properties);
        };

        ShareSpec_conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.ShareSpecs != null && m.hasOwnProperty("ShareSpecs")) {
                for (var ks = Object.keys(m.ShareSpecs), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]);
                    $root.rawdata.ShareSpec.encode(m.ShareSpecs[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        ShareSpec_conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.ShareSpec_conf(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.ShareSpecs === $util.emptyObject)
                        m.ShareSpecs = {};
                    k = r.int32();
                    r.pos++;
                    m.ShareSpecs[k] = $root.rawdata.ShareSpec.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ShareSpec_conf;
    })();

    rawdata.StageSpec = (function() {

        function StageSpec(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        StageSpec.prototype.SpecId = 0;
        StageSpec.prototype.Type = 0;
        StageSpec.prototype.UnlockLevel = 0;
        StageSpec.prototype.GuestNum = 0;
        StageSpec.prototype.PieceNum = 0;
        StageSpec.prototype.SoupNum = 0;
        StageSpec.prototype.Name = "";

        StageSpec.create = function create(properties) {
            return new StageSpec(properties);
        };

        StageSpec.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.SpecId != null && m.hasOwnProperty("SpecId"))
                w.uint32(8).int32(m.SpecId);
            if (m.Type != null && m.hasOwnProperty("Type"))
                w.uint32(16).int32(m.Type);
            if (m.UnlockLevel != null && m.hasOwnProperty("UnlockLevel"))
                w.uint32(24).int32(m.UnlockLevel);
            if (m.GuestNum != null && m.hasOwnProperty("GuestNum"))
                w.uint32(32).int32(m.GuestNum);
            if (m.PieceNum != null && m.hasOwnProperty("PieceNum"))
                w.uint32(40).int32(m.PieceNum);
            if (m.SoupNum != null && m.hasOwnProperty("SoupNum"))
                w.uint32(48).int32(m.SoupNum);
            if (m.Name != null && m.hasOwnProperty("Name"))
                w.uint32(58).string(m.Name);
            return w;
        };

        StageSpec.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.StageSpec();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.SpecId = r.int32();
                    break;
                case 2:
                    m.Type = r.int32();
                    break;
                case 3:
                    m.UnlockLevel = r.int32();
                    break;
                case 4:
                    m.GuestNum = r.int32();
                    break;
                case 5:
                    m.PieceNum = r.int32();
                    break;
                case 6:
                    m.SoupNum = r.int32();
                    break;
                case 7:
                    m.Name = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return StageSpec;
    })();

    rawdata.StageSpec_conf = (function() {

        function StageSpec_conf(p) {
            this.StageSpecs = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        StageSpec_conf.prototype.StageSpecs = $util.emptyObject;

        StageSpec_conf.create = function create(properties) {
            return new StageSpec_conf(properties);
        };

        StageSpec_conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.StageSpecs != null && m.hasOwnProperty("StageSpecs")) {
                for (var ks = Object.keys(m.StageSpecs), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]);
                    $root.rawdata.StageSpec.encode(m.StageSpecs[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        StageSpec_conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.StageSpec_conf(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.StageSpecs === $util.emptyObject)
                        m.StageSpecs = {};
                    k = r.int32();
                    r.pos++;
                    m.StageSpecs[k] = $root.rawdata.StageSpec.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return StageSpec_conf;
    })();

    rawdata.StoreSpec = (function() {

        function StoreSpec(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        StoreSpec.prototype.SpecId = 0;
        StoreSpec.prototype.Coins = 0;
        StoreSpec.prototype.ItemId = 0;
        StoreSpec.prototype.Desc = "";

        StoreSpec.create = function create(properties) {
            return new StoreSpec(properties);
        };

        StoreSpec.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.SpecId != null && m.hasOwnProperty("SpecId"))
                w.uint32(8).int32(m.SpecId);
            if (m.Coins != null && m.hasOwnProperty("Coins"))
                w.uint32(16).int32(m.Coins);
            if (m.ItemId != null && m.hasOwnProperty("ItemId"))
                w.uint32(24).int32(m.ItemId);
            if (m.Desc != null && m.hasOwnProperty("Desc"))
                w.uint32(34).string(m.Desc);
            return w;
        };

        StoreSpec.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.StoreSpec();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.SpecId = r.int32();
                    break;
                case 2:
                    m.Coins = r.int32();
                    break;
                case 3:
                    m.ItemId = r.int32();
                    break;
                case 4:
                    m.Desc = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return StoreSpec;
    })();

    rawdata.StoreSpec_conf = (function() {

        function StoreSpec_conf(p) {
            this.StoreSpecs = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        StoreSpec_conf.prototype.StoreSpecs = $util.emptyObject;

        StoreSpec_conf.create = function create(properties) {
            return new StoreSpec_conf(properties);
        };

        StoreSpec_conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.StoreSpecs != null && m.hasOwnProperty("StoreSpecs")) {
                for (var ks = Object.keys(m.StoreSpecs), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]);
                    $root.rawdata.StoreSpec.encode(m.StoreSpecs[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        StoreSpec_conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.StoreSpec_conf(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.StoreSpecs === $util.emptyObject)
                        m.StoreSpecs = {};
                    k = r.int32();
                    r.pos++;
                    m.StoreSpecs[k] = $root.rawdata.StoreSpec.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return StoreSpec_conf;
    })();

    rawdata.TaskSpec = (function() {

        function TaskSpec(p) {
            this.reward = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        TaskSpec.prototype.Type = "";
        TaskSpec.prototype.string = "";
        TaskSpec.prototype.LowParameter = 0;
        TaskSpec.prototype.UpParameter = 0;
        TaskSpec.prototype.initParameter = 0;
        TaskSpec.prototype.reward = $util.emptyObject;

        TaskSpec.create = function create(properties) {
            return new TaskSpec(properties);
        };

        TaskSpec.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.Type != null && m.hasOwnProperty("Type"))
                w.uint32(10).string(m.Type);
            if (m.string != null && m.hasOwnProperty("string"))
                w.uint32(18).string(m.string);
            if (m.LowParameter != null && m.hasOwnProperty("LowParameter"))
                w.uint32(24).int32(m.LowParameter);
            if (m.UpParameter != null && m.hasOwnProperty("UpParameter"))
                w.uint32(32).int32(m.UpParameter);
            if (m.initParameter != null && m.hasOwnProperty("initParameter"))
                w.uint32(40).int32(m.initParameter);
            if (m.reward != null && m.hasOwnProperty("reward")) {
                for (var ks = Object.keys(m.reward), i = 0; i < ks.length; ++i) {
                    w.uint32(50).fork().uint32(8).int32(ks[i]).uint32(16).int32(m.reward[ks[i]]).ldelim();
                }
            }
            return w;
        };

        TaskSpec.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.TaskSpec(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.Type = r.string();
                    break;
                case 2:
                    m.string = r.string();
                    break;
                case 3:
                    m.LowParameter = r.int32();
                    break;
                case 4:
                    m.UpParameter = r.int32();
                    break;
                case 5:
                    m.initParameter = r.int32();
                    break;
                case 6:
                    r.skip().pos++;
                    if (m.reward === $util.emptyObject)
                        m.reward = {};
                    k = r.int32();
                    r.pos++;
                    m.reward[k] = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return TaskSpec;
    })();

    rawdata.TaskSpec_conf = (function() {

        function TaskSpec_conf(p) {
            this.TaskSpecs = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        TaskSpec_conf.prototype.TaskSpecs = $util.emptyObject;

        TaskSpec_conf.create = function create(properties) {
            return new TaskSpec_conf(properties);
        };

        TaskSpec_conf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.TaskSpecs != null && m.hasOwnProperty("TaskSpecs")) {
                for (var ks = Object.keys(m.TaskSpecs), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(10).string(ks[i]);
                    $root.rawdata.TaskSpec.encode(m.TaskSpecs[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        TaskSpec_conf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.rawdata.TaskSpec_conf(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.TaskSpecs === $util.emptyObject)
                        m.TaskSpecs = {};
                    k = r.string();
                    r.pos++;
                    m.TaskSpecs[k] = $root.rawdata.TaskSpec.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return TaskSpec_conf;
    })();

    return rawdata;
})();

$root.requests = (function() {

    var requests = {};

    requests.ClientStoragePut = (function() {

        function ClientStoragePut(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ClientStoragePut.prototype.key = "";
        ClientStoragePut.prototype.val = "";
        ClientStoragePut.prototype.timeout = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        ClientStoragePut.create = function create(properties) {
            return new ClientStoragePut(properties);
        };

        ClientStoragePut.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.key != null && m.hasOwnProperty("key"))
                w.uint32(10).string(m.key);
            if (m.val != null && m.hasOwnProperty("val"))
                w.uint32(18).string(m.val);
            if (m.timeout != null && m.hasOwnProperty("timeout"))
                w.uint32(24).int64(m.timeout);
            return w;
        };

        ClientStoragePut.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.ClientStoragePut();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.key = r.string();
                    break;
                case 2:
                    m.val = r.string();
                    break;
                case 3:
                    m.timeout = r.int64();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ClientStoragePut;
    })();

    requests.ClientStorageGet = (function() {

        function ClientStorageGet(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ClientStorageGet.prototype.key = "";

        ClientStorageGet.create = function create(properties) {
            return new ClientStorageGet(properties);
        };

        ClientStorageGet.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.key != null && m.hasOwnProperty("key"))
                w.uint32(10).string(m.key);
            return w;
        };

        ClientStorageGet.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.ClientStorageGet();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.key = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ClientStorageGet;
    })();

    requests.ClientStorageDel = (function() {

        function ClientStorageDel(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ClientStorageDel.prototype.key = "";

        ClientStorageDel.create = function create(properties) {
            return new ClientStorageDel(properties);
        };

        ClientStorageDel.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.key != null && m.hasOwnProperty("key"))
                w.uint32(10).string(m.key);
            return w;
        };

        ClientStorageDel.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.ClientStorageDel();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.key = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ClientStorageDel;
    })();

    requests.ClientStorageReset = (function() {

        function ClientStorageReset(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ClientStorageReset.create = function create(properties) {
            return new ClientStorageReset(properties);
        };

        ClientStorageReset.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        ClientStorageReset.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.ClientStorageReset();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ClientStorageReset;
    })();

    requests.MessageList = (function() {

        function MessageList(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        MessageList.create = function create(properties) {
            return new MessageList(properties);
        };

        MessageList.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        MessageList.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.MessageList();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return MessageList;
    })();

    requests.MessageAdd = (function() {

        function MessageAdd(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        MessageAdd.prototype.Message = null;

        MessageAdd.create = function create(properties) {
            return new MessageAdd(properties);
        };

        MessageAdd.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.Message != null && m.hasOwnProperty("Message"))
                $root.gamekits.Message.encode(m.Message, w.uint32(10).fork()).ldelim();
            return w;
        };

        MessageAdd.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.MessageAdd();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.Message = $root.gamekits.Message.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return MessageAdd;
    })();

    requests.MessageRead = (function() {

        function MessageRead(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        MessageRead.prototype.MsgId = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        MessageRead.create = function create(properties) {
            return new MessageRead(properties);
        };

        MessageRead.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.MsgId != null && m.hasOwnProperty("MsgId"))
                w.uint32(8).uint64(m.MsgId);
            return w;
        };

        MessageRead.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.MessageRead();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.MsgId = r.uint64();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return MessageRead;
    })();

    requests.MessageDelete = (function() {

        function MessageDelete(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        MessageDelete.prototype.MsgId = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        MessageDelete.create = function create(properties) {
            return new MessageDelete(properties);
        };

        MessageDelete.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.MsgId != null && m.hasOwnProperty("MsgId"))
                w.uint32(8).uint64(m.MsgId);
            return w;
        };

        MessageDelete.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.MessageDelete();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.MsgId = r.uint64();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return MessageDelete;
    })();

    requests.MessageAddReward = (function() {

        function MessageAddReward(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        MessageAddReward.prototype.MsgId = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        MessageAddReward.create = function create(properties) {
            return new MessageAddReward(properties);
        };

        MessageAddReward.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.MsgId != null && m.hasOwnProperty("MsgId"))
                w.uint32(8).uint64(m.MsgId);
            return w;
        };

        MessageAddReward.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.MessageAddReward();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.MsgId = r.uint64();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return MessageAddReward;
    })();

    requests.FetchPmConf = (function() {

        function FetchPmConf(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        FetchPmConf.create = function create(properties) {
            return new FetchPmConf(properties);
        };

        FetchPmConf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        FetchPmConf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.FetchPmConf();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return FetchPmConf;
    })();

    requests.Login = (function() {

        function Login(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        Login.prototype.platform = 0;
        Login.prototype.platformId = "";
        Login.prototype.code = "";

        Login.create = function create(properties) {
            return new Login(properties);
        };

        Login.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.platform != null && m.hasOwnProperty("platform"))
                w.uint32(8).int32(m.platform);
            if (m.platformId != null && m.hasOwnProperty("platformId"))
                w.uint32(18).string(m.platformId);
            if (m.code != null && m.hasOwnProperty("code"))
                w.uint32(26).string(m.code);
            return w;
        };

        Login.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.Login();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.platform = r.int32();
                    break;
                case 2:
                    m.platformId = r.string();
                    break;
                case 3:
                    m.code = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return Login;
    })();

    requests.CollectCoins = (function() {

        function CollectCoins(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        CollectCoins.prototype.coins = $util.Long ? $util.Long.fromBits(0,0,true) : 0;
        CollectCoins.prototype.hasShare = false;

        CollectCoins.create = function create(properties) {
            return new CollectCoins(properties);
        };

        CollectCoins.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.coins != null && m.hasOwnProperty("coins"))
                w.uint32(8).uint64(m.coins);
            if (m.hasShare != null && m.hasOwnProperty("hasShare"))
                w.uint32(16).bool(m.hasShare);
            return w;
        };

        CollectCoins.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.CollectCoins();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.coins = r.uint64();
                    break;
                case 2:
                    m.hasShare = r.bool();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return CollectCoins;
    })();

    requests.Share = (function() {

        function Share(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        Share.prototype.shareType = 0;

        Share.create = function create(properties) {
            return new Share(properties);
        };

        Share.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.shareType != null && m.hasOwnProperty("shareType"))
                w.uint32(8).int32(m.shareType);
            return w;
        };

        Share.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.Share();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.shareType = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return Share;
    })();

    requests.ListTasks = (function() {

        function ListTasks(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ListTasks.create = function create(properties) {
            return new ListTasks(properties);
        };

        ListTasks.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        ListTasks.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.ListTasks();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ListTasks;
    })();

    requests.IncrementTask = (function() {

        function IncrementTask(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        IncrementTask.prototype.taskId = 0;
        IncrementTask.prototype.incValue = 0;

        IncrementTask.create = function create(properties) {
            return new IncrementTask(properties);
        };

        IncrementTask.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.taskId != null && m.hasOwnProperty("taskId"))
                w.uint32(8).int32(m.taskId);
            if (m.incValue != null && m.hasOwnProperty("incValue"))
                w.uint32(16).int32(m.incValue);
            return w;
        };

        IncrementTask.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.IncrementTask();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.taskId = r.int32();
                    break;
                case 2:
                    m.incValue = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return IncrementTask;
    })();

    requests.FetchTaskReward = (function() {

        function FetchTaskReward(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        FetchTaskReward.prototype.taskId = 0;

        FetchTaskReward.create = function create(properties) {
            return new FetchTaskReward(properties);
        };

        FetchTaskReward.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.taskId != null && m.hasOwnProperty("taskId"))
                w.uint32(8).int32(m.taskId);
            return w;
        };

        FetchTaskReward.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.FetchTaskReward();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.taskId = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return FetchTaskReward;
    })();

    requests.FetchGuideReward = (function() {

        function FetchGuideReward(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        FetchGuideReward.prototype.id = 0;

        FetchGuideReward.create = function create(properties) {
            return new FetchGuideReward(properties);
        };

        FetchGuideReward.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.id != null && m.hasOwnProperty("id"))
                w.uint32(8).int32(m.id);
            return w;
        };

        FetchGuideReward.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.FetchGuideReward();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.id = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return FetchGuideReward;
    })();

    requests.BeginStage = (function() {

        function BeginStage(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        BeginStage.prototype.stageId = 0;

        BeginStage.create = function create(properties) {
            return new BeginStage(properties);
        };

        BeginStage.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.stageId != null && m.hasOwnProperty("stageId"))
                w.uint32(8).int32(m.stageId);
            return w;
        };

        BeginStage.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.BeginStage();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.stageId = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return BeginStage;
    })();

    requests.ClearPieces = (function() {

        function ClearPieces(p) {
            this.details = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ClearPieces.prototype.details = $util.emptyObject;
        ClearPieces.prototype.coins = $util.Long ? $util.Long.fromBits(0,0,true) : 0;
        ClearPieces.prototype.isEnd = false;

        ClearPieces.create = function create(properties) {
            return new ClearPieces(properties);
        };

        ClearPieces.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.details != null && m.hasOwnProperty("details")) {
                for (var ks = Object.keys(m.details), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]);
                    $root.gamekits.ClearDetail.encode(m.details[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            if (m.coins != null && m.hasOwnProperty("coins"))
                w.uint32(16).uint64(m.coins);
            if (m.isEnd != null && m.hasOwnProperty("isEnd"))
                w.uint32(24).bool(m.isEnd);
            return w;
        };

        ClearPieces.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.ClearPieces(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.details === $util.emptyObject)
                        m.details = {};
                    k = r.int32();
                    r.pos++;
                    m.details[k] = $root.gamekits.ClearDetail.decode(r, r.uint32());
                    break;
                case 2:
                    m.coins = r.uint64();
                    break;
                case 3:
                    m.isEnd = r.bool();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ClearPieces;
    })();

    requests.CostSoup = (function() {

        function CostSoup(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        CostSoup.prototype.soup = 0;

        CostSoup.create = function create(properties) {
            return new CostSoup(properties);
        };

        CostSoup.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.soup != null && m.hasOwnProperty("soup"))
                w.uint32(8).int32(m.soup);
            return w;
        };

        CostSoup.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.CostSoup();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.soup = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return CostSoup;
    })();

    requests.StageHistory = (function() {

        function StageHistory(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        StageHistory.create = function create(properties) {
            return new StageHistory(properties);
        };

        StageHistory.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        StageHistory.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.StageHistory();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return StageHistory;
    })();

    requests.BuyStoreItem = (function() {

        function BuyStoreItem(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        BuyStoreItem.prototype.storeId = 0;

        BuyStoreItem.create = function create(properties) {
            return new BuyStoreItem(properties);
        };

        BuyStoreItem.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.storeId != null && m.hasOwnProperty("storeId"))
                w.uint32(8).int32(m.storeId);
            return w;
        };

        BuyStoreItem.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.BuyStoreItem();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.storeId = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return BuyStoreItem;
    })();

    requests.WeChatAuth = (function() {

        function WeChatAuth(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        WeChatAuth.prototype.code = "";

        WeChatAuth.create = function create(properties) {
            return new WeChatAuth(properties);
        };

        WeChatAuth.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.code != null && m.hasOwnProperty("code"))
                w.uint32(10).string(m.code);
            return w;
        };

        WeChatAuth.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.requests.WeChatAuth();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.code = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return WeChatAuth;
    })();

    return requests;
})();

$root.responses = (function() {

    var responses = {};

    responses.ClientStoragePut = (function() {

        function ClientStoragePut(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ClientStoragePut.create = function create(properties) {
            return new ClientStoragePut(properties);
        };

        ClientStoragePut.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        ClientStoragePut.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.ClientStoragePut();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ClientStoragePut;
    })();

    responses.ClientStorageGet = (function() {

        function ClientStorageGet(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ClientStorageGet.prototype.val = "";

        ClientStorageGet.create = function create(properties) {
            return new ClientStorageGet(properties);
        };

        ClientStorageGet.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.val != null && m.hasOwnProperty("val"))
                w.uint32(10).string(m.val);
            return w;
        };

        ClientStorageGet.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.ClientStorageGet();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.val = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ClientStorageGet;
    })();

    responses.ClientStorageDel = (function() {

        function ClientStorageDel(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ClientStorageDel.create = function create(properties) {
            return new ClientStorageDel(properties);
        };

        ClientStorageDel.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        ClientStorageDel.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.ClientStorageDel();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ClientStorageDel;
    })();

    responses.ClientStorageReset = (function() {

        function ClientStorageReset(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ClientStorageReset.create = function create(properties) {
            return new ClientStorageReset(properties);
        };

        ClientStorageReset.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        ClientStorageReset.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.ClientStorageReset();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ClientStorageReset;
    })();

    responses.MessageList = (function() {

        function MessageList(p) {
            this.messages = [];
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        MessageList.prototype.messages = $util.emptyArray;

        MessageList.create = function create(properties) {
            return new MessageList(properties);
        };

        MessageList.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.messages != null && m.messages.length) {
                for (var i = 0; i < m.messages.length; ++i)
                    $root.gamekits.Message.encode(m.messages[i], w.uint32(10).fork()).ldelim();
            }
            return w;
        };

        MessageList.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.MessageList();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    if (!(m.messages && m.messages.length))
                        m.messages = [];
                    m.messages.push($root.gamekits.Message.decode(r, r.uint32()));
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return MessageList;
    })();

    responses.MessageAdd = (function() {

        function MessageAdd(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        MessageAdd.create = function create(properties) {
            return new MessageAdd(properties);
        };

        MessageAdd.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        MessageAdd.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.MessageAdd();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return MessageAdd;
    })();

    responses.MessageRead = (function() {

        function MessageRead(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        MessageRead.create = function create(properties) {
            return new MessageRead(properties);
        };

        MessageRead.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        MessageRead.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.MessageRead();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return MessageRead;
    })();

    responses.MessageDelete = (function() {

        function MessageDelete(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        MessageDelete.create = function create(properties) {
            return new MessageDelete(properties);
        };

        MessageDelete.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        MessageDelete.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.MessageDelete();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return MessageDelete;
    })();

    responses.MessageAddReward = (function() {

        function MessageAddReward(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        MessageAddReward.create = function create(properties) {
            return new MessageAddReward(properties);
        };

        MessageAddReward.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        MessageAddReward.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.MessageAddReward();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return MessageAddReward;
    })();

    responses.FetchPmConf = (function() {

        function FetchPmConf(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        FetchPmConf.prototype.pmconf = null;

        FetchPmConf.create = function create(properties) {
            return new FetchPmConf(properties);
        };

        FetchPmConf.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.pmconf != null && m.hasOwnProperty("pmconf"))
                $root.gamekits.Conf.encode(m.pmconf, w.uint32(10).fork()).ldelim();
            return w;
        };

        FetchPmConf.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.FetchPmConf();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.pmconf = $root.gamekits.Conf.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return FetchPmConf;
    })();

    responses.PlayerData = (function() {

        function PlayerData(p) {
            this.decorations = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        PlayerData.prototype.base = null;
        PlayerData.prototype.clientInfo = null;
        PlayerData.prototype.coins = $util.Long ? $util.Long.fromBits(0,0,true) : 0;
        PlayerData.prototype.exp = $util.Long ? $util.Long.fromBits(0,0,true) : 0;
        PlayerData.prototype.collectAt = $util.Long ? $util.Long.fromBits(0,0,true) : 0;
        PlayerData.prototype.decorations = $util.emptyObject;

        PlayerData.create = function create(properties) {
            return new PlayerData(properties);
        };

        PlayerData.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.base != null && m.hasOwnProperty("base"))
                $root.gamekits.BasePlayerData.encode(m.base, w.uint32(10).fork()).ldelim();
            if (m.clientInfo != null && m.hasOwnProperty("clientInfo"))
                $root.gamekits.ClientStorageState.encode(m.clientInfo, w.uint32(18).fork()).ldelim();
            if (m.coins != null && m.hasOwnProperty("coins"))
                w.uint32(24).uint64(m.coins);
            if (m.exp != null && m.hasOwnProperty("exp"))
                w.uint32(32).uint64(m.exp);
            if (m.collectAt != null && m.hasOwnProperty("collectAt"))
                w.uint32(40).uint64(m.collectAt);
            if (m.decorations != null && m.hasOwnProperty("decorations")) {
                for (var ks = Object.keys(m.decorations), i = 0; i < ks.length; ++i) {
                    w.uint32(50).fork().uint32(8).int32(ks[i]).uint32(16).uint64(m.decorations[ks[i]]).ldelim();
                }
            }
            return w;
        };

        PlayerData.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.PlayerData(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.base = $root.gamekits.BasePlayerData.decode(r, r.uint32());
                    break;
                case 2:
                    m.clientInfo = $root.gamekits.ClientStorageState.decode(r, r.uint32());
                    break;
                case 3:
                    m.coins = r.uint64();
                    break;
                case 4:
                    m.exp = r.uint64();
                    break;
                case 5:
                    m.collectAt = r.uint64();
                    break;
                case 6:
                    r.skip().pos++;
                    if (m.decorations === $util.emptyObject)
                        m.decorations = {};
                    k = r.int32();
                    r.pos++;
                    m.decorations[k] = r.uint64();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return PlayerData;
    })();

    responses.Login = (function() {

        function Login(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        Login.prototype.st = $util.Long ? $util.Long.fromBits(0,0,true) : 0;
        Login.prototype.token = "";
        Login.prototype.player = null;
        Login.prototype.isNew = false;

        Login.create = function create(properties) {
            return new Login(properties);
        };

        Login.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.st != null && m.hasOwnProperty("st"))
                w.uint32(8).uint64(m.st);
            if (m.token != null && m.hasOwnProperty("token"))
                w.uint32(18).string(m.token);
            if (m.player != null && m.hasOwnProperty("player"))
                $root.responses.PlayerData.encode(m.player, w.uint32(26).fork()).ldelim();
            if (m.isNew != null && m.hasOwnProperty("isNew"))
                w.uint32(32).bool(m.isNew);
            return w;
        };

        Login.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.Login();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.st = r.uint64();
                    break;
                case 2:
                    m.token = r.string();
                    break;
                case 3:
                    m.player = $root.responses.PlayerData.decode(r, r.uint32());
                    break;
                case 4:
                    m.isNew = r.bool();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return Login;
    })();

    responses.CollectCoins = (function() {

        function CollectCoins(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        CollectCoins.create = function create(properties) {
            return new CollectCoins(properties);
        };

        CollectCoins.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        CollectCoins.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.CollectCoins();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return CollectCoins;
    })();

    responses.Share = (function() {

        function Share(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        Share.create = function create(properties) {
            return new Share(properties);
        };

        Share.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        Share.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.Share();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return Share;
    })();

    responses.IncrementTask = (function() {

        function IncrementTask(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        IncrementTask.create = function create(properties) {
            return new IncrementTask(properties);
        };

        IncrementTask.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        IncrementTask.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.IncrementTask();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return IncrementTask;
    })();

    responses.FetchTaskReward = (function() {

        function FetchTaskReward(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        FetchTaskReward.create = function create(properties) {
            return new FetchTaskReward(properties);
        };

        FetchTaskReward.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        FetchTaskReward.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.FetchTaskReward();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return FetchTaskReward;
    })();

    responses.ListTasks = (function() {

        function ListTasks(p) {
            this.tasks = {};
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ListTasks.prototype.tasks = $util.emptyObject;

        ListTasks.create = function create(properties) {
            return new ListTasks(properties);
        };

        ListTasks.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.tasks != null && m.hasOwnProperty("tasks")) {
                for (var ks = Object.keys(m.tasks), i = 0; i < ks.length; ++i) {
                    w.uint32(10).fork().uint32(8).int32(ks[i]);
                    $root.gamekits.TaskData.encode(m.tasks[ks[i]], w.uint32(18).fork()).ldelim().ldelim();
                }
            }
            return w;
        };

        ListTasks.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.ListTasks(), k;
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    r.skip().pos++;
                    if (m.tasks === $util.emptyObject)
                        m.tasks = {};
                    k = r.int32();
                    r.pos++;
                    m.tasks[k] = $root.gamekits.TaskData.decode(r, r.uint32());
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ListTasks;
    })();

    responses.FetchGuideReward = (function() {

        function FetchGuideReward(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        FetchGuideReward.create = function create(properties) {
            return new FetchGuideReward(properties);
        };

        FetchGuideReward.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        FetchGuideReward.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.FetchGuideReward();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return FetchGuideReward;
    })();

    responses.BeginStage = (function() {

        function BeginStage(p) {
            this.npcs = [];
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        BeginStage.prototype.npcs = $util.emptyArray;

        BeginStage.create = function create(properties) {
            return new BeginStage(properties);
        };

        BeginStage.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.npcs != null && m.npcs.length) {
                for (var i = 0; i < m.npcs.length; ++i)
                    $root.gamekits.NpcData.encode(m.npcs[i], w.uint32(10).fork()).ldelim();
            }
            return w;
        };

        BeginStage.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.BeginStage();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    if (!(m.npcs && m.npcs.length))
                        m.npcs = [];
                    m.npcs.push($root.gamekits.NpcData.decode(r, r.uint32()));
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return BeginStage;
    })();

    responses.ClearPieces = (function() {

        function ClearPieces(p) {
            this.npcs = [];
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        ClearPieces.prototype.npcs = $util.emptyArray;

        ClearPieces.create = function create(properties) {
            return new ClearPieces(properties);
        };

        ClearPieces.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.npcs != null && m.npcs.length) {
                for (var i = 0; i < m.npcs.length; ++i)
                    $root.gamekits.NpcData.encode(m.npcs[i], w.uint32(10).fork()).ldelim();
            }
            return w;
        };

        ClearPieces.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.ClearPieces();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    if (!(m.npcs && m.npcs.length))
                        m.npcs = [];
                    m.npcs.push($root.gamekits.NpcData.decode(r, r.uint32()));
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return ClearPieces;
    })();

    responses.CostSoup = (function() {

        function CostSoup(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        CostSoup.create = function create(properties) {
            return new CostSoup(properties);
        };

        CostSoup.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        CostSoup.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.CostSoup();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return CostSoup;
    })();

    responses.StageHistory = (function() {

        function StageHistory(p) {
            this.history = [];
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        StageHistory.prototype.history = $util.emptyArray;

        StageHistory.create = function create(properties) {
            return new StageHistory(properties);
        };

        StageHistory.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.history != null && m.history.length) {
                for (var i = 0; i < m.history.length; ++i)
                    $root.gamekits.StageRecord.encode(m.history[i], w.uint32(10).fork()).ldelim();
            }
            return w;
        };

        StageHistory.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.StageHistory();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    if (!(m.history && m.history.length))
                        m.history = [];
                    m.history.push($root.gamekits.StageRecord.decode(r, r.uint32()));
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return StageHistory;
    })();

    responses.BuyStoreItem = (function() {

        function BuyStoreItem(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        BuyStoreItem.create = function create(properties) {
            return new BuyStoreItem(properties);
        };

        BuyStoreItem.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            return w;
        };

        BuyStoreItem.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.BuyStoreItem();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return BuyStoreItem;
    })();

    responses.WeChatAuth = (function() {

        function WeChatAuth(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        WeChatAuth.prototype.openid = "";
        WeChatAuth.prototype.unionid = "";
        WeChatAuth.prototype.sessionKey = "";

        WeChatAuth.create = function create(properties) {
            return new WeChatAuth(properties);
        };

        WeChatAuth.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.openid != null && m.hasOwnProperty("openid"))
                w.uint32(10).string(m.openid);
            if (m.unionid != null && m.hasOwnProperty("unionid"))
                w.uint32(18).string(m.unionid);
            if (m.sessionKey != null && m.hasOwnProperty("sessionKey"))
                w.uint32(26).string(m.sessionKey);
            return w;
        };

        WeChatAuth.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.responses.WeChatAuth();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.openid = r.string();
                    break;
                case 2:
                    m.unionid = r.string();
                    break;
                case 3:
                    m.sessionKey = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            return m;
        };

        return WeChatAuth;
    })();

    return responses;
})();

module.exports = $root;
