// FoodManager.ts
// Created by zacklocx on 2018-12-23

const {ccclass, property} = cc._decorator;

import configLoader = require('../common/configLoader');
import utils = require('../common/utils');
import game = require('../game');
import ds = require('../common/ds');
import tools = require('../common/tools');
import soundManager = require('../soundManager');
import ui = require('../ui/ui');

@ccclass
export default class FoodManager extends cc.Component
{
    @property(cc.Node)
    canvas: cc.Node = null;

    @property(cc.Prefab)
    food_prefab: cc.Prefab = null;

    @property(cc.Node)
    pot_left: cc.Node = null;

    @property(cc.Node)
    pot_right: cc.Node = null;

    @property(cc.Sprite)
    coin_icon: cc.Sprite = null;

    @property(cc.Sprite)
    soup_icon: cc.Sprite = null;

    @property(cc.Node)
    debug_node: cc.Node = null;

    @property
    _touching = false;

    @property(cc.Vec2)
    _last_touch_pos: cc.Vec2 = null;

    @property
    _current_selected = -1;
    _current_selected_flag = -1;

    @property
    _exploding = false;

    @property
    _auto_exploding = false;

    @property(Map)
    _config: Map<string, any> = null;

    @property(Array)
    _bodies: Array<any> = null;

    @property(Array)
    _selected_stack: Array<any> = null;

    @property(Set)
    _all_selected: Set<any> = null;

    @property(Set)
    _auto_list: Set<any> = null;

    @property(Map)
    _auto_connect_list: Map<any, any> = null;

    @property(Set)
    _auto_delete_list: Set<any> = null;

    @property(Set)
    _connected_list: Set<any> = null;

    @property(Set)
    _hint_list: Set<any> = null;

    @property(Set)
    _explode_preview_list: Set<any> = null;

    @property(Set)
    _explode_list: Set<any> = null;
    _explode_list_to_explore: Set<any> = null;

    @property(Set)
    _bomb_list: Set<any> = null;

    @property(cc.Node)
    _selected_bomb: cc.Node = null;

    @property(Map)
    _bomb_level: Map<any, any> = null;

    @property
    _timeout_callback = null;

    @property
    _min_connected_num = 0;

    @property
    _max_connected_num = 0;

    @property(cc.Node)
    food_node : cc.Node = null;

    effectLayer: cc.Node = null;
    crashEffectLayer : cc.Node = null;
    auto_explore_connected_list: Set<any> = null;
    auto_explore_map : Map<any, any> = null;
    bomb_config_connect_array: Array<any> = null;
    _hint_list_array: Array<any> = null;
    delayTimer : Number = 0;
    delayTime : Number = 0;
    delayTimerStart : Boolean = false;
    stageFinished : Boolean = false;
    comboIndex : any = 0;
    auto_connect_number_limit : any = 0;
    current_connect_food_id : any = 0;
    timeToCheckVaild : any = 0;

    onInitialize()
    {
        cc.director.getPhysicsManager().enabled = true;

        this.canvas.on(cc.Node.EventType.TOUCH_START,
            function(event)
            {
                let touches = event.getTouches();
                let touch_pos = touches[0].getLocation();

                this.touch_began(touch_pos);
            }, this);

        this.canvas.on(cc.Node.EventType.TOUCH_MOVE,
            function(event)
            {
                let touches = event.getTouches();
                let touch_pos = touches[0].getLocation();

                this.touch_move(touch_pos);
            }, this);

        this.canvas.on(cc.Node.EventType.TOUCH_END,
            function(event)
            {
                let touches = event.getTouches();
                let touch_pos = touches[0].getLocation();

                this.touch_end(touch_pos);
            }, this);

        this.init();
    }

    init()
    {
        this._current_selected = -1;
        this._exploding = false;

        this._config = new Map();
        this._bodies = new Array();
        this._selected_stack = new Array();
        this._all_selected = new Set();
        this._auto_list = new Set();
        this._auto_connect_list = new Map();
        this._auto_delete_list = new Set();
        this.auto_explore_connected_list = new Set();
        this.auto_explore_map = new Map();
        this._connected_list = new Set();
        this._hint_list = new Set();
        this._explode_preview_list = new Set();
        this._explode_list = new Set();
        this._explode_list_to_explore = new Set();
        this._bomb_list = new Set();
        this.bomb_config_connect_array = new Array();
        this._hint_list_array = new Array();
        // this._bomb_level = new Map();
        this._selected_bomb = null;

        let config = configLoader.instance.ConfigSpecConf.ConfigSpecs[0];

        let stage = game.instance.getGameLevelStage();
        let stageConfig = configLoader.instance.getStageConfig(stage);

        cc.log('stage pieces: ' + stage + ' ' + stageConfig.PieceNum);

        this._config.set('max_gap', config.MatchDistance);
        this._config.set('extra_dist', config.MatchDistance);
        this._config.set('min_remove_num', 3);
        this._config.set('max_body_num', stageConfig.PieceNum);
        this._config.set('hint_seconds', config.AutoTip);
        this._config.set('auto_match_num', config.AutoMatchNum);
    
        // this._config.set('bomb_1_radius', 120.0);
        // this._config.set('bomb_2_size', 50.0);

        // this._bomb_level.set(1, 4);
        // this._bomb_level.set(2, 5);

        // let s = new Set();
        // s.add(1);
        // s.add(2);
        // s.add(3);
        // s.add(4);
        // s.add(5);
        
        // console.log('start');
        
        // s.forEach((value) => {
        //     console.log('value: ' + value + ', size: ' + s.size);
        //     s.delete(value);
        // });

        for(var i = 30001; i <= 30003; ++i)
        {
            let bomb_config = configLoader.instance.getPieceConfig(i);
            let n1 = bomb_config.Para.n1;
            let n2 = bomb_config.Para.n2;

            if(0 == this._min_connected_num || n1 < this._min_connected_num)
            {
                this._min_connected_num = n1;
            }

            if(0 == this._max_connected_num || n2 > this._max_connected_num)
            {
                this._max_connected_num = n2;
            }

            this.bomb_config_connect_array.push(n1);
        }

        this._timeout_callback = function() { this.hint(); };
    }

    start()
    {
        //this.generate(this._config.get('max_body_num'), false);
    }

    reset()
    {
        this.remove_array(this._bodies, false);

        this._current_selected = -1;
        this._bodies.splice(0, this._bodies.length);
        this._selected_stack.splice(0, this._selected_stack.length);
        this._all_selected.clear();
        this._connected_list.clear();
        this._explode_preview_list.clear();
        this._explode_list.clear();
        this._explode_list_to_explore.clear();
        this._bomb_list.clear();
        this._auto_list.clear();
        this._auto_connect_list.clear();
        this._auto_delete_list.clear();
        this.auto_explore_connected_list.clear();
        this.auto_explore_map.clear();

        this.generate(this._config.get('max_body_num'), false);
    }

    send_event(name, param, extra = null)
    {
        if('feed_food' == name)
        {
            let pos = cc.v2(0, 0);
            let num = 0;
            if(this._selected_stack.length > 0)
            {
                for(let i = 0; i < this._selected_stack.length; ++i)
                {
                    let body = this._bodies[this._selected_stack[i]];
                    let pos2 = body.getComponent(cc.RigidBody).getWorldCenter();

                    let x = pos2.x - this.canvas.width * 0.5
                    let y = pos2.y - this.canvas.height * 0.5

                    pos.x += x;
                    pos.y += y;
                    num ++;
                }
            }

            if (num > 0) {
                pos.x /= num;
                pos.y /= num;
            }

            param.set('pos', pos);
        }

        let event = new cc.Event.EventCustom(name, true);
        event.setUserData(param);
        this.node.dispatchEvent(event);
    }

    touch_began(touch_pos)
    {
        if (this.stageFinished){
            return false;
        }

        if (this.delayTimerStart ||
            this.auto_explore_connected_list.size > 0) {
            return false;
        }

        this.current_connect_food_id = 0;

        this.comboIndex = 0;
        // cc.log('touch_began ' + touch_pos.x + ', ' + touch_pos.y);

        this.unschedule(this._timeout_callback);

        this._hint_list.forEach((hint_body) =>
        {
            this.apply_fx(hint_body, 'default');
        });

        this._hint_list.clear();

        this._touching = true;
        this._last_touch_pos = touch_pos;

        this._current_selected = -1;
        
        let nearest_dist_sq = 0;

        let new_dist = this._config.get('extra_dist') * 4;
        let new_dist_sq = new_dist * new_dist;

        for(let i = 0; i < this._bodies.length; ++i)
        {
            let body = this._bodies[i];
            let pos = body.getComponent(cc.RigidBody).getWorldCenter();

            if(this.is_touched(body, touch_pos))
            {
                this._current_selected = i;

                // this.apply_fx(body, 'selected');

                break;
            }
            else
            {
                // this.apply_fx(body, 'unselected');

                let vec = new cc.Vec2(touch_pos.x - pos.x, touch_pos.y - pos.y);
                let vec_dist_sq = vec.magSqr();

                if(!(nearest_dist_sq > 0) || (vec_dist_sq <= new_dist_sq && vec_dist_sq < nearest_dist_sq))
                {
                    nearest_dist_sq = vec_dist_sq;
                    this._current_selected = i;
                }
            }
        }

        if(this._current_selected >= 0)
        {
            let body = this._bodies[this._current_selected];

            if(this._bomb_list.has(body))
            {
                //this.explode_preview(body);
            }
            else
            {
                this.connect(body);
            }

            soundManager.instance.playSound('click');
        }
    }

    touch_move(touch_pos)
    {
        // cc.log('touch_move ' + touch_pos.x + ', ' + touch_pos.y);

        this._touching = true;
        this._last_touch_pos = touch_pos;

        if(this._current_selected < 0)
        {
            return;
        }

        let target_index = -1;

        let nearest_dist_sq = 0;

        let this_body = this._bodies[this._current_selected];
        //let food_script = this_body.getComponent('Food');
        let pos0 = this_body.getComponent(cc.RigidBody).getWorldCenter();
        let vec = new cc.Vec2(touch_pos.x - pos0.x, touch_pos.y - pos0.y);
        let base_dist = vec.magSqr();//*0.7;

        for(let i = 0; i < this._bodies.length; ++i)
        {
            if(i == this._current_selected)
            {
                continue;
            }

            let that_body = this._bodies[i];
            let that_food_script = that_body.getComponent('Food');
            let bomb_script = that_body.getComponent('Bomb')

            if (this.current_connect_food_id == that_food_script.id ||
                this.current_connect_food_id == 0 ||
                bomb_script.level > 0) 
            {
                let pos = that_body.getComponent(cc.RigidBody).getWorldCenter();

                if (this.is_touched(that_body,touch_pos)) {
                    target_index = i;
                    break;
                }
                
                if (!this.is_touched(this_body, touch_pos)) {

                    let vec = new cc.Vec2(touch_pos.x - pos.x, touch_pos.y - pos.y);
                    let d0 = vec.magSqr();

                    
                    if(!(nearest_dist_sq > 0) || (d0 < nearest_dist_sq))
                    {
                        if (d0 < base_dist) {
                            nearest_dist_sq = d0;
                            target_index = i;
                        }
                    }
                }
            }
        }

        if(target_index >= 0)
        {
            let that_body = this._bodies[target_index];

            this._explode_preview_list.clear();

            let length = this._selected_stack.length;

            if(length > 0 && target_index == this._selected_stack[length - 1])
            {
                this._current_selected = target_index;
                this._all_selected.delete(target_index);
                this._selected_stack.pop();

                return;
            }

            if(this._all_selected.has(target_index))
            {
                return;
            }

            if(this.is_connected(that_body, this_body))
            {
                let that_food_script = that_body.getComponent('Food');
                let bomb_script = that_body.getComponent('Bomb')
                if (bomb_script.level == 0) {
                    this.current_connect_food_id = that_food_script.id;
                }                

                this._selected_stack.push(this._current_selected);
                this._all_selected.add(this._current_selected);
                this._current_selected = target_index;

                soundManager.instance.playSound('click');
            }
        }
    }

    touch_end(touch_pos)
    {
        // cc.log('touch_end ' + touch_pos.x + ', ' + touch_pos.y);

        this._touching = false;
        this._last_touch_pos = touch_pos;
        let all_connected_num = 0;
        let bombExploded = false;

        if(this._current_selected >= 0)
        {
            let body = this._bodies[this._current_selected];
            let food_script = body.getComponent('Food');
            let bomb_script = body.getComponent('Bomb');

            //if(this.is_rough_touched(body, touch_pos))
            {
                let min_remove_num = this._config.get('min_remove_num');

                all_connected_num = this.get_connected_num();

                if(all_connected_num >= min_remove_num)
                {
                    let bomb_script = body.getComponent('Bomb');
                    if(bomb_script.level > 0)
                    {
                        //本来就是炸弹
                        this.explode(body,false);
                        bombExploded = true;
                    }

                    if(all_connected_num >= this._min_connected_num)
                    {
                        if(all_connected_num > this._max_connected_num)
                        {
                            all_connected_num = this._max_connected_num;
                        }

                        let bomb_script = body.getComponent('Bomb');
                        bomb_script.init(all_connected_num);

                        if(bomb_script.level > 0)
                        {
                            this._bomb_list.add(body);
                            soundManager.instance.playSound('pop_bomb');
                        }
                    }
                    else
                    {
                        this._selected_stack.push(this._current_selected);
                        this._all_selected.add(this._current_selected);

                        soundManager.instance.playSound('normalpop');
                    }

                    {
                        this._all_selected.forEach((index) =>
                        {
                            let select_body = this._bodies[index];
                            let select_bomb_script = select_body.getComponent('Bomb');
    
                            if(select_bomb_script.level > 0)
                            {
                                this.explode(select_body);
                                bombExploded = true;
                            }
                        });
                    }


                    this._all_selected.forEach((index) =>
                    {
                        let select_body = this._bodies[index];
                        let select_bomb_script = select_body.getComponent('Bomb');

                        if(0 == select_bomb_script.level
                            && (!this._explode_list.has(select_body)))
                        {
                            this.apply_fx(select_body, 'remove');
                        }
                    });

                    let body_map = new Map();
                    body_map.set(0, 1);
                    body_map.set(this.find_base_id(), all_connected_num);
                    body_map.set('eventSource','hand');
                    this.send_event('feed_food', body_map, body);

                    this.remove_idx_array(this._selected_stack, true);
                }
                else if(this._bomb_list.has(body))
                {
                    let body_map = new Map();
                    body_map.set(0, 1);
                    body_map.set(bomb_script.id, 1);
                    body_map.set('eventSource','hand');
                    this.send_event('feed_food', body_map);

                    this._exploding = true;
                    this._selected_bomb = body;
                    this.explode(body);
                    bombExploded = true;
                    this._exploding = false;
                }
            }
        }

        this._current_selected = -1;
        this._selected_stack.splice(0, this._selected_stack.length);
        this._all_selected.clear();
        this._connected_list.clear();
        this._explode_preview_list.clear();

        if (bombExploded == false) {

            let gen_num = this._config.get('max_body_num') - this._bodies.length;

            if(gen_num > 0)
            {
                let minus = gen_num - this._config.get('auto_match_num');
                if (minus >= 0) {

                    this.auto_connect_number_limit = all_connected_num;
                    this.generate(gen_num, this.auto_connect_number_limit > 0);
                }
                else {
                    this.auto_connect_number_limit = 0;
                    this.generate(gen_num, false);
                }
            }
        }else{
            this.auto_connect_number_limit = -99;
        }

        if (game.instance.isInGuideMode() == false) {
            this.schedule(this._timeout_callback, this._config.get('hint_seconds'));
        }
    }

    hint()
    {
        var game_level = this.canvas.getChildByName('script').getComponent('gameLevel');
        var plate_area = game_level.plateAreaNode;
        var npc_num = game_level.npcNumber;
        var npc_data = game_level.npcDatas;

        let min_remove_num = this._config.get('min_remove_num');

        let BreakException = {};

        this._hint_list.clear();
        this._hint_list_array = [];

        var isPlaySound : any = false;

        try
        {
            this._bodies.forEach((body) =>
            {
                this._hint_list.clear();
                this._hint_list_array = [];

                let food_script = body.getComponent('Food');
                let bomb_script = body.getComponent('Bomb');
                let soup_script = body.getComponent('Soup');

                if(0 == bomb_script.level && 0 == soup_script.id)
                {
                    let npc_want = false;

                    for(let i = 0; i < npc_num; ++i)
                    {
                        let want = npc_data[i].wantEat;

                        if(want == food_script.id)
                        {
                            npc_want = true;
                            break;
                        }
                    }

                    if(npc_want)
                    {
                        let bomb_num = 0;

                        let open_list = new ds.Queue();
        
                        open_list.push(body);
                
                        while(!open_list.empty())
                        {
                            let this_body = open_list.pop();
                
                            for(let i = 0; i < this._bodies.length; ++i)
                            {
                                let that_body = this._bodies[i];
                
                                if(that_body != this_body &&
                                    !this._hint_list.has(that_body) &&
                                    this.is_connected(that_body, this_body))
                                {
                                    open_list.push(that_body);
                                    this._hint_list.add(that_body);
                                    this._hint_list_array.push(that_body);

                                    isPlaySound = true;
    
                                    let that_bomb = that_body.getComponent('Bomb');
    
                                    if(that_bomb.level > 0)
                                    {
                                        ++bomb_num;
                                    }
                                }
                            }
                        }
    
                        if(this._hint_list.size - bomb_num >= min_remove_num)
                        {
                            throw BreakException;
                        }
                    }
                }
            });
        }
        catch(e)
        {
            if(isPlaySound == true)
            {
                soundManager.instance.playSound('auto_init');
            }

            return;
        }

        this._hint_list.clear();
        this._hint_list_array = [];

        try
        {
            this._bodies.forEach((body) =>
            {
                this._hint_list.clear();
                this._hint_list_array = [];

                let food_script = body.getComponent('Food');
                let bomb_script = body.getComponent('Bomb');
                let soup_script = body.getComponent('Soup');

                if(0 == bomb_script.level && 0 == soup_script.id)
                {
                    let bomb_num = 0;

                    let open_list = new ds.Queue();
    
                    open_list.push(body);
            
                    while(!open_list.empty())
                    {
                        let this_body = open_list.pop();
            
                        for(let i = 0; i < this._bodies.length; ++i)
                        {
                            let that_body = this._bodies[i];
            
                            if(that_body != this_body &&
                                !this._hint_list.has(that_body) &&
                                this.is_connected(that_body, this_body))
                            {
                                open_list.push(that_body);
                                this._hint_list.add(that_body);
                                this._hint_list_array.push(that_body);
                                isPlaySound = true;

                                let that_bomb = that_body.getComponent('Bomb');

                                if(that_bomb.level > 0)
                                {
                                    ++bomb_num;
                                }
                            }
                        }
                    }

                    if(this._hint_list.size - bomb_num >= min_remove_num)
                    {
                        throw BreakException;
                    }
                }
            });
        }
        catch(e)
        {
            if(isPlaySound == true)
            {
                soundManager.instance.playSound('auto_init');
            }

            return;
        }
    }

    checkHasVaildLinks()
    {
        let check_list_set: Set<any> = new Set();
        let bdLen = this._bodies.length;

        for (let i =0; i <bdLen; i++)
        {
            let body = this._bodies[i];
            check_list_set.clear();

            let bomb_script = body.getComponent('Bomb');
            let soup_script = body.getComponent('Soup');

            let rigid_body = body.getComponent(cc.RigidBody);
            let velocity = rigid_body.linearVelocity;

            if(velocity.magSqr() > 50) {
                return true;
            }

            if(0 == bomb_script.level && 0 == soup_script.id)
            {
                let open_list = new ds.Queue();

                open_list.push(body);
        
                while(!open_list.empty())
                {
                    let this_body = open_list.pop();
        
                    for(let i = 0; i < this._bodies.length; ++i)
                    {
                        let that_body = this._bodies[i];
        
                        if(that_body != this_body &&
                            !check_list_set.has(that_body) &&
                            this.is_connected(that_body, this_body))
                        {
                            open_list.push(that_body);
                            check_list_set.add(that_body);
                        }
                    }
                }
            }

            if (check_list_set.size >= 3) {
                return true;
            }
        }

        return false;
    }

    refreshPot()
    {
        let win_size = cc.winSize;

        let left_pos = this.pot_left.convertToWorldSpace(new cc.Vec2(0, 0));
        let right_pos = this.pot_right.convertToWorldSpace(new cc.Vec2(0, 0));

        let low_y = win_size.height * 0.6;
        let high_y = win_size.height * 1.2;

        let num = this._bodies.length;
        for(let i = 0; i < num; ++i)
        {            
            let x = tools.rand_real(left_pos.x, right_pos.x);
            let y = tools.rand_real(low_y, high_y);

            let body = this._bodies[i];

            let food_script = body.getComponent('Food');
            if (!!food_script) {
                food_script.apply_fx('default');
            }

            let pos = new cc.Vec2(x, y);
            pos = this.food_node.convertToNodeSpace(pos);

            body.setPosition(pos);
            body.angle = Math.random()*360.0;

            let rigid_body = body.getComponent(cc.RigidBody);

            pos = rigid_body.getWorldCenter();

            rigid_body.angularVelocity = tools.rand_real(-10, 10);
            rigid_body.linearVelocity = new cc.Vec2(0, tools.rand_real(-500, -650));
        }

        ui.instance.showAlertTip('没有可以吃的菜了，搅拌一下。。。');
    }

    debug()
    {
        let curr_body = null;

        if(this._current_selected >= 0)
        {
            curr_body = this._bodies[this._current_selected];
        }

        let match_num = this._config.get('auto_match_num');
        let sel_len = this._selected_stack.length;
       
        let bomo_preview = new Set();
        let bomobIndex = 0;

        for (let i =0;i<sel_len;i++) {
            let body_cur = this._bodies[this._selected_stack[i]];
            if(!this._bomb_list.has(body_cur))
            {
                bomobIndex ++;
            }
        }    
        
        if (!!curr_body) {
            if(!this._bomb_list.has(curr_body)) {
                bomobIndex ++;
            }
        }

        let bomoSelect = 0;
        for(let i =0; i < this.bomb_config_connect_array.length;i++) {
            
            if (this.bomb_config_connect_array[i] <= bomobIndex) {
                bomoSelect = this.bomb_config_connect_array[i];
            }
        }

        if (bomoSelect > 0) {

            if (sel_len == bomoSelect - 1 ||
                sel_len >= bomoSelect) {

                if (!!curr_body) {
                    let bomb_script = curr_body.getComponent('Bomb');
                    if (bomb_script.preview(bomoSelect)) 
                    {
                        bomo_preview.add(curr_body);
                    }
                }
            }
        }


        this._bodies.forEach((value) =>
        {
            let body = value;
            
            if (bomo_preview.has(body) == false) {

                let flag = false;

                if (sel_len >= match_num)
                {
                    let body2 = this._bodies[this._selected_stack[match_num-1]];
                    if (body == body2) {
                        this.apply_fx(body,'autoExplore', {tim:0});
                        flag = true;
                    }
                }
                else if (sel_len == match_num -1)
                {
                    if (body == curr_body)
                    {
                        this.apply_fx(body,'autoExplore', {tim:0});
                        flag = true;
                    }
                }

                if (flag == false) {
                    

                    if(this._bomb_list.has(body))
                    {
                        this.apply_fx(body, 'bomb');
                    }
                    else if(this._auto_list.has(body))
                    {
                        //this.apply_fx(body, 'auto');
                    }
                    else if(this._explode_preview_list.has(body))
                    {
                        this.apply_fx(body, 'explode_preview');
                    }
                    else if(this._connected_list.has(body) || this._hint_list.has(body))
                    {
                        // if(body != curr_body)
                        {
                            if (curr_body == body)
                            {
                                this.apply_fx(body, 'touchingConnected');
                            }else{
                                this.apply_fx(body, 'connected');
                            }
                        }
                        // else
                        // {
                        //     this.apply_fx(body, 'big_connected');
                        // }
                    }
                    else
                    {
                        if(this._connected_list.size > 0 || this._hint_list.size > 0)
                        {
                            this.apply_fx(body, 'unconnected');
                        }
                        else
                        {
                            this.apply_fx(body, 'default');
                        }
                    }
                }
            }
        });

        if (this._current_selected_flag != this._selected_stack.length) {

            let lineBoneScript = this.debug_node.getComponent('lineBone');
            this._current_selected_flag = this._selected_stack.length;

            if(!(this._selected_stack.length > 0))
            {
                lineBoneScript.clearNodes();
                return;
            }

            let pointsArray = [];
            for(let i = 0; i < this._selected_stack.length; ++i)
            {
                let body = this._bodies[this._selected_stack[i]];
                let pos = body.getComponent(cc.RigidBody).getWorldCenter();

                let x = pos.x - this.canvas.width * 0.5
                let y = pos.y - this.canvas.height * 0.5

                pointsArray[pointsArray.length] = cc.v2(x,y);
            }

            if (this._current_selected >= 0) {
                let body = this._bodies[this._current_selected];
                let pos = body.getComponent(cc.RigidBody).getWorldCenter();
                let x = pos.x - this.canvas.width * 0.5;
                let y = pos.y - this.canvas.height * 0.5;

                pointsArray[pointsArray.length] = cc.v2(x,y);
            }

            lineBoneScript.setPoints(pointsArray);
        }
    }

    is_body_valid(body)
    {
        this._bodies.forEach((value) =>
        {
            if(body == value)
            {
                return true;
            }
        })

        return false;
    }

    delayUpdate(tim)
    {
        this.delayTimer = 0;
        this.delayTime = tim;
        this.delayTimerStart = true;
    }

    update(dt)
    {
        if (this.stageFinished){
            return;
        }

        if (this.delayTimerStart) {
            this.delayTimer += dt;
            if (this.delayTimer > this.delayTime) {
                this.delayTimer = 0;
                this.delayTime = 0;
                this.delayTimerStart = false;
            }
            else {
                return;
            }
        }

        if (this._all_selected.size == 0 &&
             this._current_selected == -1) {

            let ret0 = this.auto_connect();
            if (ret0.delayTime > 0) {
                this.delayUpdate(ret0.delayTime + 0.25);
                return;
            }
    
            let ret1 = this.bomb_exploding();
            if (ret1.delayTime > 0) {
                this.delayUpdate(ret1.delayTime + 0.25);
                return;
            }
    
            if (ret0.sendFinish == true &&
                ret1.sendFinish == true) 
            {
                this.send_event('stage_finish_check', null);
            }
        }


        if(this._auto_delete_list.size > 0)
        {
            this.remove_set(this._auto_delete_list, false);
            this._auto_delete_list.clear();
        }

        let gen_num = this._config.get('max_body_num') - this._bodies.length

        if(gen_num > 0)
        {
            if(gen_num == this._config.get('max_body_num'))
            {
                this.generate(gen_num, false);

                if (game.instance.isInGuideMode() == false) {
                    this.schedule(this._timeout_callback, this._config.get('hint_seconds'));
                }
            }
            else
            {
                //this.generate(gen_num, true);
                
                let minus = gen_num - this._config.get('auto_match_num');
                if (minus >= 0) {

                    if (this.auto_connect_number_limit == -99) {
                        //表示跟炸弹一起的，延迟算
                        this.auto_connect_number_limit = gen_num;
                        this.generate(gen_num, true);
                    }
                    else {
                        let config = configLoader.instance.getBaseConfig();
                        this.auto_connect_number_limit -= config.DecayNum;
                        if (this.auto_connect_number_limit < 0) {
                            this.auto_connect_number_limit = 0;
                        }

                        this.generate(gen_num, this.auto_connect_number_limit > 0);
                    }
                }
                else {
                    this.generate(gen_num, false);
                }                
            }
        }

        this.debug();

        if (this._touching != true) {
            
            this.timeToCheckVaild += dt;
            if (this.timeToCheckVaild > 2.0) {

                let hasVaild = this.checkHasVaildLinks();
                if (hasVaild == false) {
                    cc.log('no vaild link ,need refresh !');

                    this.refreshPot();
                    this.delayUpdate(2.0);
                }
                else {
                    cc.log('link vaild!');
                }

                this.timeToCheckVaild = 0;
            }
        }
    }

    apply_fx_list(body, fx_list)
    {
        if(body != null)
        {
            let script = body.getComponent('Food');
            script.apply_fx_list(fx_list);
        }
        else
        {

        }
    }

    apply_fx(body, fx, param = null)
    {
        if(body != null)
        {
            let script = body.getComponent('Food');
            script.apply_fx(fx, param);
        }
        else
        {

        }
    }

    is_nearby(this_body, that_body)
    {
        let this_pos = this_body.getComponent(cc.RigidBody).getWorldCenter();
        let that_pos = that_body.getComponent(cc.RigidBody).getWorldCenter();

        let this_radius = this_body.getComponent(cc.PhysicsCircleCollider).radius;
        let that_radius = that_body.getComponent(cc.PhysicsCircleCollider).radius;

        let vec = new cc.Vec2(this_pos.x - that_pos.x, this_pos.y - that_pos.y);
        let dist = this_radius + that_radius + this._config.get('max_gap');

        return vec.magSqr() <= dist * dist;
    }

    gen_soup_pack(soup_id)
    {
        return;

        cc.log('gen_soup_pack: ' + soup_id);

        let soup_config = configLoader.instance.getPieceConfig(soup_id);
        cc.log('s: ' + soup_config.Para.s);

        let BreakException = {};

        try
        {
            this._bodies.forEach((value) =>
            {
                let body = value;
                let bomb_script = body.getComponent('Bomb');
                let soup_script = body.getComponent('Soup');
    
                if(0 == bomb_script.level && 0 == soup_script.id)
                {
                    soup_script.id = soup_id;
    
                    this.apply_fx(body, 'soup');

                    throw BreakException;
                }
            });
        }
        catch(e)
        {

        }
    }

    is_connected(this_body, that_body)
    {
        let ret = false;

        let this_soup_script = this_body.getComponent('Soup');
        let that_soup_script = that_body.getComponent('Soup');

        if(this_soup_script.id > 0 || that_soup_script.id > 0)
        {
            return ret;
        }

        let base_id = this.find_base_id();

        let is_this_bomb = this._bomb_list.has(this_body);
        let is_that_bomb = this._bomb_list.has(that_body);

        if(this.is_nearby(this_body, that_body))
        {
            let this_script = this_body.getComponent('Food');
            let that_script = that_body.getComponent('Food');

            let same_id = (this_script.id == that_script.id);

            if(base_id > 0)
            {
                if(is_this_bomb && is_that_bomb)
                {
                    ret = true;
                }
                else if(is_this_bomb)
                {
                    ret = (base_id == that_script.id);
                }
                else if(is_that_bomb)
                {
                    ret = (base_id == this_script.id);
                }
                else
                {
                    ret = (same_id && base_id == this_script.id);
                }
            }
            else
            {
                if(is_this_bomb || is_that_bomb)
                {
                    ret = true;
                }
                else
                {
                    ret = same_id;
                }
            }
        }

        return ret;
    }

    is_touched(body, touch_pos)
    {
        let pos = body.getComponent(cc.RigidBody).getWorldCenter();
        let radius = body.getComponent(cc.PhysicsCircleCollider).radius;

        let vec = new cc.Vec2(touch_pos.x - pos.x, touch_pos.y - pos.y);

        return vec.magSqr() <= radius * radius;
    }

    is_rough_touched(body, touch_pos)
    {
        let pos = body.getComponent(cc.RigidBody).getWorldCenter();
        let radius = body.getComponent(cc.PhysicsCircleCollider).radius + this._config.get('extra_dist') * 4;

        let vec = new cc.Vec2(touch_pos.x - pos.x, touch_pos.y - pos.y);

        return vec.magSqr() <= radius * radius;
    }

    getBombListInGuide()
    {
        let ret = [];

        this._bomb_list.forEach((value) =>
        {
            ret[ret.length] = value;
        });

        return ret;
    }

    get3LinkkedInGuide()
    {
        this.hint();
        return this._hint_list_array;
    }

    generate(num, auto_remove)
    {       
        if (this.stageFinished){
            return;
        }

        let win_size = cc.winSize;

        let left_pos = this.pot_left.convertToWorldSpace(new cc.Vec2(0, 0));
        let right_pos = this.pot_right.convertToWorldSpace(new cc.Vec2(0, 0));

        let low_y = win_size.height * 0.6;
        let high_y = win_size.height * 1.2;

        //let outPutTest = {};
        let isGuide = game.instance.isInGuideMode();

        for(let i = 0; i < num; ++i)
        {
            //outPutTest[i] = '';

            let id = tools.rand_int(20001, 20005);
            if (num == 40 && isGuide == true) {
                id = Math.floor(i/10 + 20001);
            }
            
            let x = tools.rand_real(left_pos.x, right_pos.x);
            let y = tools.rand_real(low_y, high_y);

            let body = cc.instantiate(this.food_prefab);
            let script = body.getComponent('Food');

            script.effectLayer = this.effectLayer;
            script.crashEffectLayer = this.crashEffectLayer;
            script.id = id;
            script.canvas = this.canvas;
            script.coin_icon = this.coin_icon;
            script.soup_icon = this.soup_icon;
            script.init();

            body.parent = this.food_node;

            let pos = new cc.Vec2(x, y);
            pos = this.food_node.convertToNodeSpace(pos);

            //outPutTest[i] = outPutTest[i] + pos.x + ',';
            //outPutTest[i] = outPutTest[i] + pos.y + ',';

            body.setPosition(pos);
            body.angle = Math.random()*360.0;

            //outPutTest[i] = outPutTest[i] + body.angle + ',';

            let rigid_body = body.getComponent(cc.RigidBody);

            pos = rigid_body.getWorldCenter();

            rigid_body.angularVelocity = tools.rand_real(-10, 10);
            rigid_body.linearVelocity = new cc.Vec2(0, tools.rand_real(-500, -650));

            //outPutTest[i] = outPutTest[i] + rigid_body.angularVelocity + ',';
            //outPutTest[i] = outPutTest[i] + rigid_body.linearVelocity;

            //rigid_body.applyForceToCenter(new cc.Vec2(0, tools.rand_real(-500, -1000)), true);
            //rigid_body.applyLinearImpulse(new cc.Vec2(0, tools.rand_real(-500, -1000)), pos, true);

            this._bodies.push(body);

            if(auto_remove)
            {
                if (i < this.auto_connect_number_limit) {

                    this.apply_fx(body, 'autoExplore', {tim : 0,scale : false});
                    this._auto_list.add(body);
                }
            }
        }

        //cc.log('------------------ GENERAL FINISHED ---------------------');
        //cc.log(outPutTest);
    }

    connect(body)
    {
        this._connected_list.clear();

        let open_list = new ds.Queue();

        open_list.push(body);

        while(!open_list.empty())
        {
            let this_body = open_list.pop();

            for(let i = 0; i < this._bodies.length; ++i)
            {
                let that_body = this._bodies[i];

                if(that_body != this_body &&
                    !this._connected_list.has(that_body) &&
                    this.is_connected(that_body, this_body))
                {
                    open_list.push(that_body);
                    this._connected_list.add(that_body);
                }
            }
        }
    }

    stage_finished()
    {
        this.stageFinished = true;
    }

    bomb_exploding()
    {
        let delayTimeRet = 0;
        let needSendFinish = false;

        if (this._explode_list_to_explore.size > 0) {

            let body_map = new Map();
            this._explode_list_to_explore.forEach((value)=>
            {
                let body = value;
                let food_script = body.getComponent('Food');
                let bomb_script = body.getComponent('Bomb');

                if(0 == bomb_script.level)
                {
                    if(null == body_map.get(food_script.id))
                    {
                        body_map.set(food_script.id, 1);
                    }
                    else
                    {
                        body_map.set(food_script.id, body_map.get(food_script.id) + 1);
                    }
                }
            });

            body_map.set('eventSource','bomb');
            this.send_event('feed_food', body_map, this._selected_bomb);

            this.remove_set(this._explode_list_to_explore, false);
            this._explode_list_to_explore.clear();
        }

        let explodSize = this._explode_list.size;

        if(!this._exploding)
        {
            this._exploding = true;

            let flag = false;

            if(this._explode_list.size > 0)
            {
                flag = true;

                let maxDist = 0.1;
                let minDist = 10000;
                this._explode_list.forEach((value)=>
                {
                    let food_script = value.getComponent('Food');
                    let dist = food_script.get_explode_dist();
                    if (maxDist < dist) {
                        maxDist = dist;
                    }

                    if (minDist > dist){
                        minDist = dist;
                    }
                });

                let dd = maxDist - minDist;
                if (dd == 0) {
                    dd = 1;
                }

                let timeScale = 0.35/dd;

                this._explode_list.forEach((value)=>
                {
                    let body = value;

                    let food_script = body.getComponent('Food');
                    let dist = food_script.get_explode_dist();
                    let tim = (dist - minDist) * timeScale;
                    if (delayTimeRet < tim) {
                        delayTimeRet = tim;
                    }

                    cc.log('delay Time =' + tim);
                    food_script.apply_fx('remove_by_explode', {time : tim});

                    this._explode_list_to_explore.add(body);
                });

                this._selected_bomb = null;
                this._explode_list.clear();
            }

            if(0 == this._explode_list_to_explore.size && 0 == explodSize)
            {
                needSendFinish = true;
            }

            this._exploding = false;
            this._auto_exploding = false;
        }

        let retStruct = {
            delayTime : delayTimeRet,
            sendFinish : needSendFinish,
        };

        return retStruct;
    }

    auto_connect()
    {
        if (this.auto_explore_connected_list.size > 0) {

            let soundName = 'normalpop';
            this.auto_explore_connected_list.forEach((value) =>
            {
                let select_body = value;
                let select_bomb_script = select_body.getComponent('Bomb');

                if(0 == select_bomb_script.level)
                {
                    this.apply_fx(select_body, 'remove');
                }else{
                    soundName = 'pop_bomb';
                }

            });

            this.comboIndex ++;

            let body_map = new Map();
            body_map.set('level', this.comboIndex);

            this.send_event('stage_playCombo', body_map);

            soundManager.instance.playSound(soundName);

            let soundIndex = this.comboIndex;
            if (soundIndex > 4) {
                soundIndex = 4;
            }
            soundManager.instance.playSound('combo' + soundIndex);
            this.auto_explore_connected_list.clear();

            if(this.auto_explore_map.size > 0)
            {
                this.send_event('feed_food', this.auto_explore_map);
                this.auto_explore_map.clear();
            }
            
            this.unschedule(this._timeout_callback);

            this._hint_list.forEach((hint_body) =>
            {
                this.apply_fx(hint_body, 'default');
            });
    
            this._hint_list.clear();

            this.schedule(this._timeout_callback, this._config.get('hint_seconds'));

            return 0;
        }
        

        let delayTimeReturn = 0;

        let min_remove_num = this._config.get('min_remove_num');

        let min_connected_num = 0;
        let max_connected_num = 0;
        let BreakException = {};

        let first_body = null;
        this.auto_explore_map.clear();

        let auto_list_size = this._auto_list.size;

        try
        {
            this._auto_list.forEach((auto_body) =>
            {
                let food_script = auto_body.getComponent('Food');
                let bomb_script = auto_body.getComponent('Bomb');
    
                let rigid_body = auto_body.getComponent(cc.RigidBody);
                let velocity = rigid_body.linearVelocity;
    
                if(velocity.magSqr() < 75)
                {
                    let connect_list = this._auto_connect_list.get(auto_body);
    
                    if(null == connect_list)
                    {
                        this._auto_connect_list.set(auto_body, new Set());
                        connect_list = this._auto_connect_list.get(auto_body);
    
                        connect_list.add(auto_body);
                    }
    
                    let open_list = new ds.Queue();
    
                    open_list.push(auto_body);
    
                    while(!open_list.empty())
                    {
                        let this_body = open_list.pop();
    
                        for(let i = 0; i < this._bodies.length; ++i)
                        {                              
                            let that_body = this._bodies[i];
            
                            if(!this._bomb_list.has(that_body) &&
                            !connect_list.has(that_body) && this.is_connected(that_body, this_body))
                            {
                                if (!this._auto_list.has(that_body)) {
                                    open_list.push(that_body);
                                    connect_list.add(that_body);
                                }
                                else {

                                    if (that_body != auto_body) {

                                        open_list.push(that_body);
                                        connect_list.add(that_body);

                                        this._auto_list.delete(that_body);
                                    }
                                }
                            }
                        }
                    }
    
                    let all_connected_num = 0;
    
                    connect_list.forEach((value) =>
                    {
                        let body = value;
    
                        if(!this._bomb_list.has(body))
                        {
                            ++all_connected_num;
                        }
                    });
    
                    if(all_connected_num >= min_remove_num)
                    {
                        if(all_connected_num >= this._min_connected_num)
                        {
                            if(all_connected_num > this._max_connected_num)
                            {
                                all_connected_num = this._max_connected_num;
                            }

                            /*
                            bomb_script.init(all_connected_num);
                            if(bomb_script.level > 0)
                            {
                                connect_list.delete(auto_body);
    
                                this._bomb_list.add(auto_body);

                                //soundManager.instance.playSound('pop_bomb');
                            }
                            */
                        }
                        //else
                        //{
                            //soundManager.instance.playSound('normalpop');
                        //}
    
                        let i = 0;
                        connect_list.forEach((value) =>
                        {
                            let select_body = value;
                            let select_bomb_script = select_body.getComponent('Bomb');

                            if(0 == select_bomb_script.level && !this._auto_delete_list.has(select_body))
                            {
                                this._auto_delete_list.add(select_body);
                                this.auto_explore_connected_list.add(select_body);
                                 
                                let tm = (i + 0.5)*0.12;
                                if (delayTimeReturn < tm) {
                                    delayTimeReturn = tm;
                                }

                                if (select_body != auto_body) {
                                    this.apply_fx(select_body, 'autoExplore', {tim : tm,scale:true,sound:true});
                                    i ++;
                                }
                            }
                        });

                        this._auto_delete_list.add(auto_body);
                        this.auto_explore_connected_list.add(auto_body);

                        if(null == first_body)
                        {
                            first_body = auto_body;
                        }

                        if(null == this.auto_explore_map.get(food_script.id))
                        {
                            this.auto_explore_map.set(food_script.id, all_connected_num);
                        }
                        else
                        {
                            this.auto_explore_map.set(food_script.id, this.auto_explore_map.get(food_script.id) + all_connected_num);
                        }
                    }

                    this._auto_list.delete(auto_body);

                    //if(0 == this._auto_list.size)
                    //{
                        //this._exploding = true;
                        //this._auto_exploding = true;
                        //this.auto_explode();
                        //this._exploding = false;
                    //}
    
                    // throw BreakException;
                }
            });
        }
        catch(e)
        {

        }

        let needSendFinish = false;

        if(0 == this.auto_explore_connected_list.size && 0 == auto_list_size)
        {
            needSendFinish = true;
        }

        let retStruct = {
            delayTime : delayTimeReturn,
            sendFinish : needSendFinish,
        };
        
        return retStruct;
    }

    find_base_id()
    {
        let ret = 0;

        for(let i = 0; i < this._selected_stack.length; ++i)
        {
            let body = this._bodies[this._selected_stack[i]];
            let script = body.getComponent('Food');

            if(!this._bomb_list.has(body))
            {
                ret = script.id;
                break;
            }
        }

        if(0 == ret)
        {
            if(this._current_selected >= 0)
            {
                let body = this._bodies[this._current_selected];
                let food_script = body.getComponent('Food');
                let soup_script = body.getComponent('Soup');

                if(!this._bomb_list.has(body) && 0 == soup_script.id)
                {
                    ret = food_script.id;
                }
            }
        }

        return ret;
    }

    get_connected_num()
    {
        let ret = 0;

        if(this._current_selected >= 0)
        {
            let body = this._bodies[this._current_selected];

            //if(!this._bomb_list.has(body))
            {
                ++ret;
            }

            this._selected_stack.forEach((value) =>
            {
                let body = this._bodies[value];

                //if(!this._bomb_list.has(body))
                {
                    ++ret;
                }
            })
        }

        return ret;
    }

    rand_force()
    {

    }

    auto_remove()
    {
        let min_remove_num = this._config.get('min_remove_num');

        // let min_connected_num = 0;
        // let max_connected_num = 0;

        // let index = 0;

        // this._bomb_level.forEach((value, key) =>
        // {
        //     if(0 == index)
        //     {
        //         min_connected_num = value;
        //     }

        //     max_connected_num = value;

        //     ++index;
        // });

        this._auto_connect_list.forEach((value, key) =>
        {
            let body = key;
            let connect_list = value;

            let food_script = body.getComponent('Food');
            let bomb_script = body.getComponent('Bomb');

            let rigid_body = body.getComponent(cc.RigidBody);
            let velocity = rigid_body.linearVelocity;

            if(velocity.magSqr() < 1)
            {
                let all_connected_num = connect_list.size;

                if(all_connected_num >= min_remove_num)
                {
                    if(all_connected_num >= this._min_connected_num)
                    {
                        if(all_connected_num > this._max_connected_num)
                        {
                            all_connected_num = this._max_connected_num;
                        }
    
                        bomb_script.init(all_connected_num);

                        // let bomb_level = 0;
    
                        // this._bomb_level.forEach((value, key) =>
                        // {
                        //     if(all_connected_num <= value)
                        //     {
                        //         if(0 == bomb_level)
                        //         {
                        //             bomb_level = key;
                        //         }
                        //     }
                        // });
    
                        if(bomb_script.level > 0)
                        {
                            connect_list.delete(body);

                            // bomb_script.level = bomb_level;
                            this._bomb_list.add(body);
                        }
                    }

                    this.remove_set(connect_list, false);
                }

                this._auto_connect_list.delete(body);
            }
        })
    }

    explode(bomb, addToBomb = true)
    {
        if(this._explode_list.has(bomb))
        {
            return;
        }

        let bomb_script = bomb.getComponent('Bomb');
        let bomb_level = bomb_script.level;

        if(bomb_level > 0)
        {
            soundManager.instance.playSound('boom_30001');

            let bomb_pos = bomb.getComponent(cc.RigidBody).getWorldCenter();

            if (addToBomb == true) {
                this._explode_list.add(bomb);
            }

            this.apply_fx(bomb, 'explode');

            //soundManager.instance.playSound('combo' + bomb_level);

            this._bodies.forEach((value) =>
            {
                let body = value;

                if(!this._explode_list.has(body))
                {
                    let body_pos = body.getComponent(cc.RigidBody).getWorldCenter();
                    let body_radius = body.getComponent(cc.PhysicsCircleCollider).radius;

                    if(1 == bomb_level)
                    {
                        let dist = bomb_script.r + body_radius;

                        let vec: cc.Vec2 = new cc.Vec2(body_pos.x - bomb_pos.x, body_pos.y - bomb_pos.y);

                        if(vec.magSqr() <= dist * dist)
                        {
                            if(!this._bomb_list.has(body))
                            {
                                let food_script = body.getComponent('Food');
                                if (!!food_script) {

                                    let distacne = Math.hypot(vec.x,vec.y);
                                    food_script.set_explode_dist(distacne);
                                }

                                this._explode_list.add(body);
                            }
                            /*
                            else
                            {
                                this.explode(body);
                            }
                            */
                        }
                    }
                    else if(2 == bomb_level)
                    {
                        let bomb_2_height = bomb_script.h;
                        let bomb_2_width = bomb_script.w;

                        let min_x = bomb_pos.x - bomb_2_height;
                        let max_x = bomb_pos.x + bomb_2_height;
                        let min_y = bomb_pos.y - bomb_2_height;
                        let max_y = bomb_pos.y + bomb_2_height;

                        let dx = body_pos.x - bomb_pos.x
                        let dy = body_pos.y - bomb_pos.y

                        if(dx < 0)
                        {
                            dx = -dx;
                        }

                        if(dy < 0)
                        {
                            dy = -dy;
                        }

                        if((dx < bomb_2_width + body_radius && dy < bomb_2_height) ||
                            (dy < bomb_2_width + body_radius && dx < bomb_2_height))

                        // if((min_x >= body_pos.x - body_radius && min_x <= body_pos.x + body_radius) ||
                        //     (max_x >= body_pos.x - body_radius && max_x <= body_pos.x + body_radius) ||
                        //     (min_y >= body_pos.y - body_radius && min_y <= body_pos.y + body_radius) ||
                        //     (max_y >= body_pos.y - body_radius && max_y <= body_pos.y + body_radius))
                        {
                            if(!this._bomb_list.has(body))
                            {
                                let food_script = body.getComponent('Food');
                                if (!!food_script) {

                                    let distacne = Math.hypot(dx,dy);
                                    food_script.set_explode_dist(distacne);
                                }


                                this._explode_list.add(body);
                            }
                            /*
                            else
                            {
                                this.explode(body);
                            }
                            */
                        }
                    }
                }
            });
        }
    }

    explode_preview(bomb)
    {
        let bomb_script = bomb.getComponent('Bomb');
        let bomb_level = bomb_script.level;

        if(bomb_level > 0)
        {
            let bomb_pos = bomb.getComponent(cc.RigidBody).getWorldCenter();

            this._explode_preview_list.add(bomb);

            this._bodies.forEach((value) =>
            {
                let body = value;

                if(!this._explode_preview_list.has(body))
                {
                    let body_pos = body.getComponent(cc.RigidBody).getWorldCenter();
                    let body_radius = body.getComponent(cc.PhysicsCircleCollider).radius;

                    if(1 == bomb_level)
                    {
                        let dist = bomb_script.r + body_radius;

                        let vec: cc.Vec2 = new cc.Vec2(body_pos.x - bomb_pos.x, body_pos.y - bomb_pos.y);

                        if(vec.magSqr() <= dist * dist)
                        {
                            if(!this._bomb_list.has(body))
                            {
                                this._explode_preview_list.add(body);
                            }
                            else
                            {
                                this.explode_preview(body);
                            }
                        }
                    }
                    else if(2 == bomb_level)
                    {
                        let bomb_2_height = bomb_script.h;
                        let bomb_2_width = bomb_script.w;

                        let min_x = bomb_pos.x - bomb_2_height;
                        let max_x = bomb_pos.x + bomb_2_height;
                        let min_y = bomb_pos.y - bomb_2_height;
                        let max_y = bomb_pos.y + bomb_2_height;

                        let dx = body_pos.x - bomb_pos.x
                        let dy = body_pos.y - bomb_pos.y

                        if(dx < 0)
                        {
                            dx = -dx;
                        }

                        if(dy < 0)
                        {
                            dy = -dy;
                        }

                        if((dx < bomb_2_width + body_radius && dy < bomb_2_height) ||
                            (dy < bomb_2_width + body_radius && dx < bomb_2_height))

                        // if((min_x >= body_pos.x - body_radius && min_x <= body_pos.x + body_radius) ||
                        //     (max_x >= body_pos.x - body_radius && max_x <= body_pos.x + body_radius) ||
                        //     (min_y >= body_pos.y - body_radius && min_y <= body_pos.y + body_radius) ||
                        //     (max_y >= body_pos.y - body_radius && max_y <= body_pos.y + body_radius))
                        {
                            if(!this._bomb_list.has(body))
                            {
                                this._explode_preview_list.add(body);
                            }
                            else
                            {
                                this.explode_preview(body);
                            }
                        }
                    }
                }
            });
        }
    }

    /*
    auto_explode()
    {
        this._bomb_list.forEach((value) =>
        {
            let body = value;
            this.explode(body);
        });

        this._bomb_list.clear();

        let body_map = new Map();

        this._bodies.forEach((value) =>
        {
            let body = value;

            if(!this._explode_list.has(body))
            {
                let soup_script = body.getComponent('Soup');

                if(soup_script.id > 0)
                {
                    if(null == body_map.get(soup_script.id))
                    {
                        body_map.set(soup_script.id, 1);
                    }
                    else
                    {
                        body_map.set(soup_script.id, body_map.get(soup_script.id) + 1);
                    }

                    this._explode_list.add(body);
                }
            }
        });

        this.send_event('feed_food', body_map);
    }
    */

    remove_idx_array(remove_list: Array<any>, enable_explode)
    {
        if(0 == remove_list.length)
        {
            return;
        }

        remove_list.sort((n1, n2) : number => { return n1 - n2; });

        let deleted_num = 0

        for(let i = 0; i < remove_list.length; ++i)
        {
            let to_delete = remove_list[i] - deleted_num;

            let body = this._bodies[to_delete];

            this._auto_list.delete(body);

            if(enable_explode && (this._explode_list.has(body)) )
            {
            }
            else
            {
                this._bomb_list.delete(body);
                this._bodies.splice(to_delete, 1);

                //this.apply_fx(body, 'death');

                body.destroy();

                ++deleted_num;
            }
        }
    }

    remove_array(remove_list, enable_explode)
    {
        let idx_array = new Array();

        for(let i = 0; i < this._bodies.length; ++i)
        {
            remove_list.forEach((value) =>
            {
                if(value == this._bodies[i])
                {
                    idx_array.push(i);
                }
            });
        }

        this.remove_idx_array(idx_array, enable_explode);
    }

    remove_set(remove_list, enable_explode)
    {
        let idx_array = new Array();

        for(let i = 0; i < this._bodies.length; ++i)
        {
            remove_list.forEach((value) =>
            {
                if(value == this._bodies[i])
                {
                    idx_array.push(i);
                }
            });
        }

        this.remove_idx_array(idx_array, enable_explode);
    }
}
