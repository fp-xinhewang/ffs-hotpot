// Bomb.ts
// Created by zacklocx on 2018-12-23

import configLoader = require('../common/configLoader');
import utils = require('../common/utils');

const {ccclass, property} = cc._decorator;

@ccclass
export default class Bomb extends cc.Component
{
    @property
    level = 0;

    @property
    n = 0;

    @property
    r = 0;

    @property
    w = 0;

    @property
    h = 0;

    @property
    t = 0;

    @property
    id = 0;

    @property(cc.Node)
    bkLayer: cc.Node = null;

    @property(cc.Node)
    spriteLayer: cc.Node = null;
        
    @property(cc.Node)
    foodEffect: cc.Node = null;

    @property(cc.Node)
    bombNode : cc.Node = null;

    prewModeIndex : any = 0;
    prewModeTimer : any = 0;

    init(n)
    {
        this.prewModeIndex = 0;

        for(var i = 30001; i <= 30003; ++i)
        {
            let bomb_config = configLoader.instance.getPieceConfig(i);

            if(n >= bomb_config.Para.n1 && n <= bomb_config.Para.n2)
            {
                this.id = i;
                this.n = n;
                this.level = bomb_config.Type;

                if(1 == this.level)
                {
                    this.r = bomb_config.Para.r;
                }
                else if(2 == this.level)
                {
                    this.h = bomb_config.Para.h;
                    this.w = bomb_config.Para.w;
                }
                else if(3 == this.level)
                {
                    this.r = bomb_config.Para.r;
                    this.t = bomb_config.Para.t;
                }

                break;
            }
        }
    }

    hideOtherNodes()
    {
        this.bkLayer.active = false;
        this.spriteLayer.active = false;
        this.foodEffect.active = false;
    }

    showOtherNodes()
    {
        this.bkLayer.active = true;
        this.spriteLayer.active = true;
        this.foodEffect.active = true;
    }

    applyInterface()
    {
        this.removeInterface();
        
        let bomb = utils.createSpineNode('bomb/' + this.id + '/' + this.id, 'animation');        
        if(!!bomb)
        {
            bomb.setPosition(0, -40);
            bomb.scale = 1;

            this.bombNode.addChild(bomb);
            this.node.zIndex = 11;
            this.hideOtherNodes();
        }
    }

    removeInterface()
    {
        this.bombNode.removeAllChildren();
        this.showOtherNodes();
    }

    preview(n)
    {
        this.prewModeTimer = 0;

        if (this.prewModeIndex == n) {
            return;
        }

        for(var i = 30001; i <= 30003; ++i)
        {
            let bomb_config = configLoader.instance.getPieceConfig(i);

            if(n == bomb_config.Para.n1)
            {
                let level = bomb_config.Type;
                let bomb = utils.createSpineNode('bomb/' + i + '/' + i, 'animation');

                if(!!bomb)
                {
                    bomb.setPosition(0, -40);
                    bomb.scale = 1;
                    this.removeInterface();
                    this.bombNode.addChild(bomb);
                    this.node.zIndex = 10;
                    this.hideOtherNodes();
                    this.prewModeIndex = n;
                }else{
                    this.removeInterface();
                    this.prewModeIndex = n;
                }

                return true;
            }
            else{
                this.removeInterface();
                this.prewModeIndex = n;
            }
        }

        return false;
    }

    start()
    {
    }

    update(dt)
    {
        if (this.prewModeIndex != 0) {

            this.prewModeTimer += dt;
            if (this.prewModeTimer > 0.1) {
                this.node.zIndex = 0;
                this.removeInterface();
                this.prewModeIndex = 0;
            }
            return;
        }
    }
}
