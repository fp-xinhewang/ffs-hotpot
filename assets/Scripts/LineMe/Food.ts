// Food.ts
// Created by zacklocx on 2018-12-23

const {ccclass, property} = cc._decorator;

import utils = require('../common/utils');
import soundManager = require('../soundManager');
import game = require('../game');

@ccclass
export default class Food extends cc.Component
{
    @property
    id = 0;

    @property
    food_name = '';

    @property
    fx_name = '';

    @property(Set)
    fx_set: Set<any> = null;

    @property(cc.Node)
    canvas: cc.Node = null;

    @property(cc.Sprite)
    coin_icon: cc.Sprite = null;

    @property(cc.Sprite)
    soup_icon: cc.Sprite = null;


    @property(cc.Node)
    bkLayer: cc.Node = null;

    @property(cc.Node)
    spriteLayer: cc.Node = null;
        
    @property(cc.Node)
    foodEffect: cc.Node = null;

    effectLayer : cc.Node = null;
    crashEffectLayer : cc.Node = null;
    cur_fx : any = null;
    explode_dist : any = 0;
    world_pos : cc.Vec2 = cc.Vec2.ZERO;

    onLoad()
    {
        cc.director.getPhysicsManager().enabled = true;

        this.cur_fx = null;
    }

    start()
    {
    }

    set_explode_dist(val)
    {
        this.explode_dist = val;
    }

    get_explode_dist()
    {
        return this.explode_dist;
    }

    change_to_saodong()
    {
        let sprite = this.spriteLayer.getChildByName('sprite');

        if (!!sprite) {

            let moveAngle = Math.random()*360;
            let mx = (Math.random()*3 + 2) * Math.cos(moveAngle);
            let my = (Math.random()*3 + 2) * Math.sin(moveAngle);

            let move0 = cc.moveTo(Math.random() + 1,new cc.Vec2(mx,my));
            let move1 = cc.moveTo(Math.random() + 1,new cc.Vec2(-mx,-my));
            let seq = cc.repeatForever(cc.sequence(move0,move1));

            sprite.stopAllActions();
            sprite.runAction(seq);
            /*
            let spineComp = sprite.getComponent(sp.Skeleton);
            if (!!spineComp) {
                let animName = 'saodong_' + (utils.randInt(2) + 1);
                spineComp.setAnimation(0,animName,true);
            }
            */
        }
    }

    change_to_idle()
    {
        let sprite = this.spriteLayer.getChildByName('sprite');

        if (!!sprite) {

            sprite.stopAllActions();
            sprite.setPosition(0,0);
            /*
            let spineComp = sprite.getComponent(sp.Skeleton);
            if (!!spineComp) {
                spineComp.setAnimation(0,'idle',true);
            }
            */
        }
    }

    init()
    {
        if(0 == this.id)
        {
            this.node.active = false;
        }
        else
        {
            let stageId = game.instance.getGameLevelStage();
            let plist_name = 'dish/' + stageId.toString() + '/dishKind';
            let sprite = utils.createSpriteFromPlist(plist_name, this.id.toString());
            
            /*
            let fdName = 'food_' + this.id;
            let sprite =  utils.createSpineNode('fx/'+ fdName + '/' + fdName, 'idle', true);
            */

            let dlyTime = Math.random() * 3.0;
            let delay = cc.delayTime(dlyTime);
            var callback = cc.callFunc(this.change_to_saodong, this);
            let seq0 = cc.sequence(delay, callback, null);
            this.node.runAction(seq0);

            //let sprite = utils.createSprite('general/broth');            
            let bg_sprite = utils.createSpriteFromPlist(plist_name, this.id.toString() + '_0');
            let bg_sprite_highLight = utils.createSpriteFromPlist(plist_name, this.id.toString() + '_1');
            let bg_sprite_autoExplore = utils.createSpriteFromPlist(plist_name, this.id.toString() + '_2');
            bg_sprite_highLight.active = false;
            bg_sprite_autoExplore.active = false;
            bg_sprite.active = false;

            if(sprite != null)
            {
                this.spriteLayer.addChild(sprite, 2, 'sprite');
                this.bkLayer.addChild(bg_sprite, 1, 'bg_sprite');
                this.bkLayer.addChild(bg_sprite_highLight, 1, 'bg_sprite_highLight');
                this.foodEffect.addChild(bg_sprite_autoExplore, 3, 'bg_sprite_autoExplore');
                
            }

            this.fx_name = 'crash_' + this.id;
        }
    }

    apply_fx_list(fx_list)
    {
        for(let i = 0; i < fx_list.length; ++i)
        {
            this.apply_fx(fx_list[i]);
        }
    }

    playSelectSound()
    {
        soundManager.instance.playSound('click');
    }

    playRemoveByExplodeAnimation()
    {
        let seq0 = cc.sequence(cc.delayTime(0.1),cc.fadeOut(0.15));
        let seq1 = cc.sequence(cc.scaleTo(0.1,1.1),cc.scaleTo(0.05,1));

        let sp = cc.spawn(seq0,seq1);
        this.node.runAction(sp);
        
        let effectPool = this.crashEffectLayer.getComponent('effectNodePool');
        
        effectPool.playCrash('crash_coins', this.world_pos, 'start');
        effectPool.playCrash('crash_smog', this.world_pos, 'smog');
    }

    playRemoveAnimation()
    {
        let effectPool = this.crashEffectLayer.getComponent('effectNodePool');

        effectPool.playCrash('crash_coins', this.world_pos, 'start');
        effectPool.playCrash(this.fx_name, this.world_pos, 'start', true);
    }

    apply_fx(fx, param)
    {
        let sprite = this.spriteLayer.getChildByName('sprite');
        let bg_sprite = this.bkLayer.getChildByName('bg_sprite');
        let bg_sprite_highLight = this.bkLayer.getChildByName('bg_sprite_highLight');
        let bg_sprite_auto_explore = this.foodEffect.getChildByName('bg_sprite_autoExplore');

        bg_sprite.active = false;

        if(null == sprite)
        {
            return;
        }

        if(this.cur_fx == fx)
        {
            return;
        }

        if (this.cur_fx == 'touchingConnected') {
            this.change_to_saodong();
        }

        this.cur_fx = fx;

        let scene = this.effectLayer;//cc.director.getScene();

        let size = sprite.getContentSize();
        let node_size = this.node.getContentSize();
        let win_size = cc.winSize;

        let sprite_size = sprite.getContentSize();
        let anchor = sprite.getAnchorPoint();
        let pos = cc.v2(sprite_size.width * anchor.x, sprite_size.height * anchor.y);
        let world_pos = sprite.convertToWorldSpace(pos);
        world_pos = this.effectLayer.convertToNodeSpace(world_pos);
        this.world_pos = world_pos;

        var game_level = this.canvas.getChildByName('script').getComponent('gameLevel');
        var plate_area = game_level.plateAreaNode;
        var npc_num = game_level.npcNumber;
        var npc_data = game_level.npcDatas;

        let z = 888;
        bg_sprite_highLight.active = false;

        if('' == fx || 'default' == fx)
        {
            sprite.scale = 1.0;
            //sprite.opacity = 255;
            sprite.runAction(cc.fadeTo(0.25,255));
            sprite.color = cc.Color.WHITE;
            sprite.removeAllChildren(true);

            bg_sprite.scale = 1.0;
            bg_sprite.active = false;
            bg_sprite_highLight.active = false;

            //bg_sprite_auto_explore.stopAllActions();
           // bg_sprite_auto_explore.opacity = 0;
            bg_sprite_auto_explore.active = false;

        }
        else if ('touchingConnected' == fx)
        {
            bg_sprite_auto_explore.active = false;
            bg_sprite_highLight.active = true;

            this.change_to_idle();
            //bg_sprite.active = true;
            // sprite.color = cc.Color.RED;

            sprite.scale = 1.0;
            bg_sprite.scale = 1.0;

            bg_sprite_highLight.runAction(cc.fadeTo(0.25, 255));
            
            let seq2 = cc.sequence(
                cc.scaleTo(0.02, 0.5).easing(cc.easeBackOut()),
                cc.scaleTo(0.1, 1.1).easing(cc.easeBackOut()),
                cc.scaleTo(0.05, 1.0).easing(cc.easeBackOut()));

            this.node.runAction(seq2);

        }
        else if ('autoExplore' == fx)
        {
            if (!!param) {

                bg_sprite_auto_explore.active = true;
                bg_sprite_auto_explore.opacity = 0;
                let seq = cc.sequence(cc.delayTime(param.tim),cc.fadeTo(0.1,255));
                bg_sprite_auto_explore.runAction(seq);

                if (param.sound == true)
                {
                    let delay = cc.delayTime(param.tim);
                    var callback = cc.callFunc(this.playSelectSound, this);
        
                    let seq = cc.sequence(delay, callback, null);
                    this.node.runAction(seq);
                }

                if (param.scale == true) {
                    let tm = param.tim - 0.1;
                    if (tm < 0) {
                        tm = 0;
                    }
                    let seq2 = cc.sequence(cc.delayTime(tm),
                                            cc.scaleTo(0.02, 0.5).easing(cc.easeBackOut()),
                                            cc.scaleTo(0.1, 1.1).easing(cc.easeBackOut()),
                                            cc.scaleTo(0.05, 1.0).easing(cc.easeBackOut()));

                    this.node.runAction(seq2);
                }
            }
        }
        else if('selected' == fx)
        {
            sprite.scale = 1.2;
            //sprite.opacity = 255;
            sprite.runAction(cc.fadeTo(0.25,255));
            //bg_sprite_highLight.active = true;

            // let fx_node = utils.createParticle('particle/cabbage_crash');

            // fx_node.setPosition(world_pos.x - this.canvas.width * 0.5, world_pos.y - this.canvas.height * 0.5);
            // this.canvas.addChild(fx_node, z);

            // fx_node.setPosition(world_pos);
            // scene.addChild(fx_node, z);
        }
        else if('unselected' == fx)
        {
            sprite.scale = 1.0;
            sprite.runAction(cc.fadeTo(0.25, 255));
        }
        else if('connected' == fx)
        {
            //bg_sprite.active = true;
            // sprite.color = cc.Color.RED;
            bg_sprite_auto_explore.active = false;
            sprite.scale = 1.0;
            bg_sprite.scale = 1.0;

            sprite.runAction(cc.fadeTo(0.25, 255));
        }
        else if('big_connected' == fx)
        {
            //bg_sprite.active = true;
            // sprite.color = cc.Color.RED;

            sprite.scale = 1.2;
            bg_sprite.scale = 1.2;

        }
        else if('unconnected' == fx)
        {
            bg_sprite.active = false;
            // sprite.color = cc.Color.BLACK;
            //sprite.opacity = 128;
            sprite.runAction(cc.fadeTo(0.25, 128));

            sprite.scale = 1.0;
            bg_sprite.scale = 1.0;

        }
        else if('bomb' == fx)
        {
            // sprite.removeFromParent(true);
            // sprite = utils.createSprite('store/button_close');

            // if(sprite != null)
            // {
            //     this.node.addChild(sprite, 1, 'sprite');
            // }

            sprite.active = false;
            bg_sprite.active = false;

            let bomb_script = this.node.getComponent('Bomb');
            bomb_script.applyInterface();
        }
        else if('soup' == fx)
        {
            sprite.active = false;
            bg_sprite.active = false;

            let soup_icon = utils.createSprite('general/soup');
            soup_icon.setPosition(0, 0);
            this.node.addChild(soup_icon);

        }
        else if('auto' == fx)
        {
            // let fx_sprite = utils.createSprite('particle/paopao');
            // fx_sprite.scale = 4.0;
            // fx_sprite.parent = sprite;

            //bg_sprite.active = true;

        }
        else if('remove_by_explode' == fx)
        {
            let dlyTime = 0;
            if (!!param) {
                dlyTime = param.time;
            }

            if (dlyTime > 0) {

                cc.log('dlyTime  =' + dlyTime);

                let delay = cc.delayTime(dlyTime);
                var callback = cc.callFunc(this.playRemoveByExplodeAnimation, this);
                let seq0 = cc.sequence(delay, callback, null);
                this.node.runAction(seq0);
            }
            else {

                this.node.runAction(cc.fadeOut(0.1));
                let effectPool = this.crashEffectLayer.getComponent('effectNodePool');
                effectPool.playCrash('crash_smog', world_pos, 'smog');
            }
        }
        else if('remove' == fx)
        {
            this.playRemoveAnimation();
            /*
            let dlyTime = Math.random()*0.2;
            let delay = cc.delayTime(dlyTime);
            var callback = cc.callFunc(this.playRemoveAnimation, this);
            let seq0 = cc.sequence(delay, callback, null);
            this.node.runAction(seq0);
            */
        }
        else if('explode_preview' == fx)
        {
            sprite.color = cc.Color.BLUE;
            //let effectPool = this.crashEffectLayer.getComponent('effectNodePool');
            //effectPool.playCrash(this.fx_name, world_pos);

        }
        else if('explode' == fx)
        {
            
            let explode_fx = null;

            let bomb_script = this.node.getComponent('Bomb');

            let bomb_name = 'crash_' + bomb_script.id;

            if(1 == bomb_script.level)
            {
                //this.node.runAction(cc.fadeOut(0.3));
                explode_fx = utils.createSpineNode('fx/crash_bomo/crash_bomo', bomb_name, false, '',
                    function(node, name)
                    {
                        let sk = node.getComponent(sp.Skeleton);
                        sk.timeScale = 1.75;
                        let track = sk.setAnimation(0, name, false);

                        if(track)
                        {
                            sk.setCompleteListener((trackEntry, loopCount) =>
                            {
                                let name = trackEntry.animation? trackEntry.animation.name : '';

                                if(name == bomb_name)
                                {
                                    node.destroy();
                                }
                            });
                        }
                    });
            }
            else if(2 == bomb_script.level)
            {
                //this.node.runAction(cc.fadeOut(0.3));

                explode_fx = utils.createSpineNode('fx/crash_bomo/crash_bomo', bomb_name, false, '',
                    function(node, name)
                    {
                        let sk = node.getComponent(sp.Skeleton);
                        sk.timeScale = 1.75;
                        let track = sk.setAnimation(0, name, false);

                        if(track)
                        {
                            sk.setCompleteListener((trackEntry, loopCount) =>
                            {
                                let name = trackEntry.animation? trackEntry.animation.name : '';

                                if(name === bomb_name)
                                {
                                    node.destroy();
                                }
                            });
                        }
                    });
            }

            if(explode_fx)
            {
                explode_fx.scale = 1.5;
                explode_fx.setPosition(world_pos.x, world_pos.y);
                scene.addChild(explode_fx, z);
            }
        }
        else if('death' == fx)
        {
            sprite.scale = 1.0;
            //sprite.opacity = 255;
            sprite.runAction(cc.fadeTo(0.25,255));
            sprite.color = cc.Color.WHITE;
            sprite.removeAllChildren(true);

            sprite.active = false;
            bg_sprite.active = false;

            let bomb_script = this.node.getComponent('Bomb');
            let soup_script = this.node.getComponent('Soup');

            // if(0 == bomb_script.level)
            {
                if(soup_script.id > 0)
                {
                    let soup_icon = utils.createSprite('general/soup');

                    scene.addChild(soup_icon);
                    soup_icon.setPosition(world_pos.x, world_pos.y);

                    let icon_size = this.soup_icon.getContentSize();
                    let icon_anchor = this.soup_icon.getAnchorPoint();
                    let icon_pos = cc.v2(icon_size.width * icon_anchor.x, icon_size.height * icon_anchor.y);

                    let target_pos = this.soup_icon.convertToWorldSpace(icon_pos);

                    let delay1 = cc.delayTime(0.2);
                    let move_to = cc.moveTo(0.8, target_pos);
                    let delay2 = cc.delayTime(0.3);
                    let remove_self = cc.removeSelf(true);
                    let seq = cc.sequence(delay1, move_to, delay2, remove_self);

                    soup_icon.runAction(seq);
                }
                else if(this.id > 0)
                {
                    // let coin_icon = utils.createSprite('general/coins');

                    // scene.addChild(coin_icon);
                    // coin_icon.setPosition(world_pos.x, world_pos.y);

                    // let icon_size = this.coin_icon.getContentSize();
                    // let icon_anchor = this.coin_icon.getAnchorPoint();
                    // let icon_pos = cc.v2(icon_size.width * icon_anchor.x, icon_size.height * icon_anchor.y);

                    // let target_pos = this.coin_icon.convertToWorldSpace(icon_pos);

                    // let delay1 = cc.delayTime(0.2);
                    // let move_to = cc.moveTo(0.8, target_pos);
                    // let delay2 = cc.delayTime(0.3);
                    // let remove_self = cc.removeSelf(true);
                    // let seq = cc.sequence(delay1, move_to, delay2, remove_self);

                    // coin_icon.runAction(seq);

                    for(let i = 0; i < npc_num; ++i)
                    {
                        let npc_node = plate_area.getChildByName('npc_' + i);
                        let food_node = npc_node.getChildByName('dishKind');
                        let food_node_size = food_node.getContentSize();
                        let food_node_anchor = food_node.getAnchorPoint();
                        let want = npc_data[i].wantEat;

                        if(want == this.id)
                        {
                            let target_pos = food_node.convertToWorldSpace(cc.v2(food_node_size.width * food_node_anchor.x, food_node_size.height * food_node_anchor.y));

                            let plist_name = 'dishKind';
                            let tmp_sprite = utils.createSpriteFromPlist(plist_name, this.id.toString());
                            tmp_sprite.setPosition(world_pos.x, world_pos.y);
                            scene.addChild(tmp_sprite, z);

                            // let remove_fx = utils.createSpineNode('fx/' + this.fx_name + '/' + this.fx_name, 'start', true, this.fx_name);
                            // remove_fx.setPosition(world_pos.x, world_pos.y);
                            // scene.addChild(remove_fx, z);

                            let delay1 = cc.delayTime(0.2);
                            let move_to = cc.moveTo(0.8, target_pos);
                            let delay2 = cc.delayTime(0.3);
                            let remove_self = cc.removeSelf(true);
                            let seq = cc.sequence(delay1, move_to, delay2, remove_self);

                            tmp_sprite.runAction(seq);

                            // remove_fx.runAction(seq);
                        }
                    }
                }
            }

        }
    }

    update(dt)
    {
    }
}
